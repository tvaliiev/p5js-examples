
# P5 js examples
[![Pipeline status](https://gitlab.com/tvaliiev/p5js-examples/badges/master/pipeline.svg)](https://gitlab.com/tvaliiev/p5js-examples/-/commits/master)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)


## Description
This is my personal project to discover p5js library and have fun with it.

## Development

### Dependencies
Please pay attention, that project is using `pnpm` to manage dependencies.
You can install it by running:
```shell
$ npm i -g pnpm
```

### Building and development process
To build and watch it, clone this repo and run:

```shell
$ pnpm install
$ pnpm run serve
```

### Testing
To run tests, simply run:
```shell
$ pnpm run test
```

You can also refer to scripts in package.json to see what is going on and what exactly runs.
