import { createApp } from 'vue'
import App from './App.vue'
import * as VueRouter from 'vue-router'

const routes = [
  { path: '/', component: () => import('./pages/Home.vue') },
  { path: '/:renderer(.+)', component: () => import('./pages/Renderer.vue') },
]

const router = VueRouter.createRouter({
  history: VueRouter.createWebHistory(),
  routes,
})
const app = createApp(App)
app.use(router)
app.mount('#app')
