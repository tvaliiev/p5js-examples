import seedrandom from 'seedrandom'

export function restoreRandom(seed: string = null) {
  let originalRandom = Math.random
  afterEach(() => {
    Math.random = originalRandom
  })

  if (seed !== null) {
    beforeEach(() => {
      predictableRandom(seed)
    })
  }
}

export function predictableRandom(seed: string = '1') {
  seedrandom(seed, { global: true })
}
