export class LocalStorage {
  private static NAMESPACE_SEPARATOR = '--'

  constructor(private namespace) {}

  private getNameSpacePrefix(): string {
    return this.namespace + LocalStorage.NAMESPACE_SEPARATOR
  }

  private formatName(name: string): string {
    return this.getNameSpacePrefix() + name
  }

  public getItem(name: string, defaultItem: any = undefined): any {
    if (!this.hasItem(name)) {
      return defaultItem
    }

    return JSON.parse(localStorage.getItem(this.formatName(name)))
  }

  public setItem(name: string, value: any) {
    localStorage.setItem(this.formatName(name), JSON.stringify(value))
  }

  public hasItem(name: string): boolean {
    return localStorage.getItem(this.formatName(name)) !== undefined
  }
}
