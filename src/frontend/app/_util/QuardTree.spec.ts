import { Boundary } from './Boundary'
import { IBoundary } from './IBoundary'
import { Point } from './Point'
import {
  AlreadyDividedException,
  QuadTree,
  QuadTreeDivideDir,
} from './QuadTree'

describe('QuadTree.divide&forceDivide', () => {
  it('Should create and divide tree with id', () => {
    const tree = new QuadTree(
      new Boundary(new Point(10, 10), new Point(40, 40)),
      QuadTree.DEFAULT_DIVIDE_THRESHOLD,
      'r',
    )

    tree.forceDivide(2)

    expect(tree.isDivided()).toBeTruthy()
    expect(tree.getId()).toEqual('r')

    Object.values(QuadTreeDivideDir).forEach(divideDir1 => {
      const subTree1 = tree.getSubTrees()[divideDir1]

      expect(subTree1.getId()).toEqual(`r-${divideDir1}`)

      Object.values(QuadTreeDivideDir).forEach(divideDir2 => {
        const subTree2 = subTree1.getSubTrees()[divideDir2]

        expect(subTree2.getId()).toEqual(`r-${divideDir1}-${divideDir2}`)
      })
    })
  })

  it('Should not divide already divided tree', () => {
    const tree = new QuadTree(
      new Boundary(new Point(0, 0), new Point(400, 400)),
    )

    tree.divide()

    expect(() => tree.divide()).toThrow(AlreadyDividedException)
  })

  it('Should divide into 4 trees 2 times', () => {
    const tree = new QuadTree(
      new Boundary(new Point(10, 10), new Point(40, 40)),
    )

    type DivideConfig = {
      boundary: IBoundary
      subTrees?: {
        [key in QuadTreeDivideDir]: DivideConfig
      }
    }
    const config: DivideConfig = {
      boundary: { top_left: { x: 10, y: 10 }, bottom_right: { x: 40, y: 40 } },
      subTrees: {
        [QuadTreeDivideDir.TOP_LEFT]: {
          boundary: {
            top_left: { x: 10, y: 10 },
            bottom_right: { x: 25, y: 25 },
          },
          subTrees: {
            [QuadTreeDivideDir.TOP_LEFT]: {
              boundary: {
                top_left: { x: 10, y: 10 },
                bottom_right: { x: 17.5, y: 17.5 },
              },
            },
            [QuadTreeDivideDir.TOP_RIGHT]: {
              boundary: {
                top_left: { x: 17.5, y: 10 },
                bottom_right: { x: 25, y: 17.5 },
              },
            },
            [QuadTreeDivideDir.BOTTOM_LEFT]: {
              boundary: {
                top_left: { x: 10, y: 17.5 },
                bottom_right: { x: 17.5, y: 25 },
              },
            },
            [QuadTreeDivideDir.BOTTOM_RIGHT]: {
              boundary: {
                top_left: { x: 17.5, y: 17.5 },
                bottom_right: { x: 25, y: 25 },
              },
            },
          },
        },
        [QuadTreeDivideDir.TOP_RIGHT]: {
          boundary: {
            top_left: { x: 25, y: 10 },
            bottom_right: { x: 40, y: 25 },
          },
          subTrees: {
            [QuadTreeDivideDir.TOP_LEFT]: {
              boundary: {
                top_left: { x: 25, y: 10 },
                bottom_right: { x: 32.5, y: 17.5 },
              },
            },
            [QuadTreeDivideDir.TOP_RIGHT]: {
              boundary: {
                top_left: { x: 32.5, y: 10 },
                bottom_right: { x: 40, y: 17.5 },
              },
            },
            [QuadTreeDivideDir.BOTTOM_LEFT]: {
              boundary: {
                top_left: { x: 25, y: 17.5 },
                bottom_right: { x: 32.5, y: 25 },
              },
            },
            [QuadTreeDivideDir.BOTTOM_RIGHT]: {
              boundary: {
                top_left: { x: 32.5, y: 17.5 },
                bottom_right: { x: 40, y: 25 },
              },
            },
          },
        },
        [QuadTreeDivideDir.BOTTOM_LEFT]: {
          boundary: {
            top_left: { x: 10, y: 25 },
            bottom_right: { x: 25, y: 40 },
          },
          subTrees: {
            [QuadTreeDivideDir.TOP_LEFT]: {
              boundary: {
                top_left: { x: 10, y: 25 },
                bottom_right: { x: 17.5, y: 32.5 },
              },
            },
            [QuadTreeDivideDir.TOP_RIGHT]: {
              boundary: {
                top_left: { x: 17.5, y: 25 },
                bottom_right: { x: 25, y: 32.5 },
              },
            },
            [QuadTreeDivideDir.BOTTOM_LEFT]: {
              boundary: {
                top_left: { x: 10, y: 32.5 },
                bottom_right: { x: 17.5, y: 40 },
              },
            },
            [QuadTreeDivideDir.BOTTOM_RIGHT]: {
              boundary: {
                top_left: { x: 17.5, y: 32.5 },
                bottom_right: { x: 25, y: 40 },
              },
            },
          },
        },
        [QuadTreeDivideDir.BOTTOM_RIGHT]: {
          boundary: {
            top_left: { x: 25, y: 25 },
            bottom_right: { x: 40, y: 40 },
          },
          subTrees: {
            [QuadTreeDivideDir.TOP_LEFT]: {
              boundary: {
                top_left: { x: 25, y: 25 },
                bottom_right: { x: 32.5, y: 32.5 },
              },
            },
            [QuadTreeDivideDir.TOP_RIGHT]: {
              boundary: {
                top_left: { x: 32.5, y: 25 },
                bottom_right: { x: 40, y: 32.5 },
              },
            },
            [QuadTreeDivideDir.BOTTOM_LEFT]: {
              boundary: {
                top_left: { x: 25, y: 32.5 },
                bottom_right: { x: 32.5, y: 40 },
              },
            },
            [QuadTreeDivideDir.BOTTOM_RIGHT]: {
              boundary: {
                top_left: { x: 32.5, y: 32.5 },
                bottom_right: { x: 40, y: 40 },
              },
            },
          },
        },
      },
    }

    tree.forceDivide(2)
    expect(tree).toMatchObject(config)

    checkDivision(tree, config)

    function checkDivision(
      tree: QuadTree,
      config: DivideConfig,
      path: QuadTreeDivideDir[] = [],
    ) {
      const boundary = tree.getBoundary()

      expect(boundary.top_left.x).toEqual(config.boundary.top_left.x)
      expect(boundary.top_left.y).toEqual(config.boundary.top_left.y)

      expect(boundary.bottom_right.x).toEqual(config.boundary.bottom_right.x)
      expect(boundary.bottom_right.y).toEqual(config.boundary.bottom_right.y)

      if (config.subTrees) {
        expect(Object.values(tree.getSubTrees()).length).toEqual(4)

        Object.keys(tree.getSubTrees()).forEach(direction => {
          checkDivision(
            tree.getSubTrees()[direction],
            config.subTrees[direction],
            [...path, <QuadTreeDivideDir>(<any>direction)],
          )
        })
      } else {
        expect(Object.values(tree.getSubTrees()).length).toEqual(0)
      }
    }
  })
})

describe('QuadTree.addPoint', () => {
  it('Should not add same point twice', () => {
    const tree = new QuadTree(
      new Boundary(new Point(10, 10), new Point(20, 20)),
    )

    tree.addPoint(new Point(10, 10))
    tree.addPoint(new Point(10, 10))

    expect(tree.getPoints().length).toEqual(1)
  })

  it('Should add point', () => {
    const tree = new QuadTree(
      new Boundary(new Point(10, 10), new Point(20, 20)),
    )

    const result = tree.addPoint(new Point(15, 15))
    expect(result).toBeTruthy()
  })

  it('Should not add point when it out of tree boundary', () => {
    const tree = new QuadTree(
      new Boundary(new Point(10, 10), new Point(20, 20)),
    )

    const result = tree.addPoint(new Point(9, 9))
    expect(result).toBeFalsy()
  })

  it(`Should divide into subtree after insertion of treshhold points`, () => {
    const tree = new QuadTree(
      new Boundary(
        new Point(10, 10),
        new Point(
          20 + QuadTree.DEFAULT_DIVIDE_THRESHOLD,
          20 + QuadTree.DEFAULT_DIVIDE_THRESHOLD,
        ),
      ),
    )

    for (let i = 0; i <= tree.getThreshold(); i++) {
      expect(tree.addPoint(new Point(10 + i, 10 + i))).toBeTruthy()
    }

    expect(tree.isDivided()).toBeTruthy()
    expect(Object.values(tree.getSubTrees()))
  })
})

describe('QuadTree.inBoundary', () => {
  const tree = new QuadTree(new Boundary(new Point(10, 10), new Point(20, 20)))

  it.each([
    new Boundary(new Point(0, 0), new Point(20, 20)),
    new Boundary(new Point(19, 19), new Point(30, 30)),
  ])(`Should tell that %s in quadTree`, boundary => {
    expect(tree.inMyBoundary(boundary)).toBeTruthy()
  })
})

describe('QuadTree.pointsInBoundary', () => {
  it('Should output empty array when boundary is not in range', () => {
    const tree = new QuadTree(
      new Boundary(new Point(10, 10), new Point(20, 20)),
    )
    expect(
      tree.itemsInBoundary(new Boundary(new Point(15, 25), new Point(20, 30)))
        .length,
    ).toEqual(0)
  })

  it('Should return list of points in boundary in tree', () => {
    const tree = new QuadTree(
      new Boundary(new Point(10, 10), new Point(20, 20)),
      40,
    )
    const boundary1 = new Boundary(new Point(5, 5), new Point(15, 15))
    const boundary2 = new Boundary(new Point(15, 5), new Point(40, 40))

    for (let i = 10; i <= 20; i++) {
      tree.addPoint(new Point(i, i))
    }

    expect(tree.isDivided()).toBeFalsy()
    expect(tree.getPoints().length).toEqual(11)
    expect(tree.itemsInBoundary(boundary1).length).toEqual(6)
    expect(tree.itemsInBoundary(boundary1)).toMatchObject(
      [
        { x: 10, y: 10 },
        new Point(11, 11),
        new Point(12, 12),
        new Point(13, 13),
        new Point(14, 14),
        new Point(15, 15),
      ].map(p => ({ point: p, item: undefined })),
    )

    expect(tree.itemsInBoundary(boundary2).length).toEqual(6)
    expect(tree.itemsInBoundary(boundary2)).toMatchObject(
      [
        new Point(15, 15),
        new Point(16, 16),
        new Point(17, 17),
        new Point(18, 18),
        new Point(19, 19),
        new Point(20, 20),
      ].map(p => ({ point: p, item: undefined })),
    )
  })

  it('Should return list of points in boundary in tree when divided', () => {
    const tree = new QuadTree(new Boundary(new Point(0, 0), new Point(10, 10)))
    tree.forceDivide(5)

    for (let i = 0; i <= 10; i++) {
      tree.addPoint(new Point(i, i))
      tree.addPoint(new Point(i, 10 - i))
    }

    expect(tree.isDivided()).toBeTruthy()
    const actualResult = tree.itemsInBoundary(
      new Boundary(new Point(0, 0), new Point(5, 15)),
    )
    expect(actualResult).toMatchObject(
      [
        { x: 0, y: 0 },
        { x: 1, y: 1 },
        { x: 2, y: 2 },
        { x: 3, y: 3 },
        { x: 4, y: 4 },
        { x: 5, y: 5 },
        { x: 4, y: 6 },
        { x: 3, y: 7 },
        { x: 2, y: 8 },
        { x: 1, y: 9 },
        { x: 0, y: 10 },
      ].map(p => ({ point: p, item: undefined })),
    )
  })
})
