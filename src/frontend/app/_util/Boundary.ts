import P5 from 'p5'
import { IBoundary } from './IBoundary'
import { Point } from './Point'
import { randomFloat } from './util'

export class Boundary implements IBoundary {
  public readonly width: number
  public readonly height: number
  public readonly center: Point

  constructor(
    public readonly top_left: Point,
    public readonly bottom_right: Point,
  ) {
    this.width = this.bottom_right.x - this.top_left.x
    this.height = this.bottom_right.y - this.top_left.y

    this.center = new Point(
      this.top_left.x + this.width / 2,
      this.top_left.y + this.height / 2,
    )
  }

  public containPoint(point: Point): boolean {
    return (
      point.x >= this.top_left.x &&
      point.x <= this.bottom_right.x &&
      point.y >= this.top_left.y &&
      point.y <= this.bottom_right.y
    )
  }

  public intersect(other: Boundary): boolean {
    return !this.notIntersect(other)
  }

  public notIntersect(other: Boundary): boolean {
    return (
      other.top_left.x > this.bottom_right.x ||
      other.bottom_right.x < this.top_left.x ||
      other.top_left.y > this.bottom_right.y ||
      other.bottom_right.y < this.top_left.y
    )
  }

  public draw(p5: P5, ...corners: number[]): void {
    p5.rect(
      this.top_left.x,
      this.top_left.y,
      this.width,
      this.height,
      ...corners,
    )
  }

  public randomPoint(): Point {
    return Point.fromArray([
      randomFloat(this.top_left.x, this.bottom_right.x),
      randomFloat(this.top_left.y, this.bottom_right.y),
    ])
  }

  public divideEvenly(boundariesCount: Point): Boundary[] {
    const result = []
    const oneBoundaryDimension = Point.fromArray([
      this.width / boundariesCount.x,
      this.height / boundariesCount.y,
    ])

    for (
      let boundaryYCoord = 0;
      boundaryYCoord < boundariesCount.y;
      boundaryYCoord++
    ) {
      for (
        let boundaryXCoord = 0;
        boundaryXCoord < boundariesCount.x;
        boundaryXCoord++
      ) {
        result.push(
          new Boundary(
            Point.fromObject({
              x: this.top_left.x + oneBoundaryDimension.x * boundaryXCoord,
              y: this.top_left.y + oneBoundaryDimension.y * boundaryYCoord,
            }),
            Point.fromObject({
              x:
                this.bottom_right.x -
                oneBoundaryDimension.x *
                  (boundariesCount.x - boundaryXCoord - 1),
              y:
                this.bottom_right.y -
                oneBoundaryDimension.y *
                  (boundariesCount.y - boundaryYCoord - 1),
            }),
          ),
        )
      }
    }

    return result
  }

  toString(): string {
    return `Boundary(${this.top_left.toString()},${this.bottom_right.toString()})`
  }
}
