import { Direction, randomDirection } from './Direction'
import { range } from './util'

describe('Direction.randomDirection', () => {
  it('should choose between direction randomly and pick one', () => {
    const pickedDirections = {
      [Direction.LEFT]: 0,
      [Direction.UP]: 0,
      [Direction.DOWN]: 0,
      [Direction.RIGHT]: 0,
    }

    range(100).forEach(() => {
      const direction = randomDirection()
      expect([
        Direction.UP,
        Direction.DOWN,
        Direction.RIGHT,
        Direction.LEFT,
      ]).toContain(direction)

      pickedDirections[direction]++
    })

    expect(Object.values(pickedDirections)).not.toContain(0)
  })
})
