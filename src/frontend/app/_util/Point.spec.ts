import P5 from 'p5'
import { Boundary } from './Boundary'
import { Color } from './Color'
import { IPoint, Point } from './Point'
import { range } from './util'

describe('Point.addPoint', () => {
  it('Should add two points', () => {
    const point = new Point(1, 2).addPoint(new Point(3, 4))
    expect(point.x).toBe(4)
    expect(point.y).toBe(6)
  })
})

describe('Point.add', () => {
  it('Should add two points', () => {
    const point = new Point(1, 2).add(3, 4)
    expect(point.x).toBe(4)
    expect(point.y).toBe(6)
  })
})

describe('Point.inBoundary', () => {
  it('Should report that point in boundary', () => {
    const result = new Point(5, 5).inBoundary(
      new Boundary(new Point(4, 4), new Point(6, 6)),
    )
    expect(result).toBeTruthy()
  })

  const boundary = new Boundary(new Point(4, 4), new Point(8, 8))
  const points = [
    new Point(0, 0), // top left
    new Point(6, 0), // top
    new Point(10, 0), // top right
    new Point(3, 5), // left
    new Point(10, 5), // right
    new Point(2, 10), // bottom left
    new Point(6, 10), // bottom
    new Point(10, 10), // bottom right
  ]

  points.forEach(point => {
    it(`Should report that ${point} not in ${boundary}`, () => {
      expect(point.inBoundary(boundary)).toBeFalsy()
    })
  })
})

describe('Point.drawAsPixel', () => {
  it('Should replace appropriate pixels in P5', () => {
    const P5Mock = jest.fn(() => ({
      pixels: range(3 * 3 * 4, 0, 0),
      width: 3,
      height: 3,
    }))

    const p5Mock = new P5Mock()
    new Point(1, 2).drawAsPixel(p5Mock as any as P5, new Color(5, 6, 7, 8))

    expect(p5Mock.pixels).toEqual([
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 5, 6, 7, 8, 0, 0, 0, 0,
    ])
  })
})

describe('Point.drawAsCircle', () => {
  it('Should draw circle', () => {
    const P5 = jest.fn(() => ({
      circle: jest.fn(),
    }))
    const p5 = new P5()

    new Point(1, 2).drawAsCircle(p5 as any as P5)

    expect(p5.circle).toBeCalledWith(1, 2, 2)
  })
})

describe('Point.drawAsPoint', () => {
  it('Should draw point', () => {
    const P5 = jest.fn(() => ({
      point: jest.fn(),
    }))
    const p5 = new P5()

    new Point(1, 2).drawAsPoint(p5 as any as P5)

    expect(p5.point).toBeCalledWith(1, 2)
  })
})

describe('Point.multiply', () => {
  it('Should multiply two points', () => {
    const result: Point = new Point(1, 2).multiply(3, 4)

    expect(result.x).toEqual(3)
    expect(result.y).toEqual(8)
  })
})

describe('Point.multiplyPoint', () => {
  it('Should multiply two points', () => {
    const result: Point = new Point(1, 2).multiplyPoint(new Point(3, 4))

    expect(result.x).toEqual(3)
    expect(result.y).toEqual(8)
  })
})

describe('Point.isEqual', () => {
  it.each([
    [new Point(0, 0), new Point(0, 0)],
    [new Point(-1, -1), new Point(-1, -1)],
    [new Point(0.255, 0.255), new Point(0.255, 0.255)],
    [new Point(23, 23), new Point(23, 23)],
  ])('Should tell that %s equals %s', (p1, p2) => {
    expect(p1.isEqual(p2)).toBeTruthy()
  })

  it.each([
    [new Point(0.255, 0.255), new Point(0.254, 0.255)],
    [new Point(-0.01, 0), new Point(-0, -0)],
  ])('Should tell that %s not equals %s', (p1, p2) => {
    expect(p1.isEqual(p2)).toBeFalsy()
  })
})

describe('Point.toShortString', () => {
  it('Should produce short representation of Point', () => {
    const actual = new Point(1, 2).toShortString()

    expect(actual).toMatchInlineSnapshot(`"(1, 2)"`)
  })
})

describe('Point.toString', () => {
  it('Should produce string', () => {
    const actual = new Point(1, 2).toString()

    expect(actual).toMatchInlineSnapshot(`"Point(1, 2)"`)
  })
})

describe('Point.limit', () => {
  it.each<{ point: Point; limit: number; result: IPoint }[]>([
    [{ point: new Point(5, -5), limit: 1, result: new Point(1, -1) }],
    [{ point: new Point(-5, 5), limit: 6, result: new Point(-5, 5) }],
    [{ point: new Point(-5, 5), limit: 1, result: new Point(-1, 1) }],
  ])('Should limit with options %p', ({ point, limit, result }) => {
    const r = point.limit(limit)

    expect(r.x).toEqual(result.x)
    expect(r.y).toEqual(result.y)
  })
})

describe('Point.copy', () => {
  it('Should copy point', () => {
    expect(new Point(2, 3).copy()).toMatchObject(new Point(2, 3))
  })
})

describe('Point.getMagnitude', () => {
  it('Should provide point magnitude', () => {
    expect(new Point(3, 4).getMagnitude()).toEqual(5)
  })
})

describe('Point.normalize', () => {
  it('Should normalize point', () => {
    const normalizedPoint = new Point(3, 4).normalize()

    expect(normalizedPoint.x).toBeCloseTo(0.6)
    expect(normalizedPoint.y).toBeCloseTo(0.8)
  })

  it('Should normalize point with 0,0', () => {
    const normalizedPoint = new Point(0, 0).normalize()

    expect(normalizedPoint).toMatchObject(new Point(0, 0))
  })
})

describe('Point.subtractPoint', () => {
  it.each([
    [new Point(1, 2), new Point(1, 2), new Point(0, 0)],
    [new Point(3, 5), new Point(7, 2), new Point(-4, 3)],
  ])('Should subtract two points', (p1, p2, result) => {
    expect(p1.subtractPoint(p2)).toMatchObject(result)
  })
})

describe('Point.setMagnitude', () => {
  it('Should set point magnitude', () => {
    const point = new Point(3, 4).setMagnitude(10)

    expect(point.x).toBeCloseTo(6)
    expect(point.y).toBeCloseTo(8)
  })

  it('Should set point magnitude with 0,0 coords', () => {
    expect(new Point(0, 0).setMagnitude(20)).toMatchObject(new Point(0, 0))
  })
})

describe('Point.fromAngle', () => {
  it('Should generate point from angle', () => {
    const p = Point.fromAngle(Math.PI)
    expect(p.x).toBeCloseTo(-1)
    expect(p.y).toBeCloseTo(0)
  })

  it('Should generate point from angle when length set', () => {
    const p = Point.fromAngle(Math.PI, 2)
    expect(p.x).toBeCloseTo(-2)
    expect(p.y).toBeCloseTo(0)
  })
})

describe('Point.random', () => {
  it('Should give random vector', () => {
    const p = Point.random()

    expect(p.x).toBeGreaterThanOrEqual(-1)
    expect(p.x).toBeLessThanOrEqual(1)

    expect(p.y).toBeGreaterThanOrEqual(-1)
    expect(p.y).toBeLessThanOrEqual(1)
  })

  it('Should give random vector with set length', () => {
    const p = Point.random(3)

    expect(p.x).toBeGreaterThanOrEqual(-3)
    expect(p.x).toBeLessThanOrEqual(3)

    expect(p.y).toBeGreaterThanOrEqual(-3)
    expect(p.y).toBeLessThanOrEqual(3)
  })
})

describe('Point.when', () => {
  it('Should execute function when condition is true', () => {
    const exec = jest.fn()
    const resultValue = new Point(4, 4)
    exec.mockReturnValue(resultValue)
    const point = new Point(2, 2)

    const result = point.when(true, exec)

    expect(exec).toBeCalledWith(point)
    expect(result).toEqual(resultValue)
  })

  it('Should not execute function when condition is false', () => {
    const exec = jest.fn()
    const resultValue = new Point(4, 4)
    exec.mockReturnValue(resultValue)
    const point = new Point(2, 2)

    const result = point.when(false, exec)

    expect(exec).not.toBeCalled()
    expect(result).toEqual(point)
  })

  it('should execute provided function when condition executed to true', () => {
    const condition = jest.fn()
    condition.mockReturnValue(true)
    const exec = jest.fn()

    Point.fromArray([1, 1]).when(condition, exec)

    expect(exec).toBeCalled()
  })

  it('should not execute provided function when condition executed to false', () => {
    const condition = jest.fn()
    condition.mockReturnValue(false)
    const exec = jest.fn()

    Point.fromArray([1, 1]).when(condition, exec)

    expect(exec).not.toBeCalled()
  })
})

describe('Point.distance', () => {
  it.each([
    [new Point(0, 1), new Point(0, 1), 0],
    [new Point(0, 1), new Point(0, 3), 2],
    [new Point(0, 0), new Point(-2, 3), 3.605551],
    [new Point(-3, -2), new Point(-2, 3), 5.09901],
    [new Point(-2, -2), new Point(2, 2), 5.65684],
  ])('Should calculate distance between two points', (p1, p2, expected) => {
    expect(p1.distance(p2)).toBeCloseTo(expected, 4)
  })
})

describe('Point.dividePoint', () => {
  it('Should divide two points', () => {
    expect(new Point(5, 5).dividePoint({ x: 2, y: 2 })).toMatchObject({
      x: 2.5,
      y: 2.5,
    })
  })
})

describe('Point.divide', () => {
  it('Should divide point', () => {
    expect(new Point(5, 5).divide(2, 5)).toMatchObject({
      x: 2.5,
      y: 1,
    })
  })
})

describe('Point.getSquaredMagnitude', () => {
  it('Should provide squared magnitude of point', () => {
    expect(new Point(5, 5).getSquaredMagnitude()).toEqual(50)
  })
})

type FlatPoint = [number, number]
describe('Point.constrain', () => {
  it.each<
    { point: FlatPoint; min: FlatPoint; max: FlatPoint; expected: FlatPoint }[]
  >([
    [{ point: [2, 2], min: [0, 3], max: [1, 5], expected: [1, 3] }],
    [{ point: [2, 2], min: [3, 0], max: [5, 1], expected: [3, 1] }],
  ])(
    'should constrain point between two boundaries',
    ({ point, min, max, expected }) => {
      const p = Point.fromArray(point)

      expect(
        p.constrain(Point.fromArray(min), Point.fromArray(max)),
      ).toMatchObject(Point.fromArray(expected))
    },
  )

  it('should constrain point when given two numbers', () => {
    expect(Point.fromArray([1, 5]).constrain(2, 3)).toMatchObject({
      x: 2,
      y: 3,
    })
  })

  it('should constraint between two boundaries with default min === (0,0)', () => {
    const p = new Point(-1, 3)
    expect(p.constrain({ x: 2, y: 2 })).toMatchObject({
      x: 0,
      y: 2,
    })
  })
})

describe('Point.floor', () => {
  it('should floor point', () => {
    const p1 = new Point(0.9, 0.9)

    expect(p1.floor()).toMatchObject({
      x: 0,
      y: 0,
    })
  })
})

describe('Point.subtract', () => {
  it('should subtract two numbers form point', () => {
    expect(Point.fromArray([2, 2]).subtract(2, 4)).toMatchObject({
      x: 0,
      y: -2,
    })
  })
})

describe('Point.toOneDimensionIndex', () => {
  it('should output index that can be used in one dimension array', () => {
    expect(Point.fromArray([2, 3]).toOneDimensionIndex(4)).toEqual(14)
  })
})

describe('Point.toArray', () => {
  it('should output array from point', () => {
    expect(Point.fromArray([2, 3]).toArray()).toEqual([2, 3])
  })
})

describe('Point.toObject', () => {
  it('should output array from point', () => {
    expect(Point.fromArray([2, 3]).toObject()).toEqual({ x: 2, y: 3 })
  })
})
