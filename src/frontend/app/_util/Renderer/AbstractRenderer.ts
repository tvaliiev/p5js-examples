import P5 from 'p5'
import { Color } from '../Color'
import { IPoint, Point } from '../Point'
import { range } from '../util'
import { IRenderer } from './IRenderer'

export interface IDimensions {
  x: number
  y: number
}

export abstract class AbstractRenderer implements IRenderer {
  public static NAME = ''

  disableContextMenu = false
  protected p5: P5

  setP5(p5: P5) {
    this.p5 = p5
  }

  getDimensions(): IDimensions {
    return {
      x: 600,
      y: 600,
    }
  }

  getSize(): Point {
    return Point.fromObject(this.getDimensions())
  }

  forEachPixel(cb: (x: number, y: number) => void) {
    range(this.p5.height).forEach(y => {
      range(this.p5.width).forEach(x => {
        cb(x, y)
      })
    })
  }

  drawPoints(points: Point[], color: Color): void {
    this.p5.loadPixels()
    points.forEach(point => point.drawAsPixel(this.p5, color))
    this.p5.updatePixels()
  }

  swap(a, b) {
    return [b, a]
  }

  public setup() {
    const dimensions = this.getDimensions()
    this.p5.createCanvas(dimensions.x, dimensions.y)
  }

  public mouseClicked(): void {}

  public mousePressed(): void {}

  public abstract draw(): void

  public preload(): void {}

  public keyPressed(): void {}

  public mouseDragged(): void {}
}
