import { AbstractRenderer, IDimensions } from './AbstractRenderer'
import { IRenderer } from './IRenderer'

export class EmptyRenderer extends AbstractRenderer implements IRenderer {
  public setup() {
    super.setup()

    this.p5.background(0).fill(255).text('Empty renderer', 30, 30).frameRate(1)
  }

  public draw(): void {}

  public getDimensions(): IDimensions {
    return { x: 200, y: 100 }
  }
}
