import P5 from 'p5'

export interface IRenderer {
  disableContextMenu: boolean

  setup(): void

  draw(): void

  mouseClicked(): void

  mousePressed(): void

  preload(): void

  keyPressed(): void

  mouseDragged(): void

  setP5(p5: P5): void
}
