import P5 from 'p5'
import { objectGet, objectSet } from '../util'
import { IRenderer } from './IRenderer'

export class RendererFactory {
  private static RENDERERS = {
    ['Coding Train']: {
      ['A-Star Pathfinding']: () =>
        import('../../Renderers/AStarPathfinding/AAStarPathfindingRenderer'),
      ['Barnsley ferm']: () =>
        import('../../Renderers/BarnsleyFerm/BarnsleyFernRenderer'),
      ['Brownian tree snowflake']: () =>
        import(
          '../../Renderers/BrownianTreeSnowflake/BrownianTreeSnowflakeRenderer'
        ),
      ['Chaos game']: () => import('../../Renderers/ChaosGame/ChaosGame'),
      ['Double pendulum']: () =>
        import('../../Renderers/DoublePendulum/DoublePendulum'),
      ['Evolutionary steering']: () =>
        import(
          '../../Renderers/EvolutionarySteering/EvolutionarySteeringRenderer'
        ),
      ['Flocking simulation']: () =>
        import('../../Renderers/FlockingSimulation/FlockingSimulationRenderer'),
      ['Fourier']: () => import('../../Renderers/Fourier/FourierRenderer'),
      ['Julia set']: () => import('../../Renderers/JuliaSet/JuliaSetRenderer'),
      ['Lissajous curve']: () =>
        import('../../Renderers/LissajousCurve/LissajousCurveRenderer'),
      ['Logo interpreter']: () =>
        import('../../Renderers/LogoInterpreter/LogoInterpreterRenderer'),
      ['Maze generator']: () =>
        import('../../Renderers/MazeGenerator/MazeGeneratorRenderer'),
      ['Random walker']: () =>
        import('../../Renderers/RandomWalkerVectored/RandomWalkerVectored'),
      ['Recursion renderer']: () =>
        import('../../Renderers/Recursion/RecursionRenderer'),
      ['Sandpiles']: () =>
        import('../../Renderers/SandPiles/SandpilesRenderer'),
      ['Steering behaviors']: () =>
        import('../../Renderers/SteeringBehaviors/SteeringBehaviorsRenderer'),
      ['Water ripple']: () =>
        import('../../Renderers/WaterRipple/WaterRippleRenderer'),
      ['Fractal Treee Space Colonization']: () =>
        import(
          '../../Renderers/FTSpaceColonization/FTSpaceColonizationRenderer'
        ),
      ['2D Supershape']: () =>
        import('../../Renderers/2DSupershapes/2DSupershapesRenderer'),
      ['Poisson-disc Sampling']: () =>
        import(
          '../../Renderers/PoissonDiscSampling/PoissonDiscSamplingRenderer'
        ),
      ['Diffusion limited aggregation']: () =>
        import(
          '../../Renderers/DiffusionLimitedAggregation/DiffusionLimitedAggregationRenderer'
        ),
      ['Cellular automata']: () =>
        import('../../Renderers/CellularAutomata/CellularAutomataRenderer'),
      ['Markov chains']: () =>
        import('../../Renderers/MarkovChains/MarkovChainsRenderer'),
      ['Prime Spiral']: () =>
        import('../../Renderers/PrimeSpiral/PrimeSpiralRenderer'),
      ['Hillbert Curve']: () =>
        import('../../Renderers/HilbertCurve/HilbertCurveRenderer'),
    },
    ['Datastructures']: {
      ['QuardTree']: () => import('../../Renderers/QuardTree/QuadTreeRenderer'),
      ['PerlinNoise']: () =>
        import('../../Renderers/Noises/PerlinNoise/PerlinNoiseRenderer'),
    },
    ['Games']: {
      ['2048']: () => import('../../Renderers/Game2048/Game2048Renderer'),
      ['Game of Life']: () =>
        import('../../Renderers/GameOfLife/GameOfLifeRenderer'),
      ['Minesweeper']: () =>
        import('../../Renderers/Minesweeper/MinesweeperRenderer'),
      ['Sudoku']: () => import('../../Renderers/Sudoku/SudokuRenderer'),
    },
    ['AI Experiments']: {
      ['Tensorflow']: {
        ['Basic']: () =>
          import(
            '../../Renderers/Tensorflow/TensorflowBasic/TensorflowBasicRenderer'
          ),
        ['XOR']: () =>
          import(
            '../../Renderers/Tensorflow/TensorFlowXOR/TensorFlowXORRenderer'
          ),
      },
      ['Basic Generic Algorithm']: () =>
        import(
          '../../Renderers/GeneticAlgorithm/BasicGeneticAlgorithmRenderer'
        ),
    },
  }

  private static DEFAULT_RENDERER = () => import('./EmptyRenderer')

  public static getRendererCategories() {
    const categories = {}

    function generateCategoriesTree(node, value: string[] = []) {
      Object.keys(node).forEach(key => {
        const newPath = [...value, key]
        if (typeof node[key] === 'function') {
          objectSet(categories, newPath, newPath)
          return
        }

        generateCategoriesTree(node[key], newPath)
      })

      return node
    }

    generateCategoriesTree(RendererFactory.RENDERERS)

    return categories
  }

  private static getRenderer(
    categoryPath: string | string[],
  ): () => Promise<any> {
    return (
      objectGet(RendererFactory.RENDERERS, categoryPath, '/') ||
      RendererFactory.DEFAULT_RENDERER
    )
  }

  private static async createRenderer(
    target: string | string[],
  ): Promise<IRenderer> {
    const moduleExports = await RendererFactory.getRenderer(target)()

    const keys = Object.keys(moduleExports)
    const Renderer = moduleExports[keys[keys.length - 1]]

    return new Renderer()
  }

  public static async createP5ForRenderer(
    rendererTag: string | string[],
    node,
  ): Promise<P5> {
    const renderer = await RendererFactory.createRenderer(rendererTag)
    const sketch = (p5: P5) => {
      renderer.setP5(p5)

      const events = [
        'setup',
        'draw',
        'mouseClicked',
        'mousePressed',
        'preload',
        'keyPressed',
        'mouseDragged',
      ]

      events.forEach(event => {
        p5[event] = (...args: any[]) => renderer[event](...args)
      })

      document.getElementById(node).addEventListener('contextmenu', e => {
        if (!renderer.disableContextMenu) {
          return
        }

        e.preventDefault()
        return false
      })
    }

    return new P5(sketch, node)
  }
}
