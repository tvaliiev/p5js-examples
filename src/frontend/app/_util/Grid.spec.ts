import { Direction } from './Direction'
import { Grid } from './Grid'
import { IPoint, Point } from './Point'

type CellData = { prop: string }

function gridFactory(size: IPoint = { x: 5, y: 5 }, hasData: boolean = true) {
  return new Grid<CellData>(size, pos =>
    hasData ? { prop: pos.toShortString() } : undefined,
  )
}

describe('Grid.constructor', () => {
  it('Should create new Grid', () => {
    const grid = gridFactory()

    expect(grid.size).toEqual({ x: 5, y: 5 })

    expect(Object.keys(grid.cells).length).toEqual(5)
    expect(Object.keys(grid.cells[0]).length).toEqual(5)

    expect(grid.cells[4][4].data).toStrictEqual({ prop: '(4, 4)' })
  })
})

describe('Grid.getCellAt', () => {
  it('Should get cell located at coordinate', () => {
    const grid = gridFactory()

    grid.cells[3][2].data.prop = 'other'

    expect(grid.getCellAt({ x: 2, y: 3 }).data.prop).toEqual('other')
  })
})

describe('Grid.getCellsAround', () => {
  it.each([
    [
      {
        target: { x: 0, y: 0 },
        expected: [
          { x: 1, y: 0 },
          { x: 0, y: 1 },
          { x: 1, y: 1 },
        ],
      },
    ],
    [
      {
        target: { x: 1, y: 1 },
        expected: [
          { x: 0, y: 0 },
          { x: 1, y: 0 },
          { x: 2, y: 0 },
          { x: 0, y: 1 },
          { x: 2, y: 1 },
          { x: 0, y: 2 },
          { x: 1, y: 2 },
          { x: 2, y: 2 },
        ],
      },
    ],
    [
      {
        target: { x: 1, y: 0 },
        expected: [
          { x: 0, y: 0 },
          { x: 2, y: 0 },
          { x: 0, y: 1 },
          { x: 1, y: 1 },
          { x: 2, y: 1 },
        ],
      },
    ],
  ])('Should get cells around cell', ({ target, expected }) => {
    const grid = gridFactory()
    const result = grid.getCellsAround(target)

    expect(result.length).toEqual(expected.length)
    expect(result).toStrictEqual(expected.map(e => grid.getCellAt(e)))
  })

  it.each([
    [
      {
        target: { x: 0, y: 0 },
        expected: [
          { x: 0, y: 0 },
          { x: 1, y: 0 },
          { x: 0, y: 1 },
          { x: 1, y: 1 },
        ],
      },
    ],
    [
      {
        target: { x: 1, y: 1 },
        expected: [
          { x: 0, y: 0 },
          { x: 1, y: 0 },
          { x: 2, y: 0 },
          { x: 0, y: 1 },
          { x: 1, y: 1 },
          { x: 2, y: 1 },
          { x: 0, y: 2 },
          { x: 1, y: 2 },
          { x: 2, y: 2 },
        ],
      },
    ],
    [
      {
        target: { x: 1, y: 0 },
        expected: [
          { x: 0, y: 0 },
          { x: 1, y: 0 },
          { x: 2, y: 0 },
          { x: 0, y: 1 },
          { x: 1, y: 1 },
          { x: 2, y: 1 },
        ],
      },
    ],
  ])('Should get cells around cell with middle', ({ target, expected }) => {
    const grid = gridFactory()
    const result = grid.getCellsAround(target, 1, true)

    expect(result.length).toEqual(expected.length)
    expect(result).toStrictEqual(expected.map(e => grid.getCellAt(e)))
  })
})

describe('Grid.forEachCell', () => {
  it('Should iterate through every cell', () => {
    const grid = gridFactory({ x: 2, y: 2 })

    const results = []
    grid.forEachCell(cell => {
      results.push(cell.data.prop)
    })

    expect(results).toStrictEqual(['(0, 0)', '(1, 0)', '(0, 1)', '(1, 1)'])
  })

  it('Should stop iterate when received false result in cb', () => {
    const grid = gridFactory({ x: 2, y: 2 })

    const results = []
    grid.forEachCell(cell => {
      if (cell.position.isEqual({ x: 0, y: 1 })) {
        return false
      }

      results.push(cell.data.prop)
    })

    expect(results).toStrictEqual(['(0, 0)', '(1, 0)'])
  })
})

describe('Grid.cellExistsAt', () => {
  it.each<{ cellPos: [number, number]; result: boolean }[]>([
    [{ cellPos: [-1, 0], result: false }],
    [{ cellPos: [0, -1], result: false }],
    [{ cellPos: [5, 0], result: false }],
    [{ cellPos: [0, 5], result: false }],
    [{ cellPos: [1, 1], result: true }],
  ])(
    'Should tell if coordinates is out of boundaries',
    ({ cellPos, result }) => {
      const grid = gridFactory()

      expect(grid.cellExistsAt({ x: cellPos[0], y: cellPos[1] })).toEqual(
        result,
      )
    },
  )
})

describe('Grid.randomCell', () => {
  const grid = gridFactory()

  it('should output randomCell without filter', () => {
    expect(grid.randomCell()).toBeTruthy()
  })

  it('should output null with impossible filter', () => {
    const filter = () => false
    expect(grid.randomCell(filter, 20)).toBeNull()
  })

  it('should output cell using filter', () => {
    const filter = cell => cell.position.isEqual({ x: 0, y: 0 })
    expect(grid.randomCell(filter, 500)).toMatchObject({
      position: {
        x: 0,
        y: 0,
      },
    })
  })
})

describe('Grid.getCellsAtDirections', () => {
  it.each<
    {
      point: [number, number]
      expected: {
        cell: { position: { x: number; y: number } }
        direction: Direction
      }[]
    }[]
  >([
    [
      {
        point: [0, 0],
        expected: [
          { cell: { position: { x: 1, y: 0 } }, direction: Direction.RIGHT },
          { cell: { position: { x: 0, y: 1 } }, direction: Direction.DOWN },
        ],
      },
    ],
    [
      {
        point: [1, 1],
        expected: [
          { cell: { position: { x: 1, y: 0 } }, direction: Direction.UP },
          { cell: { position: { x: 0, y: 1 } }, direction: Direction.LEFT },
          { cell: { position: { x: 2, y: 1 } }, direction: Direction.RIGHT },
          { cell: { position: { x: 1, y: 2 } }, direction: Direction.DOWN },
        ],
      },
    ],
  ])('should give points around with directions', ({ point, expected }) => {
    const grid = gridFactory(
      {
        x: 5,
        y: 5,
      },
      false,
    )

    expect(grid.getCellsAtDirections(Point.fromArray(point))).toMatchObject(
      expected,
    )
  })
})
