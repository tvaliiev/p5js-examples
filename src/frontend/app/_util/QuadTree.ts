import P5 from 'p5'
import { Boundary } from './Boundary'
import { Point } from './Point'

export enum QuadTreeDivideDir {
  TOP_LEFT = 'tl',
  TOP_RIGHT = 'tr',
  BOTTOM_LEFT = 'bl',
  BOTTOM_RIGHT = 'br',
}

type SubTree = { [key in QuadTreeDivideDir]?: QuadTree<any> }

type QTItem<R = any> = { point: Point; item: R }

export class QuadTree<R = any> {
  private debug = {
    boundaryCoords: 0,
    borders: 1,
    id: 0,
  }
  public static readonly DEFAULT_DIVIDE_THRESHOLD = 50
  private items: QTItem<R>[] = []
  private divided: boolean = false

  private subTrees: SubTree = {}

  /* istanbul ignore next */
  public getThreshold(): number {
    return this.threshold
  }

  constructor(
    private readonly boundary: Boundary,
    private readonly threshold = QuadTree.DEFAULT_DIVIDE_THRESHOLD,
    private readonly id: string = '',
  ) {}

  /* istanbul ignore next */
  public getId(): string {
    return this.id
  }

  /* istanbul ignore next */
  public getBoundary(): Boundary {
    return this.boundary
  }

  /* istanbul ignore next */
  public getSubTrees(): SubTree {
    return this.subTrees
  }

  /* istanbul ignore next */
  public getPoints(): QTItem[] {
    return this.items
  }

  public addPoint(point: Point, item?: R): boolean {
    if (!this.shouldHavePoint(point)) {
      return false
    }

    if (this.divided) {
      return Object.values(this.subTrees)
        .find(tree => tree.shouldHavePoint(point))
        .addPoint(point, item)
    }

    if (!this.items.find(p => p.point.isEqual(point))) {
      this.items.push({ point, item })
    }

    if (this.items.length >= this.threshold) {
      this.divide()
    }

    return true
  }

  private shouldHavePoint(point: Point) {
    return point.inBoundary(this.boundary)
  }

  /* istanbul ignore next */
  public draw(p5: P5) {
    if (this.divided) {
      Object.values(this.subTrees).forEach(tree => tree.draw(p5))
      return
    }

    if (this.debug.borders) {
      this.boundary.draw(p5)
    }

    if (this.id && this.debug.id) {
      const idWidth = this.id.length * 6
      p5.text(this.id, this.boundary.top_left.x, this.boundary.top_left.y + 10)
      p5.text(
        this.id,
        this.boundary.bottom_right.x - idWidth,
        this.boundary.bottom_right.y,
      )
    }

    if (this.debug.boundaryCoords) {
      const idWidth = this.id.length * 6
      p5.text(
        `${this.boundary.top_left.x},${this.boundary.top_left.y}`,
        this.boundary.top_left.x,
        this.boundary.top_left.y + 10,
      )
      let strbl = `${this.boundary.bottom_right.x},${this.boundary.bottom_right.y}`
      p5.text(
        strbl,
        this.boundary.bottom_right.x - strbl.length * 7,
        this.boundary.bottom_right.y,
      )
    }
  }

  /* istanbul ignore next */
  public isDivided(): boolean {
    return this.divided
  }

  public forceDivide(levels: number) {
    if (levels <= 0) {
      return
    }

    this.divide()

    Object.values(this.subTrees).forEach(tree => {
      tree.forceDivide(levels - 1)
    })
  }

  public divide(): void {
    if (this.divided) {
      throw new AlreadyDividedException('Already divided')
    }

    const centerPoint = new Point(
      this.boundary.top_left.x + this.boundary.width / 2,
      this.boundary.top_left.y + this.boundary.height / 2,
    )

    this.subTrees[QuadTreeDivideDir.TOP_LEFT] = new QuadTree(
      new Boundary(this.boundary.top_left, centerPoint),
      this.threshold,
      this.id ? `${this.id}-${QuadTreeDivideDir.TOP_LEFT}` : '',
    )
    this.subTrees[QuadTreeDivideDir.TOP_RIGHT] = new QuadTree(
      new Boundary(
        new Point(
          this.boundary.top_left.x + this.boundary.width / 2,
          this.boundary.top_left.y,
        ),
        new Point(
          this.boundary.bottom_right.x,
          this.boundary.bottom_right.y - this.boundary.height / 2,
        ),
      ),
      this.threshold,
      this.id ? `${this.id}-${QuadTreeDivideDir.TOP_RIGHT}` : '',
    )
    this.subTrees[QuadTreeDivideDir.BOTTOM_LEFT] = new QuadTree(
      new Boundary(
        new Point(
          this.boundary.top_left.x,
          this.boundary.bottom_right.y - this.boundary.height / 2,
        ),
        new Point(
          this.boundary.bottom_right.x - this.boundary.width / 2,
          this.boundary.bottom_right.y,
        ),
      ),
      this.threshold,
      this.id ? `${this.id}-${QuadTreeDivideDir.BOTTOM_LEFT}` : '',
    )
    this.subTrees[QuadTreeDivideDir.BOTTOM_RIGHT] = new QuadTree(
      new Boundary(centerPoint, this.boundary.bottom_right),
      this.threshold,
      this.id ? `${this.id}-${QuadTreeDivideDir.BOTTOM_RIGHT}` : '',
    )

    this.divided = true
    const items = this.items
    this.items = []

    items.forEach(({ point, item }) => this.addPoint(point, item))
  }

  public inMyBoundary(boundary: Boundary): boolean {
    return boundary.intersect(this.boundary)
  }

  public itemsInBoundary(boundary: Boundary): QTItem[] {
    if (!this.inMyBoundary(boundary)) {
      return []
    }

    if (this.divided) {
      return Object.values(this.subTrees)
        .map(tree => tree.itemsInBoundary(boundary))
        .flat(1)
    }

    return this.items.filter(item => boundary.containPoint(item.point))
  }
}

export class AlreadyDividedException extends Error {}
