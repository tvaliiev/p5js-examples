import P5 from 'p5'
import { IBoundary } from './IBoundary'
import { IColor } from './Color'
import { value } from './value'

export interface IPoint {
  x: number
  y: number
}

export class Point implements IPoint {
  constructor(
    public x: number,
    public y: number,
  ) {}

  public addPoint(other: IPoint): Point {
    return new Point(other.x + this.x, other.y + this.y)
  }

  public subtractPoint(other: IPoint): Point {
    return new Point(this.x - other.x, this.y - other.y)
  }

  public subtract(x: number, y: number): Point {
    return new Point(this.x - x, this.y - y)
  }

  public add(x: number, y: number): Point {
    return new Point(this.x + x, this.y + y)
  }

  public multiply(x: number, y: number): Point {
    return new Point(this.x * x, this.y * y)
  }

  public multiplyPoint(other: IPoint): Point {
    return new Point(this.x * other.x, this.y * other.y)
  }

  public dividePoint(other: IPoint): Point {
    return new Point(this.x / other.x, this.y / other.y)
  }

  public divide(x: number, y: number): Point {
    return new Point(this.x / x, this.y / y)
  }

  public drawAsPixel(p5: P5, color: IColor): void {
    const index = (this.x + this.y * p5.width) * 4

    p5.pixels[index] = color.r
    p5.pixels[index + 1] = color.g
    p5.pixels[index + 2] = color.b
    p5.pixels[index + 3] = color.a
  }

  public drawAsCircle(p5: P5, radius: number = 2): void {
    p5.circle(this.x, this.y, radius)
  }

  public drawAsPoint(p5: P5): void {
    p5.point(this.x, this.y)
  }

  public inBoundary(boundary: IBoundary): boolean {
    return (
      this.x >= boundary.top_left.x &&
      this.x <= boundary.bottom_right.x &&
      this.y >= boundary.top_left.y &&
      this.y <= boundary.bottom_right.y
    )
  }

  public isEqual(other: IPoint): boolean {
    return other.x === this.x && other.y === this.y
  }

  public getMagnitude(): number {
    return Math.sqrt(this.x * this.x + this.y * this.y)
  }

  public normalize(): Point {
    const len = this.getMagnitude()

    if (len !== 0) {
      return this.multiply(1 / len, 1 / len)
    }

    return this
  }

  public setMagnitude(m: number): Point {
    return this.normalize().multiply(m, m)
  }

  public getSquaredMagnitude(): number {
    return this.y * this.y + this.x * this.x
  }

  public copy(): Point {
    return new Point(this.x, this.y)
  }

  public limit(limit: number): Point {
    const result = this.clone()

    if (Math.abs(result.x) > limit) {
      result.x = result.x < 0 ? -1 * limit : limit
    }

    if (Math.abs(result.y) > limit) {
      result.y = result.y < 0 ? -1 * limit : limit
    }

    return result
  }

  public clone(): Point {
    return new Point(this.x, this.y)
  }

  public toString(): string {
    return `Point(${this.x}, ${this.y})`
  }

  public toArray(): [number, number] {
    return [this.x, this.y]
  }

  public toShortString(): string {
    return `(${this.x}, ${this.y})`
  }

  public static fromAngle(angle, length = 1): Point {
    return new Point(length * Math.cos(angle), length * Math.sin(angle))
  }

  public static fromObject(obj: IPoint): Point {
    return new Point(obj.x, obj.y)
  }

  public static fromArray(arr: [number, number]): Point {
    return new Point(arr[0], arr[1])
  }

  public static random(length = 1): Point {
    return Point.fromAngle(Math.random() * Math.PI * 2, length)
  }

  public static empty(): Point {
    return new Point(0, 0)
  }

  public when(
    condition: boolean | ((p: Point) => boolean),
    execute: (p: Point) => Point,
  ): Point {
    if (value(condition, null, this)) {
      return execute(this)
    }

    return this
  }

  public distance(other: Point): number {
    return Math.sqrt(this.distanceSquared(other))
  }

  public distanceSquared(other: Point): number {
    const x = this.x - other.x
    const y = this.y - other.y

    return x * x + y * y
  }

  public floor(): Point {
    return new Point(Math.floor(this.x), Math.floor(this.y))
  }

  public constrain(min: IPoint, max?: IPoint): Point
  public constrain(min: number, max?: number): Point
  public constrain(min: any, max?: any): Point {
    let x = this.x
    let y = this.y

    if (max === undefined) {
      max = min
      min = { x: 0, y: 0 }
    }

    if (typeof min === 'number' && typeof max === 'number') {
      ;[min, max] = [
        { x: min, y: max },
        { x: min, y: max },
      ]
    }

    if (x < min.x) x = min.x
    if (y < min.y) y = min.y

    if (x > max.x) x = max.x
    if (y > max.y) y = max.y

    return new Point(x, y)
  }

  toOneDimensionIndex(totalCols: number): number {
    return this.y * totalCols + this.x
  }

  toObject(): IPoint {
    return { x: this.x, y: this.y }
  }

  reduce<T>(reducer: (x: number, y: number) => T): T {
    return reducer(this.x, this.y)
  }
}
