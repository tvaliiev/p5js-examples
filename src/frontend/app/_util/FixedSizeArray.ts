export class FixedSizeArray<T> {
  private items: T[] = []

  constructor(private readonly size: number) {}

  public push(item: T): void {
    this.items.unshift(item)

    if (this.items.length > this.size) {
      this.items.pop()
    }
  }

  /* istanbul ignore next */
  public getItems(): T[] {
    return this.items
  }
}
