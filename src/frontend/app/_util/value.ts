export function value(v: any, ...applyArgs: any[]) {
  if (typeof v === 'function') {
    return v.apply(...applyArgs)
  }

  return v
}
