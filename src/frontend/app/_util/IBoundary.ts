import { IPoint } from './Point'

export interface IBoundary {
  top_left: IPoint
  bottom_right: IPoint
}
