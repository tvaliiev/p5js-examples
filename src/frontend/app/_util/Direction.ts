import { Point } from './Point'
import { randomBetween, randomFloat, randomInt } from './util'

export enum Direction {
  UP = 'up',
  DOWN = 'down',
  LEFT = 'left',
  RIGHT = 'right',
  UP_LEFT = 'up_left',
  UP_RIGHT = 'up_right',
  DOWN_LEFT = 'bottom_left',
  DOWN_RIGHT = 'bottom_right',
  CENTER = 'center',
}

export const DirectionMove: { [key in Direction]: Point } = {
  [Direction.UP]: new Point(0, -1),
  [Direction.DOWN]: new Point(0, 1),
  [Direction.LEFT]: new Point(-1, 0),
  [Direction.RIGHT]: new Point(1, 0),
  [Direction.UP_LEFT]: new Point(-1, -1),
  [Direction.UP_RIGHT]: new Point(1, -1),
  [Direction.DOWN_LEFT]: new Point(-1, 1),
  [Direction.DOWN_RIGHT]: new Point(1, 1),
  [Direction.CENTER]: new Point(0, 0),
}

export const DirectionMoveOpposite: { [key in Direction]: Point } = {
  [Direction.UP]: DirectionMove[Direction.UP].multiply(-1, -1),
  [Direction.DOWN]: DirectionMove[Direction.DOWN].multiply(-1, -1),
  [Direction.LEFT]: DirectionMove[Direction.LEFT].multiply(-1, -1),
  [Direction.RIGHT]: DirectionMove[Direction.RIGHT].multiply(-1, -1),
  [Direction.UP_LEFT]: DirectionMove[Direction.UP_LEFT].multiply(-1, -1),
  [Direction.UP_RIGHT]: DirectionMove[Direction.UP_RIGHT].multiply(-1, -1),
  [Direction.DOWN_LEFT]: DirectionMove[Direction.DOWN_LEFT].multiply(-1, -1),
  [Direction.DOWN_RIGHT]: DirectionMove[Direction.DOWN_RIGHT].multiply(-1, -1),
  [Direction.CENTER]: new Point(0, 0),
}

export const DirectionOpposite: { [key in Direction]: Direction } = {
  [Direction.UP]: Direction.DOWN,
  [Direction.DOWN]: Direction.UP,
  [Direction.LEFT]: Direction.RIGHT,
  [Direction.RIGHT]: Direction.LEFT,
  [Direction.UP_LEFT]: Direction.DOWN_RIGHT,
  [Direction.UP_RIGHT]: Direction.DOWN_LEFT,
  [Direction.DOWN_LEFT]: Direction.UP_RIGHT,
  [Direction.DOWN_RIGHT]: Direction.UP_LEFT,
  [Direction.CENTER]: Direction.CENTER,
}

export const DirectionGroups: { [key: string]: Direction[] } = {
  HV: [Direction.UP, Direction.LEFT, Direction.RIGHT, Direction.DOWN],
  HVC: [
    Direction.UP,
    Direction.LEFT,
    Direction.RIGHT,
    Direction.DOWN,
    Direction.CENTER,
  ],
  HVD: [
    Direction.UP_LEFT,
    Direction.UP,
    Direction.UP_RIGHT,
    Direction.LEFT,
    Direction.RIGHT,
    Direction.DOWN_LEFT,
    Direction.DOWN,
    Direction.DOWN_RIGHT,
  ],
  HVDC: [
    Direction.UP_LEFT,
    Direction.UP,
    Direction.UP_RIGHT,
    Direction.LEFT,
    Direction.RIGHT,
    Direction.DOWN_LEFT,
    Direction.DOWN,
    Direction.DOWN_RIGHT,
    Direction.CENTER,
  ],
}

export function randomDirection(
  distribution: () => number = () => randomInt(0, possibleDirections.length),
  possibleDirections: Direction[] = DirectionGroups.HV,
): Direction {
  return possibleDirections[distribution()]
}
