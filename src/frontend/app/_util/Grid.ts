import { Cell } from './Cell'
import { Direction, DirectionGroups, DirectionMove } from './Direction'
import {
  BreakException,
  ignoreBreakException,
} from './Exceptions/BreakException'
import { IPoint, Point } from './Point'
import { randomBetween, randomFloat, randomInt, range } from './util'

export class Grid<CellData> {
  public cells: { [y: number]: { [x: number]: Cell<CellData> } } = {}
  public size: Point

  constructor(
    size: IPoint,
    private cellDataFactory: (pos: Point) => CellData,
  ) {
    this.size = Point.fromObject(size)

    range(size.y).forEach(y => {
      this.cells[y] = {}

      range(size.x).forEach(x => {
        const pos = new Point(x, y)
        this.cells[y][x] = new Cell<CellData>(pos, cellDataFactory(pos))
      })
    })
  }

  public cellExistsAt(point: IPoint): boolean {
    return (
      point.x >= 0 &&
      point.x < this.size.x &&
      point.y >= 0 &&
      point.y < this.size.y
    )
  }

  public getCellsAtDirections(
    position: Point,
    directions: Direction[] = DirectionGroups.HV,
  ): { direction: Direction; cell: Cell<CellData> }[] {
    return directions.reduce(
      (
        cells: { direction: Direction; cell: Cell<CellData> }[],
        d: Direction,
      ) => {
        const nextCell = this.getNextCell(position, d)
        if (!nextCell) {
          return cells
        }

        cells.push({ cell: nextCell, direction: d })

        return cells
      },
      [],
    )
  }

  public getNextCell(
    currentPos: Point,
    direction: Direction,
  ): Cell<CellData> | undefined {
    const newPosition = currentPos.addPoint(DirectionMove[direction])
    if (!this.cellExistsAt(newPosition)) {
      return undefined
    }

    return this.getCellAt(newPosition)
  }

  public getCellAt(p: IPoint): Cell<CellData> | undefined {
    return this.cells[p.y][p.x]
  }

  public getCellsAround(
    position: IPoint,
    radius: number = 1,
    includeMiddleCell: boolean = false,
  ): Cell<CellData>[] {
    const result = []

    for (let y = position.y - radius; y <= position.y + radius; y++) {
      if (y < 0 || y >= this.size.y) {
        continue
      }

      for (let x = position.x - radius; x <= position.x + radius; x++) {
        if (x < 0 || x >= this.size.x) {
          continue
        }

        const current = this.getCellAt({ x, y })

        if (
          !includeMiddleCell &&
          (!current || current.position.isEqual(position))
        ) {
          continue
        }

        result.push(current)
      }
    }

    return result
  }

  public forEachCell(cb: (cell: Cell<CellData>) => false | void) {
    ignoreBreakException(() => {
      range(this.size.y).forEach(y => {
        range(this.size.x).forEach(x => {
          if (cb(this.cells[y][x]) === false) {
            throw new BreakException()
          }
        })
      })
    })
  }

  public randomCell(
    filter: (cell: Cell<CellData>) => boolean = () => true,
    tries = 1,
  ): Cell<CellData> | null {
    while (tries) {
      const randomIndex = {
        x: randomInt(this.size.x),
        y: randomInt(this.size.y),
      }

      const randomCell = this.getCellAt(randomIndex)
      if (filter(randomCell)) {
        return randomCell
      }

      tries--
    }

    return null
  }

  /* istanbul ignore next */
  public clone(): Grid<CellData> {
    const clone = new Grid(this.size, this.cellDataFactory)

    this.forEachCell(cell => {
      clone.getCellAt(cell.position).data = Object.assign({}, cell.data)
    })

    return clone
  }

  /* istanbul ignore next */
  public getRow(y: number): Cell<CellData>[] {
    return Object.values(this.cells[y])
  }

  /* istanbul ignore next */
  public getColumn(x: number): Cell<CellData>[] {
    const result = []
    for (const cellY in this.cells) {
      result.push(this.cells[cellY][x])
    }
    return result
  }

  /* istanbul ignore next */
  public forEachInColumn(x: number, cb: (cell: Cell<CellData>) => any) {
    for (const cellY in this.cells) {
      cb(this.cells[cellY][x])
    }
  }

  /* istanbul ignore next */
  public forEachInRow(y: number, cb: (cell: Cell<CellData>) => any) {
    Object.values(this.cells[y]).forEach(cb)
  }
}
