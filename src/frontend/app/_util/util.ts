import { Boundary } from './Boundary'
import { Direction, DirectionGroups } from './Direction'
import { IPoint, Point } from './Point'

export const range = (length: number, start: number = 0, adder: number = 1) =>
  Array.from({ length }, (_, i) => i * adder + start)

export function randomBetween(min: number, max: number) {
  if (min > max) {
    const tmp = min
    min = max
    max = tmp
  }

  return Math.round(Math.random() * (max - min) + min)
}

export function randomInt(min: number = 2, max = null) {
  return Math.floor(randomFloat(min, max))
}

export function randomFloat(min: number = null, max = null) {
  const rand = Math.random()

  if (min === null) {
    return rand
  }

  if (max === null) {
    return rand * min
  }

  if (min > max) {
    const tmp = min
    min = max
    max = tmp
  }

  return rand * (max - min) + min
}

export function randomElement<R>(
  items: R[],
): { element: R; index: number } | null {
  if (items.length === 0) {
    return null
  }
  const index = randomInt(items.length)
  return { element: items[index], index }
}

export const map = (
  value: number,
  fromMin: number,
  fromMax: number,
  toMin: number,
  toMax: number,
) => (toMax - toMin) * (value / (fromMax - fromMin)) + toMin

type Indent = {
  [key in Direction]?: number
}

export function randomPointAtEdges(
  screenSize: IPoint,
  indent: Indent | number,
): Point {
  if (typeof indent === 'number') {
    const defaultIdent = indent
    indent = {}

    DirectionGroups.HV.forEach(dir => {
      indent[dir] = defaultIdent
    })
  }

  const chosenElement = randomElement(Object.keys(indent))

  if (!chosenElement) {
    return null
  }

  const direction = chosenElement.element

  const boundaries = {
    [Direction.UP]: () =>
      new Boundary(
        Point.empty(),
        Point.fromArray([screenSize.x, indent[Direction.UP]]),
      ),
    [Direction.LEFT]: () =>
      new Boundary(
        Point.empty(),
        Point.fromArray([indent[Direction.LEFT], screenSize.y]),
      ),
    [Direction.RIGHT]: () =>
      new Boundary(
        Point.fromArray([screenSize.x - indent[Direction.RIGHT], 0]),
        Point.fromObject(screenSize),
      ),
    [Direction.DOWN]: () =>
      new Boundary(
        Point.fromArray([0, screenSize.y - indent[Direction.DOWN]]),
        Point.fromObject(screenSize),
      ),
  }

  if (!boundaries[direction]) {
    return null
  }

  return boundaries[direction]().randomPoint()
}

export function tap<R>(value: R, callback: (arg: R) => void) {
  callback(value)

  return value
}

export function isPrime(value: number): boolean {
  if (value <= 1) {
    return false
  }

  for (let i = 2; i <= Math.sqrt(value); i++) {
    if (value % i === 0) {
      return false
    }
  }

  return true
}

export function objectGet(
  obj: object,
  path: string | string[],
  separator = '.',
): any | undefined {
  if (typeof path === 'string') {
    path = path.split(separator)
  }

  const currentPathPart = path.shift()
  if (!currentPathPart) {
    return undefined
  }

  if (path.length === 0) {
    return obj[currentPathPart]
  }

  if (typeof obj[currentPathPart] === 'undefined') {
    return undefined
  }

  return objectGet(obj[currentPathPart], path)
}

export function objectSet(
  obj: object,
  path: string | string[],
  value,
  separator = '.',
): void {
  const properPath =
    typeof path === 'string' ? path.split(separator) : [...path]

  while (properPath.length > 1) {
    const currentPathPart = properPath.shift()

    if (typeof obj[currentPathPart] !== 'object') {
      obj[currentPathPart] = {}
    }
    obj = obj[currentPathPart]
  }

  if (properPath.length === 1) {
    obj[properPath[0]] = value
  }
}

let perlin
let perlin_octaves = 4
const PERLIN_SIZE = 4095
const PERLIN_YWRAPB = 4
const PERLIN_YWRAP = 1 << PERLIN_YWRAPB
const PERLIN_ZWRAPB = 8
const PERLIN_ZWRAP = 1 << PERLIN_ZWRAPB
let perlin_amp_falloff = 0.5
const scaled_cosine = i => 0.5 * (1.0 - Math.cos(i * Math.PI))
export function noise(x, y = 0, z = 0) {
  if (perlin == null) {
    perlin = new Array(PERLIN_SIZE + 1)
    for (let i = 0; i < PERLIN_SIZE + 1; i++) {
      perlin[i] = Math.random()
    }
  }

  if (x < 0) {
    x = -x
  }
  if (y < 0) {
    y = -y
  }
  if (z < 0) {
    z = -z
  }

  let xi = Math.floor(x),
    yi = Math.floor(y),
    zi = Math.floor(z)
  let xf = x - xi
  let yf = y - yi
  let zf = z - zi
  let rxf, ryf

  let r = 0
  let ampl = 0.5

  let n1, n2, n3

  for (let o = 0; o < perlin_octaves; o++) {
    let of = xi + (yi << PERLIN_YWRAPB) + (zi << PERLIN_ZWRAPB)

    rxf = scaled_cosine(xf)
    ryf = scaled_cosine(yf)

    n1 = perlin[of & PERLIN_SIZE]
    n1 += rxf * (perlin[(of + 1) & PERLIN_SIZE] - n1)
    n2 = perlin[(of + PERLIN_YWRAP) & PERLIN_SIZE]
    n2 += rxf * (perlin[(of + PERLIN_YWRAP + 1) & PERLIN_SIZE] - n2)
    n1 += ryf * (n2 - n1)

    of += PERLIN_ZWRAP
    n2 = perlin[of & PERLIN_SIZE]
    n2 += rxf * (perlin[(of + 1) & PERLIN_SIZE] - n2)
    n3 = perlin[(of + PERLIN_YWRAP) & PERLIN_SIZE]
    n3 += rxf * (perlin[(of + PERLIN_YWRAP + 1) & PERLIN_SIZE] - n3)
    n2 += ryf * (n3 - n2)

    n1 += scaled_cosine(zf) * (n2 - n1)

    r += n1 * ampl
    ampl *= perlin_amp_falloff
    xi <<= 1
    xf *= 2
    yi <<= 1
    yf *= 2
    zi <<= 1
    zf *= 2

    if (xf >= 1.0) {
      xi++
      xf--
    }
    if (yf >= 1.0) {
      yi++
      yf--
    }
    if (zf >= 1.0) {
      zi++
      zf--
    }
  }
  return r
}
