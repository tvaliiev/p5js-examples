import P5 from 'p5'
import { Point } from './Point'
import { map } from './util'

export class Vehicle {
  private acceleration: Point = new Point(0, 0)

  constructor(
    private target: Point,
    private position: Point = new Point(0, 0),
    private maxSpeed = 8,
    private maxForce = 1,
    private velocity: Point = new Point(0, 0),
  ) {}

  public behaviors(mousePosition: Point) {
    const arrive = this.arrive(this.target)
    const flee = this.flee(mousePosition).multiply(2, 2)

    this.applyForce(arrive)
    this.applyForce(flee)
  }

  private flee(target: Point): Point {
    const desired = target.subtractPoint(this.position)
    if (desired.getMagnitude() < 50) {
      return desired
        .setMagnitude(this.maxSpeed)
        .multiply(-1, -1)
        .subtractPoint(this.velocity)
    }

    return new Point(0, 0)
  }

  private arrive(target: Point): Point {
    let desired = target.subtractPoint(this.position)
    const d = desired.getMagnitude()
    let speed = this.maxSpeed
    if (d < 100) {
      speed = map(d, 0, 100, 0, this.maxSpeed)
    }

    return desired.setMagnitude(speed).subtractPoint(this.velocity)
  }

  private seek(target: Point): Point {
    return target
      .subtractPoint(this.position)
      .setMagnitude(this.maxSpeed)
      .subtractPoint(this.velocity)
  }

  public applyForce(f: Point) {
    this.acceleration = this.acceleration.addPoint(f).limit(this.maxForce)
  }

  public update() {
    this.position = this.position.addPoint(this.velocity)
    this.velocity = this.velocity.addPoint(this.acceleration)
    this.acceleration = new Point(0, 0)
  }

  public getPosition(): Point {
    return this.position
  }

  public drawAsPoint(p5: P5) {
    p5.point(this.position.x, this.position.y)
  }
}
