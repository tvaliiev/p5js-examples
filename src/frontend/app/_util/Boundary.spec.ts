import P5 from 'p5'
import { Boundary } from './Boundary'
import { Point } from './Point'

describe('Boundary.constructor', () => {
  it('Should set correct center, width, height', () => {
    const boundary = new Boundary(new Point(10, 10), new Point(20, 22))

    expect(boundary.width).toEqual(10)
    expect(boundary.height).toEqual(12)
    expect(boundary.center).toMatchObject(new Point(15, 16))
  })
})

describe('Boundary.containPoint', () => {
  const boundary = new Boundary(new Point(10, 10), new Point(20, 20))

  it.each([
    new Point(0, 0), // top left
    new Point(15, 0), // top
    new Point(25, 0), // top right
    new Point(0, 15), // left
    new Point(25, 15), // right
    new Point(0, 25), // bottom left
    new Point(15, 25), // bottom
    new Point(22, 25), // bottom right
  ])('Should report that boundary does not contain point', point => {
    expect(boundary.containPoint(point)).toBeFalsy()
  })

  it.each([
    new Point(10, 10),
    new Point(15, 10),
    new Point(15, 15),
    new Point(20, 19),
    new Point(15, 20),
    new Point(20, 20),
  ])(`Should report that %s in ${boundary}`, point => {
    expect(boundary.containPoint(point)).toBeTruthy()
  })
})

describe('Boundary.intersect', () => {
  const boundary = new Boundary(new Point(10, 10), new Point(20, 20))

  describe.each([
    new Boundary(new Point(0, 0), new Point(9, 9)), // top left
    new Boundary(new Point(11, 0), new Point(19, 9)), // top
    new Boundary(new Point(21, 0), new Point(19, 19)), // top right
    new Boundary(new Point(0, 10), new Point(9, 19)), // left
    new Boundary(new Point(21, 10), new Point(30, 20)), // right
    new Boundary(new Point(0, 20), new Point(9, 25)), // bottom left
    new Boundary(new Point(10, 21), new Point(20, 22)), // bottom
    new Boundary(new Point(21, 21), new Point(30, 30)), // bottom right
  ])(`Group: negative cases for ${boundary} and %s`, other => {
    it(`Should report that ${boundary} do not intersect ${other}`, () => {
      expect(boundary.intersect(other)).toBeFalsy()
    })
    it(`Should report that ${other} do not intersect ${boundary}`, () => {
      expect(other.intersect(boundary)).toBeFalsy()
    })
  })

  describe.each([
    new Boundary(new Point(0, 0), new Point(10, 10)),
    new Boundary(new Point(9, 9), new Point(11, 11)),
    new Boundary(new Point(10, 10), new Point(20, 20)),
    new Boundary(new Point(9, 9), new Point(21, 21)),
    new Boundary(new Point(15, 15), new Point(30, 30)),
  ])(`Group positive cases for ${boundary} and %s`, other => {
    it(`Should report that ${boundary} do not intersect ${other}`, () => {
      expect(boundary.intersect(other)).toBeTruthy()
    })
    it(`Should report that ${other} do not intersect ${boundary}`, () => {
      expect(other.intersect(boundary)).toBeTruthy()
    })
  })
})

describe('Boundary.draw', () => {
  it('Should properly draw rectangle', () => {
    const P5Mock = jest.fn(() => ({
      rect: jest.fn(),
    }))

    const p5Mock = new P5Mock()
    new Boundary(new Point(2, 1), new Point(5, 10)).draw(p5Mock as any as P5)

    expect(p5Mock.rect).toBeCalledWith(2, 1, 3, 9)
  })
})

describe('Boundary.toString', () => {
  it('Should output string', () => {
    expect(
      new Boundary(new Point(1, 2), new Point(3, 5)).toString(),
    ).toMatchInlineSnapshot(`"Boundary(Point(1, 2),Point(3, 5))"`)
  })
})

describe('Boundary.divideEvenly', () => {
  it('Should divide boundary into even chunks', () => {
    expect(
      new Boundary(
        Point.fromArray([20, 20]),
        Point.fromArray([80, 120]),
      ).divideEvenly(Point.fromArray([3, 4])),
    ).toMatchObject([
      new Boundary(Point.fromArray([20, 20]), Point.fromArray([40, 45])),
      new Boundary(Point.fromArray([40, 20]), Point.fromArray([60, 45])),
      new Boundary(Point.fromArray([60, 20]), Point.fromArray([80, 45])),

      new Boundary(Point.fromArray([20, 45]), Point.fromArray([40, 70])),
      new Boundary(Point.fromArray([40, 45]), Point.fromArray([60, 70])),
      new Boundary(Point.fromArray([60, 45]), Point.fromArray([80, 70])),

      new Boundary(Point.fromArray([20, 70]), Point.fromArray([40, 95])),
      new Boundary(Point.fromArray([40, 70]), Point.fromArray([60, 95])),
      new Boundary(Point.fromArray([60, 70]), Point.fromArray([80, 95])),

      new Boundary(Point.fromArray([20, 95]), Point.fromArray([40, 120])),
      new Boundary(Point.fromArray([40, 95]), Point.fromArray([60, 120])),
      new Boundary(Point.fromArray([60, 95]), Point.fromArray([80, 120])),
    ])
  })
})
