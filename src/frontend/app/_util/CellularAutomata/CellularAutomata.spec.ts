import { CellularAutomata } from './CellularAutomata'
import { CellularAutomataRule } from './CellularAutomataRule'

describe('CellularAutomata.getNextGeneration', () => {
  it.each<{ num: number; first: boolean[]; second: boolean[] }[]>([
    [{ num: 30, first: [true], second: [false] }],
    [
      {
        num: 30,
        first: [true, true, false, false, true],
        second: [false, false, true, true, false],
      },
    ],
    [
      {
        num: 150,
        first: [true, false, true, true, false, true],
        second: [false, false, false, false, false, false],
      },
    ],
  ])('should output future generation', ({ num, first, second }) => {
    const ca = new CellularAutomata(
      new CellularAutomataRule(CellularAutomataRule.DefinitionFromNumber(num)),
    )

    expect(ca.getNextGeneration(first)).toEqual(second)
  })
})
