import { Direction } from '../Direction'
import { CellularAutomataRule } from './CellularAutomataRule'

export class CellularAutomata {
  constructor(private rule: CellularAutomataRule) {}

  getNextGeneration(cells: boolean[]): boolean[] {
    return cells.map((item, index) => {
      if (index === 0 || cells[index + 1] === undefined) {
        return false
      }

      return this.rule.determineCellState(item, {
        [Direction.LEFT]: cells[index - 1] || false,
        [Direction.RIGHT]: cells[index + 1] || false,
      })
    })
  }
}
