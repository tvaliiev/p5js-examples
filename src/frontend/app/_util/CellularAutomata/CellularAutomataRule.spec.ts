import { Direction } from '../Direction'
import { CellularAutomataRule } from './CellularAutomataRule'

describe('CellularAutomataRule.DefinitionFromNumber', () => {
  it('should output string for some length', () => {
    expect(CellularAutomataRule.DefinitionFromNumber(255)).toEqual('11111111')
  })

  it('should prepend zeroes when needed', () => {
    expect(CellularAutomataRule.DefinitionFromNumber(30)).toEqual('00011110')
  })
})

describe('CellularAutomataRule.getStateMap', () => {
  it.each<{ number: number; expected: { [key: string]: boolean } }[]>([
    [
      {
        number: 30,
        expected: {
          ['000']: false,
          ['001']: true,
          ['010']: true,
          ['011']: true,
          ['100']: true,
          ['101']: false,
          ['110']: false,
          ['111']: false,
        },
      },
    ],
    [
      {
        number: 0,
        expected: {
          ['000']: false,
          ['001']: false,
          ['010']: false,
          ['011']: false,
          ['100']: false,
          ['101']: false,
          ['110']: false,
          ['111']: false,
        },
      },
    ],
    [
      {
        number: 255,
        expected: {
          ['000']: true,
          ['001']: true,
          ['010']: true,
          ['011']: true,
          ['100']: true,
          ['101']: true,
          ['110']: true,
          ['111']: true,
        },
      },
    ],
  ])(
    'should crete proper state map from definition',
    ({ number, expected }) => {
      expect(
        CellularAutomataRule.getStateMap(
          CellularAutomataRule.DefinitionFromNumber(number),
        ),
      ).toMatchObject(expected)
    },
  )
})

describe('CellularAutomataRule.constructor', () => {
  it('should throw an error when definition length is too high', () => {
    expect(() => {
      new CellularAutomataRule('00000000000000')
    }).toThrow(/definition should be no more than \d+ characters length/)
  })
})

describe('CellularAutomataRule.determineCellState', () => {
  it.each<{ definition: number; args: boolean[]; expected: boolean }[]>([
    [{ definition: 30, args: [true, true, true], expected: false }],
    [{ definition: 30, args: [true, true, false], expected: false }],
    [{ definition: 30, args: [true, false, true], expected: false }],
    [{ definition: 30, args: [true, false, false], expected: true }],
    [{ definition: 30, args: [false, true, true], expected: true }],
    [{ definition: 30, args: [false, true, false], expected: true }],
    [{ definition: 30, args: [false, false, true], expected: true }],
    [{ definition: 30, args: [false, false, false], expected: false }],

    [{ definition: 54, args: [true, true, true], expected: false }],
    [{ definition: 54, args: [true, true, false], expected: false }],
    [{ definition: 54, args: [true, false, true], expected: true }],
    [{ definition: 54, args: [true, false, false], expected: true }],
    [{ definition: 54, args: [false, true, true], expected: false }],
    [{ definition: 54, args: [false, true, false], expected: true }],
    [{ definition: 54, args: [false, false, true], expected: true }],
    [{ definition: 54, args: [false, false, false], expected: false }],
  ])(
    'should determine cell state correctly',
    ({ definition, args, expected }) => {
      const caRule = new CellularAutomataRule(
        CellularAutomataRule.DefinitionFromNumber(definition),
      )

      const cellState = caRule.determineCellState(args[1], {
        [Direction.LEFT]: args[0],
        [Direction.RIGHT]: args[2],
      })

      expect(cellState).toEqual(expected)
    },
  )
})
