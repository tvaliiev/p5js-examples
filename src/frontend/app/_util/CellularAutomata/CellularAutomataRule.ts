import { Direction } from '../Direction'

export class CellularAutomataRule {
  public readonly possibleStates: number
  readonly #stateMap: { [key: string]: boolean }

  get stateMap() {
    return this.#stateMap
  }

  constructor(definition: string, dimensions = 1) {
    this.possibleStates = 2 ** (3 * dimensions)
    if (definition.length > this.possibleStates) {
      throw new Error(
        `definition should be no more than ${this.possibleStates} characters length`,
      )
    }

    this.#stateMap = CellularAutomataRule.getStateMap(definition, dimensions)
  }

  public static getStateMap(definition: string, dimensions: number = 1) {
    const stateMap = {}
    const possibilities = this.getPossibilitiesCount(dimensions)
    definition
      .padStart(possibilities, '0')
      .split('')
      .forEach((state, i) => {
        stateMap[
          CellularAutomataRule.DefinitionFromNumber(
            possibilities - i - 1,
            3 * dimensions,
          )
        ] = state === '1'
      })

    return stateMap
  }

  public static getPossibilitiesCount(dimensions: number) {
    return 2 ** (3 * dimensions)
  }

  public static DefinitionFromNumber(num: number, possibilities = 8): string {
    return (num >>> 0).toString(2).padStart(possibilities, '0')
  }

  determineCellState(
    cell: boolean,
    neighbours: {
      [Direction.LEFT]: boolean
      [Direction.RIGHT]: boolean
    },
  ): boolean {
    return this.stateMap[
      this.statesToStateMapKey([
        neighbours[Direction.LEFT],
        cell,
        neighbours[Direction.RIGHT],
      ])
    ]
  }

  statesToStateMapKey(arr: boolean[]): string {
    return arr.map(v => (v ? '1' : '0')).join('')
  }
}
