import { Grid } from './Grid'
import { IPoint } from './Point'
import { noise } from './util'

export class PerlinNoiseGrid extends Grid<number> {
  constructor(
    size: IPoint,
    private noiseScale: number = 0.01,
    private noiseLevel: number = 1,
  ) {
    super(size, position => {
      return (
        noiseLevel *
        noise(...position.multiply(noiseScale, noiseScale).toArray())
      )
    })
  }

  public getScale(): number {
    return this.noiseScale
  }

  public getLevel(): number {
    return this.noiseLevel
  }
}
