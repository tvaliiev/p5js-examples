import { value } from './value'

describe('value.value', () => {
  it('should execute function if provided or return plain value', () => {
    const callback = jest.fn()
    value(callback, { thisArg: 1 }, [1, 2, 3])
    expect(callback).toBeCalledWith(1, 2, 3)
  })
})
