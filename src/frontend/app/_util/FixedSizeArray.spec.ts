import { FixedSizeArray } from './FixedSizeArray'

describe('FixedSizeArray.push', () => {
  it('Should push to array successfully', () => {
    const arr = new FixedSizeArray<Number>(20)

    arr.push(20)

    expect(arr.getItems()).toMatchObject([20])
  })

  it('Should push no more then required maximum', () => {
    const arr = new FixedSizeArray<Number>(2)

    arr.push(21)
    arr.push(22)
    arr.push(23)

    expect(arr.getItems()).toMatchObject([23, 22])
  })

  it('Should replace existing element when size is 1', () => {
    const arr = new FixedSizeArray<Number>(1)

    arr.push(21)
    arr.push(22)
    arr.push(23)

    expect(arr.getItems()).toMatchObject([23])
  })

  it('Should not take any elements when size <= 0', () => {
    const arr = new FixedSizeArray<Number>(0)
    const arr2 = new FixedSizeArray<Number>(-1)

    arr.push(21)
    arr.push(22)
    arr.push(23)

    arr2.push(-21)
    arr2.push(22)
    arr2.push(-23)

    expect(arr.getItems().length).toEqual(0)
    expect(arr2.getItems().length).toEqual(0)
  })
})
