import { Point } from './Point'

export class Cell<CellData> {
  constructor(
    public position: Point,
    public data: CellData,
  ) {}

  isEqualPosition(other: Cell<CellData>) {
    return this.position.isEqual(other.position)
  }
}
