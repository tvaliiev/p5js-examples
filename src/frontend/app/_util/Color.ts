/* istanbul ignore next */
export interface IColor {
  r: number
  g: number
  b: number
  a?: number
}

/* istanbul ignore next */
export class Color implements IColor {
  constructor(
    public readonly r: number = 0,
    public readonly g: number = 0,
    public readonly b: number = 0,
    public readonly a: number = 255,
  ) {}

  toColorArgs(): number[] {
    return [this.r, this.g, this.b, this.a]
  }
}
