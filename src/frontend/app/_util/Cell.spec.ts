import { Cell } from './Cell'
import { Point } from './Point'

describe('Cell.isEqualPosition', () => {
  it.each<{ cellA: [number, number]; cellB: [number, number] }[]>([
    [{ cellA: [0, 1], cellB: [0, 0] }],
    [{ cellA: [0, 0], cellB: [1, 0] }],
    [{ cellA: [0, 0], cellB: [1, 1] }],
  ])('Should output false when position is not equal', ({ cellA, cellB }) => {
    const a = new Cell(new Point(cellA[0], cellA[1]), { test: 'a' })
    const b = new Cell(new Point(cellB[0], cellB[1]), { test: 'b' })

    expect(a.isEqualPosition(b)).toEqual(false)
  })

  it('Should output true when position is equal', () => {
    const a = new Cell(new Point(0, 0), { test: 'a' })
    const b = new Cell(new Point(0, 0), { test: 'b' })

    expect(a.isEqualPosition(b)).toEqual(true)
  })
})
