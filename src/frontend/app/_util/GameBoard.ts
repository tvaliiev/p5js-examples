import P5 from 'p5'
import { Boundary } from './Boundary'
import { Cell } from './Cell'
import { Color } from './Color'
import { Direction, DirectionGroups, DirectionMove } from './Direction'
import { Grid } from './Grid'
import { IPoint, Point } from './Point'

export class CellConfig {
  constructor(
    public width: number = 40,
    public height: number = 40,
    public background: Color = new Color(44, 44, 44),
  ) {}
}

export class GameBoardConfig {
  constructor(
    public spacing: number = 20,
    public cellConfig: CellConfig = new CellConfig(),
    public background: Color = new Color(22, 22, 22),
  ) {}
}

export abstract class GameBoard<CellData> {
  protected grid: Grid<CellData>

  constructor(
    protected size: IPoint = { x: 1, y: 1 },
    protected config: GameBoardConfig = new GameBoardConfig(),
  ) {}

  public init() {
    this.grid = this.getNewGrid()
  }

  public changeSizeToFit(dimensions: IPoint) {
    const d = Point.fromObject(dimensions).subtractPoint({
      x: this.config.spacing,
      y: this.config.spacing,
    })

    this.size = {
      x: Math.floor(d.x / (this.config.cellConfig.width + this.config.spacing)),
      y: Math.floor(
        d.y / (this.config.cellConfig.height + this.config.spacing),
      ),
    }
  }

  protected abstract defaultCellFactory(pos: Point): CellData

  protected getNewGrid(
    cellDataFactory: (pos: Point) => CellData = null,
  ): Grid<CellData> {
    return new Grid<CellData>(
      this.size,
      cellDataFactory || this.defaultCellFactory.bind(this),
    )
  }

  public getTotalWidth(): number {
    return (
      this.grid.size.x * (this.config.cellConfig.width + this.config.spacing) +
      this.config.spacing
    )
  }

  public getTotalHeight() {
    return (
      this.grid.size.y * (this.config.cellConfig.height + this.config.spacing) +
      this.config.spacing
    )
  }

  public draw(p5: P5) {
    const cellConfig = this.config.cellConfig

    const { background: boardBg } = this.config
    p5.fill(boardBg.r, boardBg.g, boardBg.b)
      .stroke(boardBg.r, boardBg.g, boardBg.b)
      .rect(0, 0, this.getTotalWidth(), this.getTotalHeight(), 3)

    const { background: cellBg } = this.config.cellConfig
    this.grid.forEachCell(cell => {
      const [x, y] = [
        cell.position.x * (cellConfig.width + this.config.spacing) +
          this.config.spacing,
        cell.position.y * (cellConfig.height + this.config.spacing) +
          this.config.spacing,
      ]

      if (cellBg.a != 0) {
        p5.fill(cellBg.r, cellBg.g, cellBg.b).stroke(
          cellBg.r,
          cellBg.g,
          cellBg.b,
        )
        p5.rect(x, y, cellConfig.width, cellConfig.height)
      }

      this.drawCell(p5, { x, y }, cell)
    })
  }

  protected drawCell(p5: P5, coordinates: IPoint, cell: Cell<CellData>) {}

  public abstract tick()

  public getCellFromAbsCoord(p: IPoint): Cell<CellData> | undefined {
    const cellConfig = this.config.cellConfig

    const x = Math.floor(
      (p.x - this.config.spacing * 2) /
        (cellConfig.width + this.config.spacing),
    )
    const y = Math.floor(
      (p.y - this.config.spacing * 2) /
        (cellConfig.height + this.config.spacing),
    )

    if (!this.grid.cellExistsAt({ x, y })) {
      return undefined
    }

    return this.grid.getCellAt({ x, y })
  }

  public getCellAbsCoordinates(cell: Cell<CellData>): Point {
    return cell.position
      .multiply(
        this.config.cellConfig.width + this.config.spacing,
        this.config.cellConfig.height + this.config.spacing,
      )
      .add(this.config.spacing, this.config.spacing)
  }

  public getCellFromGridCoord(p: IPoint): Cell<CellData> | undefined {
    return this.grid.getCellAt(p)
  }

  public getCellsAround(cell: Cell<CellData>): Cell<CellData>[] {
    return this.grid.getCellsAround(cell.position)
  }

  public getCellBoundary(cell: Cell<CellData>): Boundary {
    const cellTopLeft = this.getCellAbsCoordinates(cell)
    return new Boundary(
      cellTopLeft,
      cellTopLeft.addPoint({
        x: this.config.cellConfig.width,
        y: this.config.cellConfig.height,
      }),
    )
  }

  public clear() {
    this.init()
  }
}
