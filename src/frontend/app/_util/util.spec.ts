import { Direction } from './Direction'
import { Point } from './Point'

import {
  randomBetween,
  range,
  map,
  randomElement,
  randomFloat,
  randomPointAtEdges,
  tap,
  randomInt,
  isPrime,
  objectGet,
  objectSet,
} from './util'

describe('util.range', () => {
  it.each([
    [[0], []],
    [[-1], []],
    [
      [3, 1, 0],
      [1, 1, 1],
    ],
    [
      [3, 1, 20],
      [1, 21, 41],
    ],
    [
      [3, 1, -2],
      [1, -1, -3],
    ],
  ])(`Should create array for input %p output %p`, (input, output) => {
    // @ts-ignore
    expect(range(...input)).toEqual(output)
  })
})

describe('util.randomInt', () => {
  it('Should create random number if provided equal min and max', () => {
    for (let i = 0; i < 100; i++) {
      expect(randomBetween(1, 1)).toEqual(1)
    }
  })

  it.each([
    [0, 10],
    [10, 0],
    [-10, 0],
    [-5, 5],
    [-20, -5],
  ])('Should create random number between %d and %d', (min, max) => {
    let sum = 0
    const runs = 500
    let hasMins = false
    let hasMaxes = false
    for (let i = 0; i < runs; i++) {
      let random = randomBetween(min, max)

      sum += random

      if (random === min) {
        hasMins = true
      } else if (random === max) {
        hasMaxes = true
      }

      expect(random).toBeGreaterThanOrEqual(Math.min(min, max))
      expect(random).toBeLessThanOrEqual(Math.max(min, max))
    }

    const middle = (max + min) / 2
    // Test for normal distribution
    const range = (Math.abs(min) + Math.abs(max)) * 0.05
    expect(sum / runs).toBeGreaterThan(middle - range)
    expect(sum / runs).toBeLessThan(middle + range)

    expect(hasMaxes).toBeTruthy()
    expect(hasMins).toBeTruthy()
  })

  it('should give value between 0 and 1 with no arguments with avg 0.5', () => {
    let sum = 0
    range(300).forEach(() => {
      const random = randomInt()

      expect(random).toBeLessThanOrEqual(1)
      expect(random).toBeGreaterThanOrEqual(0)

      sum += random
    })
    sum /= 300

    expect(sum).toBeGreaterThan(0.4)
    expect(sum).toBeLessThan(0.6)
  })
})

describe('util.map', () => {
  it.each([
    [5, 0, 20, 10, 30, 15],
    [5, 0, 20, 100, 300, 150],
    [5, 0, 20, -20, 0, -15],
    [5, 0, 20, 0, 20, 5],
  ])(
    'Should map to correct values (v, fmin, fmax, tmin, tmax, result) = (%d, %d, %d, %d, %d, %d)',
    (value, fromMin, fromMax, toMin, toMax, result) => {
      expect(map(value, fromMin, fromMax, toMin, toMax)).toBeCloseTo(result, 5)
    },
  )
})

describe('util.randomElement', () => {
  it('should output null when no elements', () => {
    expect(randomElement([])).toEqual(null)
  })

  it('should output element and index when there is only one element', () => {
    expect(randomElement([5])).toMatchObject({
      index: 0,
      element: 5,
    })
  })

  it('should choose elements with equal distribution', () => {
    const elementCount = 5
    const totalRuns = 2000
    const map = {}
    const elements = range(elementCount).map(() => 123.2)
    elements.forEach((v, i) => {
      map[i] = 0
    })

    range(totalRuns).forEach(() => {
      const { index } = randomElement(elements)
      map[index]++
    })

    expect(Object.keys(map).length).toEqual(elementCount)
    Object.keys(map).forEach(index => {
      const perElement = totalRuns / elementCount
      expect(map[index]).toBeGreaterThan(perElement - perElement * 0.15)
    })
  })
})

describe('util.randomFloat', () => {
  it('should output random between 0 and 1 when 0 arguments', () => {
    range(100).forEach(() => {
      const random = randomFloat()
      expect(random).toBeLessThanOrEqual(1)
      expect(random).toBeGreaterThanOrEqual(0)
    })
  })

  it('should output random between 0 and min when 1 argument', () => {
    let sum = 0
    const runs = 300
    range(runs).forEach(() => {
      const random = randomFloat(20)
      sum += random

      expect(random).toBeGreaterThanOrEqual(0)
      expect(random).toBeLessThanOrEqual(20)
    })
    sum /= runs

    expect(sum).toBeGreaterThan(8.5)
    expect(sum).toBeLessThan(11.5)
  })

  it('should output random between min and max when 2 arguments', () => {
    let sum = 0
    range(100).forEach(() => {
      const random = randomFloat(20, 40)
      sum += random

      expect(random).toBeGreaterThanOrEqual(20)
      expect(random).toBeLessThanOrEqual(40)
    })

    expect(sum).toBeGreaterThan(2850)
    expect(sum).toBeLessThan(3150)
  })

  it('should output random between min and max when 2 arguments (swapped min and max)', () => {
    let sum = 0
    range(100).forEach(() => {
      const random = randomFloat(40, 20)
      sum += random

      expect(random).toBeGreaterThanOrEqual(20)
      expect(random).toBeLessThanOrEqual(40)
    })

    expect(sum).toBeGreaterThan(2850)
    expect(sum).toBeLessThan(3150)
  })

  it('should output 0 when given 0', () => {
    expect(randomFloat(0)).toEqual(0)
  })

  it('should give negative number when given negative ranges', () => {
    let sum = 0
    range(100).forEach(() => {
      const random = randomFloat(-40, -20)
      sum += random

      expect(random).toBeGreaterThanOrEqual(-40)
      expect(random).toBeLessThanOrEqual(-20)
    })

    expect(sum).toBeGreaterThan(-3150)
    expect(sum).toBeLessThan(-2850)
  })
})

describe('util.randomPointAtEdges', () => {
  it('should output random point at four edges when given screenSize and indent', () => {
    let sum = Point.empty()
    const iterations = 500
    range(iterations).forEach(() => {
      sum = sum.addPoint(randomPointAtEdges({ x: 20, y: 200 }, 2))
    })
    sum = sum.divide(iterations, iterations)

    expect(sum.x).toBeGreaterThan(8)
    expect(sum.x).toBeLessThan(12)
    expect(sum.y).toBeGreaterThan(80)
    expect(sum.y).toBeLessThan(120)
  })

  it('should output random point specified edges when provided indent as object', () => {
    let sum = Point.empty()
    const iterations = 500
    range(iterations).forEach(() => {
      sum = sum.addPoint(
        randomPointAtEdges({ x: 40, y: 200 }, { [Direction.UP]: 20 }),
      )
    })
    sum = sum.divide(iterations, iterations)

    expect(sum.x).toBeGreaterThan(18)
    expect(sum.x).toBeLessThan(22)
    expect(sum.y).toBeGreaterThan(8)
    expect(sum.y).toBeLessThan(12)
  })

  it('should return null when provided empty object as indent', () => {
    expect(randomPointAtEdges({ x: 2, y: 2 }, {})).toBeNull()
  })

  it('should return null when chosen unsupported indent', () => {
    expect(
      randomPointAtEdges({ x: 2, y: 2 }, { [Direction.UP_LEFT]: 20 }),
    ).toBeNull()
  })
})

describe('util.tap', () => {
  it('should call callback and return first argument regardless', () => {
    const callback = jest.fn().mockReturnValue(23)
    expect(tap(3, callback)).toEqual(3)
    expect(callback).toHaveBeenCalledWith(3)
  })
})

describe('util.isPrime', () => {
  it.each<[number, boolean]>([
    [0, false],
    [1, false],
    [2, true],
    [3, true],
    [4, false],
    [5, true],
    [6, false],
    [7, true],
    [8, false],
    [9, false],
    [10, false],
    [11, true],
    [12, false],
    [13, true],
    [14, false],
  ])('should correctly determine if number %p is prime', (num, answer) => {
    expect(isPrime(num)).toEqual(answer)
  })
})

describe('util.objectGet', () => {
  it('should return undefined if provided empty object and any path', () => {
    expect(objectGet({}, 'test')).toBeUndefined()
    expect(objectGet({}, ['test'])).toBeUndefined()
  })

  it('should return property value in simple case', () => {
    expect(objectGet({ test: 2 }, ['test'])).toEqual(2)
  })

  it('should travel to nested values with default separator', () => {
    expect(objectGet({ test: { test2: 'awd' } }, ['test', 'test2'])).toEqual(
      'awd',
    )
    expect(objectGet({ test: { test2: 'awd' } }, 'test.test2')).toEqual('awd')
    expect(objectGet({ test: { test2: 'awd' } }, 'test/test2', '/')).toEqual(
      'awd',
    )
  })

  it('should return undefined when path is invalid or value is undefined', () => {
    expect(
      objectGet({ test: { test2: undefined } }, ['test', 'test2']),
    ).toBeUndefined()
    expect(
      objectGet({ test: { test2: undefined } }, ['test', 'test3']),
    ).toBeUndefined()
    expect(
      objectGet({ test: { test2: undefined } }, [
        'test',
        'test3',
        'test4',
        'test5',
      ]),
    ).toBeUndefined()

    expect(
      objectGet({ test: { test2: undefined } }, 'test.test2'),
    ).toBeUndefined()
    expect(
      objectGet({ test: { test2: undefined } }, 'test.test3'),
    ).toBeUndefined()
    expect(
      objectGet({ test: { test2: undefined } }, 'test.test3.test4.test5'),
    ).toBeUndefined()

    expect(
      objectGet({ test: { test2: undefined } }, 'test|test2', '|'),
    ).toBeUndefined()
    expect(
      objectGet({ test: { test2: undefined } }, 'test|test3', '|'),
    ).toBeUndefined()
    expect(
      objectGet({ test: { test2: undefined } }, 'test|test3|test4|test5', '|'),
    ).toBeUndefined()
  })

  it('should work when path is empty array or empty string with or without separator provided', () => {
    expect(objectGet({ test: { test2: undefined } }, '', '|')).toBeUndefined()
    expect(objectGet({ test: { test2: undefined } }, '')).toBeUndefined()
    expect(objectGet({ test: { test2: undefined } }, [], '|')).toBeUndefined()
    expect(objectGet({ test: { test2: undefined } }, [])).toBeUndefined()
  })
})

describe('util.objectSet', () => {
  it('should correctly set value in empty object', () => {
    const obj = {}
    objectSet(obj, 'test.value', 'value')
    expect(obj).toEqual({ test: { value: 'value' } })
    const obj2 = {}
    objectSet(obj2, 'test/value', 'value', '/')
    expect(obj2).toEqual({ test: { value: 'value' } })
    const obj3 = {}
    objectSet(obj3, 'test/value/test2', 'value', '/')
    expect(obj3).toEqual({ test: { value: { test2: 'value' } } })
    const obj4 = {}
    objectSet(obj4, ['test', 'value', 'test2'], 'value')
    expect(obj4).toEqual({ test: { value: { test2: 'value' } } })
  })

  it('should not change existing properties in object', () => {
    const obj = { test3: 'value3' }
    objectSet(obj, 'test.value', 'value')
    expect(obj).toEqual({ test: { value: 'value' }, test3: 'value3' })
    const obj2 = { test3: 'value3' }
    objectSet(obj2, 'test/value', 'value', '/')
    expect(obj2).toEqual({ test: { value: 'value' }, test3: 'value3' })
    const obj3 = { test3: 'value3' }
    objectSet(obj3, 'test/value/test2', 'value', '/')
    expect(obj3).toEqual({
      test: { value: { test2: 'value' } },
      test3: 'value3',
    })
    const obj4 = { test3: 'value3' }
    objectSet(obj4, ['test', 'value', 'test2'], 'value')
    expect(obj4).toEqual({
      test: { value: { test2: 'value' } },
      test3: 'value3',
    })
  })

  it('should replace property correctly', () => {
    const obj = { test3: 'value3' }
    objectSet(obj, 'test3.value', 'value')
    expect(obj).toEqual({ test3: { value: 'value' } })
    const obj2 = { test3: 'value3', test2: 'value2' }
    objectSet(obj2, 'test3.value', 'value')
    expect(obj2).toEqual({ test3: { value: 'value' }, test2: 'value2' })
  })

  it('should work when path and value is the same variable', () => {
    const obj4 = { test3: 'value3' }
    const path = ['test', 'value', 'test2']
    objectSet(obj4, path, path)
    expect(obj4).toEqual({
      test: { value: { test2: ['test', 'value', 'test2'] } },
      test3: 'value3',
    })
  })
})
