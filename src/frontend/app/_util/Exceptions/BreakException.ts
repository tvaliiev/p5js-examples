export class BreakException extends Error {
  constructor(private result: any = false) {
    super()
  }

  getResult() {
    return this.result
  }
}

export function ignoreBreakException(cb: Function, ...args): any {
  try {
    return cb(...args)
  } catch (e) {
    if (e instanceof BreakException) {
      return e.getResult()
    }

    throw e
  }
}
