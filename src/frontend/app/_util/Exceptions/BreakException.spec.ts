import { BreakException, ignoreBreakException } from './BreakException'
import { CustomException } from './CustomException'

describe('ignoreBreakException', () => {
  it('Should execute callback with args and return data from it if everything fine', () => {
    const callback = jest.fn()
    ignoreBreakException(callback, 1, 2, 3)

    expect(callback).toBeCalledWith(1, 2, 3)
  })

  it('Should ignore BreakException and get result from it', () => {
    const callback = jest.fn(() => {
      throw new BreakException(123)
    })

    const result = ignoreBreakException(callback)

    expect(callback).toBeCalled()
    expect(result).toEqual(123)
  })

  it('Should throw other type of error', () => {
    const callback = jest.fn(() => {
      throw new CustomException()
    })

    expect(() => ignoreBreakException(callback)).toThrow(CustomException)
  })
})
