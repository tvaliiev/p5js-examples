import { Boundary } from '../../_util/Boundary'
import { Point } from '../../_util/Point'
import { QuadTree } from '../../_util/QuadTree'
import { AbstractRenderer, IDimensions, IRenderer } from '../../_util/Renderer'
import { range } from '../../_util/util'
import { Particle } from './Particle'

export class BrownianTreeSnowflakeRenderer
  extends AbstractRenderer
  implements IRenderer
{
  public static NAME = 'brownian-tree-snowflake'

  private current: Particle
  private snowflake: Particle[] = []
  private input
  private inputValue = 6

  public getDimensions(): IDimensions {
    return { x: 900, y: 900 }
  }

  public setup() {
    super.setup()

    const { p5 } = this
    this.input = p5.createSlider(2, 50, this.inputValue, 1)
    this.reset()
  }

  private reset() {
    this.current = this.getParticle()
    this.snowflake = []
    this.inputValue = this.input.value()
  }

  private getParticle() {
    return new Particle(new Point(this.p5.width / 2, 0))
  }

  public draw() {
    const { p5 } = this

    if (this.inputValue !== this.input.value()) {
      this.reset()
    }

    p5.translate(p5.width / 2, p5.height / 2).background(51)

    p5.fill(255).stroke(255)

    let count = 0
    while (
      !this.current.finished() &&
      !this.current.intersects(this.snowflake)
    ) {
      this.current.update()
      count++
    }

    if (count !== 0) {
      this.snowflake.push(this.current)
      this.current = this.getParticle()
    }

    range(this.inputValue).forEach(i => {
      p5.rotate(p5.PI / (this.inputValue / 2))
      this.current.draw(p5)
      this.snowflake.forEach(p => {
        p.draw(p5)
      })

      p5.push()
      p5.scale(1, -1)
      this.current.draw(p5)
      this.snowflake.forEach(p => {
        p.draw(p5)
      })
      p5.pop()
    })
  }
}
