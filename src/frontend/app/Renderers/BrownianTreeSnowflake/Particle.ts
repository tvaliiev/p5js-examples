import P5 from 'p5'
import { Point } from '../../_util/Point'
import { randomBetween } from '../../_util/util'

export class Particle {
  constructor(
    public position: Point,
    private readonly diameter = 6,
  ) {}

  public update() {
    this.position = this.position.add(-1, randomBetween(-1, 1))
  }

  public draw(p5: P5) {
    p5.circle(this.position.x, this.position.y, this.diameter)
  }

  public finished(): boolean {
    return this.position.x <= 0
  }

  public intersects(others: Particle[]): boolean {
    for (let i = 0; i < others.length; i++) {
      const other = others[i]
      const distance = other.position.distance(this.position)

      if (distance < this.diameter) {
        return true
      }
    }

    return false
  }
}
