import { Point } from '../../_util/Point'
import { AbstractRenderer } from '../../_util/Renderer'
import { SudokuGameBoard } from './SudokuGameBoard'

export class SudokuRenderer extends AbstractRenderer {
  private sudokuBoard: SudokuGameBoard = new SudokuGameBoard()

  public setup() {
    super.setup()
    this.sudokuBoard.init()
    this.sudokuBoard.insertNumbersRandomly()
    // this.sudokuBoard.placeNumberOn(1, {x: 0, y: 0})
    //this.p5.noLoop()
  }

  public draw(): void {
    const { p5 } = this
    p5.background(200, 200, 200)

    this.sudokuBoard.draw(p5)
  }

  public mouseClicked() {
    this.sudokuBoard.handleClick(
      Point.fromArray([this.p5.mouseX, this.p5.mouseY]),
    )
  }
}
