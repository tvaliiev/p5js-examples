import P5 from 'p5'
import { Boundary } from '../../_util/Boundary'
import { Cell } from '../../_util/Cell'
import { CellConfig, GameBoard, GameBoardConfig } from '../../_util/GameBoard'
import { IPoint, Point } from '../../_util/Point'
import { randomElement, range } from '../../_util/util'

export interface SudokuCellData {
  possibleMoves: number[]
  placedNumber: number | null
}

export class SudokuGameBoard extends GameBoard<SudokuCellData> {
  constructor() {
    super({ x: 9, y: 9 }, new GameBoardConfig(2, new CellConfig(60, 60)))
  }

  protected defaultCellFactory(pos: Point): SudokuCellData {
    return {
      possibleMoves: range(9, 1),
      placedNumber: null,
    }
  }

  public draw(p5: P5) {
    super.draw(p5)

    p5.push()
    p5.stroke(255, 255, 255)
    const totalHeight =
      this.grid.size.y * (this.config.cellConfig.width + this.config.spacing)
    const totalWidth =
      this.grid.size.x * (this.config.cellConfig.width + this.config.spacing)
    for (let i = 1; i < this.grid.size.x; i++) {
      if (i % 3 === 0) {
        p5.strokeWeight(0.4)
      } else {
        p5.strokeWeight(0.2)
      }

      p5.line(
        i * (this.config.cellConfig.width + this.config.spacing) + 1,
        0,
        i * (this.config.cellConfig.width + this.config.spacing) + 1,
        totalHeight,
      )
      p5.line(
        0,
        i * (this.config.cellConfig.height + this.config.spacing) + 1,
        totalWidth,
        i * (this.config.cellConfig.height + this.config.spacing) + 1,
      )
    }
    p5.pop()
  }

  protected drawCell(p5: P5, coordinates: IPoint, cell: Cell<SudokuCellData>) {
    super.drawCell(p5, coordinates, cell)

    if (cell.data.placedNumber) {
      p5.push()
      p5.textAlign(p5.CENTER, p5.CENTER)
      p5.textSize(30)
      p5.fill(200, 200, 200)
      p5.text(
        cell.data.placedNumber,
        coordinates.x + this.config.cellConfig.width / 2,
        coordinates.y + this.config.cellConfig.height / 2,
      )
      p5.pop()
      return
    }

    const hoveredCell = this.getCellFromAbsCoord({ x: p5.mouseX, y: p5.mouseY })
    if (hoveredCell && cell.isEqualPosition(hoveredCell)) {
      this.drawHoveredCellMenu(p5, coordinates, cell)
      return
    }
  }

  private drawHoveredCellMenu(
    p5: P5,
    coordinates: IPoint,
    cell: Cell<SudokuCellData>,
  ) {
    p5.push()
    p5.textAlign(p5.CENTER, p5.CENTER)
    p5.textSize(15)
    const subCellWidth = this.config.cellConfig.width / 3
    const subCellHeight = this.config.cellConfig.height / 3
    range(9).forEach(i => {
      const subX = coordinates.x + Math.floor(i % 3) * subCellWidth
      const subY = coordinates.y + Math.floor(i / 3) * subCellHeight

      if (cell.data.placedNumber === null) {
        if (cell.data.possibleMoves.find(_ => _ === i + 1)) {
          p5.stroke(120, 120, 120).fill(120, 120, 120)
        } else {
          p5.stroke(55, 55, 55).fill(55, 55, 55)
        }

        p5.text(i + 1, subX + subCellWidth / 2, subY + subCellHeight / 2)
      } else {
        p5.text(
          i + 1,
          subX + this.config.cellConfig.width / 2,
          subY + this.config.cellConfig.height / 2,
        )
      }
    })
    p5.pop()
  }

  public tick() {}

  public handleClick(clickedCoord: Point) {
    const cell = this.getCellFromAbsCoord(clickedCoord)
    if (!cell) {
      return
    }

    const rect = this.getCellBoundary(cell)

    // if clicked between cells (in spacing
    if (!rect.containPoint(clickedCoord)) {
      return
    }

    const clickedSubSquareIndex = rect
      .divideEvenly(Point.fromArray([3, 3]))
      .findIndex(b => b.containPoint(clickedCoord))
    const neededNumber = clickedSubSquareIndex + 1

    if (cell.data.possibleMoves.findIndex(o => o === neededNumber) === -1) {
      return
    }

    this.placeNumberOn(neededNumber, cell.position)
  }

  public placeNumberOn(num: number, placeOn: IPoint) {
    if (num < 1 || num > 9) {
      throw new Error('number that inserter should be between 1 and 9')
    }

    if (placeOn.x < 0 || placeOn.x > 8) {
      throw new Error('invalid placeOn.x')
    }

    if (placeOn.y < 0 || placeOn.y > 8) {
      throw new Error('invalid placeOn.x')
    }

    const cell = this.getCellFromGridCoord(placeOn)
    cell.data.placedNumber = num

    this.getSquareNeighborsFromCell(cell).forEach(
      c => (c.data.possibleMoves = c.data.possibleMoves.filter(_ => _ !== num)),
    )
    this.grid.forEachInRow(
      cell.position.y,
      c => (c.data.possibleMoves = c.data.possibleMoves.filter(_ => _ !== num)),
    )
    this.grid.forEachInColumn(
      cell.position.x,
      c => (c.data.possibleMoves = c.data.possibleMoves.filter(_ => _ !== num)),
    )
  }

  public getSquareNeighborsFromCell(
    cell: Cell<SudokuCellData>,
  ): Cell<SudokuCellData>[] {
    const squareMiddlePosition = Point.fromObject({
      x: Math.floor(cell.position.x / 3),
      y: Math.floor(cell.position.y / 3),
    })
      .multiply(3, 3)
      .add(1, 1)

    return this.grid.getCellsAround(squareMiddlePosition, 1, true)
  }

  public insertNumbersRandomly(count: number = 8) {
    range(count).forEach(i => {
      const cell = this.grid.randomCell(
        cell => cell.data.placedNumber === null,
        20,
      )
      this.placeNumberOn(
        randomElement(cell.data.possibleMoves).element,
        cell.position,
      )
    })
  }
}
