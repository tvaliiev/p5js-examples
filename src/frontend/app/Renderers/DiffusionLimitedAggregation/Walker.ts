import P5 from 'p5'
import { Direction } from '../../_util/Direction'
import {
  BreakException,
  ignoreBreakException,
} from '../../_util/Exceptions/BreakException'
import { IPoint, Point } from '../../_util/Point'
import { randomFloat, randomPointAtEdges } from '../../_util/util'

export class Walker {
  public position: Point
  private stuck: boolean
  private hu
  private readonly radiusSquared: number

  constructor(
    private screenSize: Point,
    private radius: number,
    position: IPoint = null,
  ) {
    if (position) {
      this.position = Point.fromObject(position)
      this.stuck = true
    } else {
      this.position = randomPointAtEdges(screenSize, 2)
      this.stuck = false
    }

    this.radiusSquared = this.radius ** 2
  }

  walk() {
    const vel = Point.random()

    this.position = this.position.addPoint(vel).constrain(this.screenSize)
  }

  checkStuck(others: Walker[]) {
    ignoreBreakException(() => {
      others.forEach(other => {
        const d = other.position.distanceSquared(this.position)
        if (
          d <
          this.radiusSquared + other.radiusSquared + other.radius * this.radius
        ) {
          this.stuck = true
          throw new BreakException()
        }
      })
    })

    return this.stuck
  }

  setHue(hu: any) {
    this.hu = hu
  }

  forceStuck() {
    this.stuck = true
    this.hu = 0
  }

  show(p5: P5) {
    p5.noStroke()
    if (this.stuck && typeof this.hu !== 'undefined') {
      p5.fill(this.hu, 255, 100, 200)
    } else {
      p5.fill(360, 0, 255)
    }

    p5.circle(this.position.x, this.position.y, this.radius * 2)
  }
}
