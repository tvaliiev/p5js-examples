import { Boundary } from '../../_util/Boundary'
import { Point } from '../../_util/Point'
import { QuadTree } from '../../_util/QuadTree'
import { AbstractRenderer } from '../../_util/Renderer'
import { range } from '../../_util/util'
import { Walker } from './Walker'

export class DiffusionLimitedAggregationRenderer extends AbstractRenderer {
  private radius = 8
  private initialRadius
  private shrinkAmount = 0.999
  private radiusSquared: number
  private hu = 0

  private tree: Walker[] = []
  private quadTree: QuadTree<Walker>
  private maxWalkers = 200

  private walkers: Walker[] = []

  private getCanvasSize(): Point {
    return Point.fromArray([this.p5.width, this.p5.height])
  }

  public setup() {
    super.setup()

    this.p5.colorMode(this.p5.HSB)

    this.initialRadius = this.radius

    this.quadTree = new QuadTree<Walker>(
      new Boundary(Point.empty(), this.getCanvasSize()),
    )

    this.tree.push(
      new Walker(
        this.getCanvasSize(),
        this.radius,
        this.getCanvasSize().divide(2, 2),
      ),
    )
    this.tree[0].forceStuck()
    this.quadTree.addPoint(this.tree[0].position, this.tree[0])

    this.shrinkRadius()

    range(this.maxWalkers).forEach(i => {
      this.addWalker()
    })
  }

  private addWalker() {
    this.walkers.push(new Walker(this.getCanvasSize(), this.radius))
    this.shrinkRadius()
  }

  private shrinkRadius() {
    this.changeRadius(this.radius * this.shrinkAmount)
  }

  private changeRadius(newRadius: number) {
    this.radius = newRadius
    this.radiusSquared = this.radius ** 2
  }

  public draw() {
    const { p5 } = this

    p5.background(0)

    this.tree.forEach(w => {
      w.show(p5)
    })

    this.walkers.forEach(w => {
      w.show(p5)
    })
    this.tree.forEach(w => {})

    range(300).forEach(() => {
      for (let i = this.walkers.length - 1; i >= 0; i--) {
        const walker = this.walkers[i]
        walker.walk()
        const items = this.quadTree
          .itemsInBoundary(
            new Boundary(
              walker.position.subtract(
                this.initialRadius * 2,
                this.initialRadius * 2,
              ),
              walker.position.add(
                this.initialRadius * 2,
                this.initialRadius * 2,
              ),
            ),
          )
          .map(({ item }) => item)

        if (walker.checkStuck(items)) {
          walker.setHue(this.hu % 360)
          this.hu += 0.5
          this.quadTree.addPoint(walker.position, walker)
          this.tree.push(walker)
          this.walkers.splice(i, 1)
        }
      }
    })

    while (this.walkers.length < this.maxWalkers && this.radius > 2) {
      this.addWalker()
    }
  }
}
