import { Cell } from '../../_util/Cell'
import { DirectionGroups } from '../../_util/Direction'
import { Grid } from '../../_util/Grid'
import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions } from '../../_util/Renderer'

interface CellData {
  f: number
  g: number
  h: number
  previous: Cell<CellData>
  wall: boolean
}

export class AAStarPathfindingRenderer extends AbstractRenderer {
  public static NAME = 'a-star-pathfinding'

  private gridSize = new Point(50, 50)
  private grid: Grid<CellData>

  private openSet: Cell<CellData>[] = []
  private closedSet: Cell<CellData>[] = []

  private start: Cell<CellData>
  private end: Cell<CellData>

  private drawCellSize: Point

  public getDimensions(): IDimensions {
    return { x: 800, y: 800 }
  }

  public setup() {
    super.setup()

    const { p5 } = this

    this.grid = new Grid<CellData>(this.gridSize, () => ({
      f: 0,
      g: 0,
      h: 0,
      previous: undefined,
      wall: p5.random(1) < 0.4,
    }))

    this.start = this.grid.getCellAt({ x: 0, y: 0 })
    this.end = this.grid.getCellAt(this.gridSize.subtractPoint({ x: 1, y: 1 }))
    this.end.data.wall = false

    this.openSet.push(this.start)

    this.drawCellSize = new Point(p5.width, p5.height).dividePoint(
      this.gridSize,
    )
  }

  public draw() {
    const { p5 } = this

    if (this.openSet.length <= 0) {
      console.log('no solution')
      p5.noLoop()
      return
    }
    const current = this.openSet.reduce((winner, cell) => {
      if (cell.data.f < winner.data.f) {
        return cell
      }
      return winner
    }, this.openSet[0])

    if (current.position.isEqual(this.end.position)) {
      console.log('DONE!')
      p5.noLoop()
      return
    }

    p5.background(255)

    this.openSet = this.openSet.filter(e => !e.isEqualPosition(current))
    this.closedSet.push(current)

    this.grid
      .getCellsAtDirections(current.position, DirectionGroups.HVD)
      .forEach(({ cell: neighbour }) => {
        if (this.closedSet.find(e => e.isEqualPosition(neighbour))) {
          return
        }

        if (
          !this.openSet.find(e => e.isEqualPosition(neighbour)) &&
          !neighbour.data.wall
        ) {
          const nextGScore = current.data.g + this.heuristic(neighbour, current)

          let newPathBetter = false
          if (this.openSet.includes(neighbour)) {
            if (nextGScore < neighbour.data.g) {
              neighbour.data.g = nextGScore
              newPathBetter = true
            }
          } else {
            neighbour.data.g = nextGScore
            newPathBetter = true
            this.openSet.push(neighbour)
          }

          if (newPathBetter) {
            neighbour.data.h = this.heuristic(neighbour, this.end)
            neighbour.data.f = neighbour.data.g + neighbour.data.h
            neighbour.data.previous = current
          }
        }
      })

    this.grid.forEachCell(cell => {
      this.drawCell(cell, p5.color(255))
    })

    this.closedSet.forEach(cell => {
      this.drawCell(cell, p5.color(100, 0, 0))
    })

    this.openSet.forEach(cell => {
      this.drawCell(cell, p5.color(0, 100, 0))
    })

    const path = [current]
    let temp = current
    while (temp.data.previous) {
      path.push(temp.data.previous)
      temp = temp.data.previous
    }
    p5.noFill()
      .stroke(255, 0, 200)
      .strokeWeight(this.drawCellSize.x / 2)
      .beginShape()
    path.forEach(cell => {
      const realPosTopLeft = cell.position.multiplyPoint(this.drawCellSize)
      const halfOfCellSize = this.drawCellSize.divide(2, 2)
      const cellMiddle = realPosTopLeft.addPoint(halfOfCellSize)

      p5.vertex(cellMiddle.x, cellMiddle.y)
    })

    p5.endShape()
  }

  public heuristic(a: Cell<CellData>, b: Cell<CellData>): number {
    return a.position.distance(b.position)
  }

  public drawCell(cell: Cell<CellData>, color, stroke: boolean = false): void {
    const { p5 } = this
    const realPosTopLeft = cell.position.multiplyPoint(this.drawCellSize)
    if (cell.data.wall) {
      const halfOfCellSize = this.drawCellSize.divide(2, 2)
      const cellMiddle = realPosTopLeft.addPoint(halfOfCellSize)
      p5.fill(0)
        .noStroke()
        .ellipse(cellMiddle.x, cellMiddle.y, halfOfCellSize.x, halfOfCellSize.y)
    } else {
      p5.fill(color)
        .noStroke()
        .rect(
          realPosTopLeft.x,
          realPosTopLeft.y,
          this.drawCellSize.x,
          this.drawCellSize.y,
        )
    }
  }
}
