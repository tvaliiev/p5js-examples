import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions } from '../../_util/Renderer'
import { IRenderer } from '../../_util/Renderer'
import { range } from '../../_util/util'
import { Circle } from './Circle'
import { Curve } from './Curve'

export class LissajousCurveRenderer
  extends AbstractRenderer
  implements IRenderer
{
  public static NAME = 'lissajous-curve'

  private circlesX: Circle[] = []
  private circlesY: Circle[] = []
  private circleSpacing = 30
  private circleRadius = 70
  private angle = 0
  private curves: Curve[][] = []

  public getDimensions(): IDimensions {
    return { x: 800, y: 800 }
  }

  public setup() {
    super.setup()

    const width = this.circleRadius + this.circleSpacing
    range(this.p5.width / width - 1).forEach(i => {
      this.circlesX.push(
        new Circle(
          this.p5,
          new Point(width + i * width - 1 + width / 2, width / 2),
          this.circleRadius,
        ),
      )
    })

    range(this.p5.height / width - 1).forEach(i => {
      this.circlesY.push(
        new Circle(
          this.p5,
          new Point(width / 2, width + i * width - 1 + width / 2),
          this.circleRadius,
        ),
      )
    })

    this.circlesY.forEach((_, y) => {
      this.circlesX.forEach((__, x) => {
        if (!this.curves[y]) {
          this.curves[y] = []
        }

        this.curves[y][x] = new Curve(this.p5)
      })
    })

    // this.p5.frameRate(20)
  }

  public draw(): void {
    this.p5.background(0).noFill().stroke(255)

    this.circlesX.forEach((circle, x) => {
      circle
        .setAngle(this.angle * (x + 1) - this.p5.HALF_PI)
        .drawXPointLine()
        .draw()

      this.circlesY.forEach((_, y) => {
        this.curves[y][x].setCurrentX(circle.getPoint().x)
      })
    })

    this.circlesY.forEach((circle, y) => {
      circle
        .setAngle(this.angle * (y + 1) - this.p5.HALF_PI)
        .drawYPointLine()
        .draw()

      this.circlesX.forEach((_, x) => {
        this.curves[y][x].setCurrentY(circle.getPoint().y)
      })
    })

    this.circlesY.forEach((_, y) => {
      this.circlesX.forEach((__, x) => {
        this.curves[y][x].addPoint().draw()
      })
    })

    this.angle += 0.01
  }
}
