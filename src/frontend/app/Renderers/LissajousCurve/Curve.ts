import P5 from 'p5'
import { FixedSizeArray } from '../../_util/FixedSizeArray'
import { Point } from '../../_util/Point'

export class Curve {
  private path: FixedSizeArray<Point> = new FixedSizeArray<Point>(700)
  private current: Point = new Point(0, 0)

  constructor(private p5: P5) {}

  public setCurrentX(x: number) {
    this.current.x = x
  }

  public setCurrentY(y: number) {
    this.current.y = y
  }

  public addPoint(): Curve {
    this.path.push(this.current)
    return this
  }

  public draw() {
    this.p5.beginShape().stroke(255).strokeWeight(1)

    this.path.getItems().forEach(point => {
      this.p5.vertex(point.x, point.y)
    })

    this.p5.endShape().strokeWeight(6).point(this.current.x, this.current.y)
    this.current = new Point(0, 0)
  }
}
