import P5 from 'p5'
import { Point } from '../../_util/Point'

export class Circle {
  private readonly radius: number
  private angle: number
  private point: Point

  constructor(
    private p5: P5,
    public readonly position: Point,
    public readonly diameter = 80,
  ) {
    this.radius = diameter / 2
    this.setAngle(0)
  }

  public setAngle(angle: number): Circle {
    this.angle = angle
    this.point = this.calculatePoint(angle).addPoint(this.position)

    return this
  }

  public getPoint(): Point {
    return this.point
  }

  public draw() {
    this.p5
      .strokeWeight(1)
      .stroke(255)
      .circle(this.position.x, this.position.y, this.diameter)
      .strokeWeight(5)
      .point(this.point.x, this.point.y)
  }

  private drawPointLine(coordinate: 'x' | 'y') {
    this.p5.strokeWeight(1).stroke(255, 30)

    if (coordinate === 'x') {
      this.p5.line(this.point.x, 0, this.point.x, this.p5.height)
    } else {
      this.p5.line(0, this.point.y, this.p5.width, this.point.y)
    }
  }

  public drawXPointLine(): Circle {
    this.drawPointLine('x')
    return this
  }

  public drawYPointLine(): Circle {
    this.drawPointLine('y')
    return this
  }

  private calculatePoint(angle: number): Point {
    return new Point(
      this.radius * this.p5.cos(angle),
      this.radius * this.p5.sin(angle),
    )
  }
}
