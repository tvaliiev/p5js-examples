import P5 from 'p5'
import { Cell } from '../../_util/Cell'
import { Direction, DirectionMove } from '../../_util/Direction'
import { GameBoard, GameBoardConfig, CellConfig } from '../../_util/GameBoard'
import { Grid } from '../../_util/Grid'
import { IPoint, Point } from '../../_util/Point'
import { Color } from '../../_util/Color'

type CellData = {
  mined: boolean
  minesAround: number
  isOpen: boolean
  isFlagged: boolean
}

export class MinesweeperGameBoard extends GameBoard<CellData> {
  protected isWin: boolean | null = null

  constructor() {
    super(
      undefined,
      new GameBoardConfig(3, new CellConfig(15, 15), new Color(22, 22, 22)),
    )
  }

  protected defaultCellFactory(pos: Point): CellData {
    return {
      mined: false,
      minesAround: 0,
      isOpen: false,
      isFlagged: false,
    }
  }

  public randomizeCells() {
    this.placeMinesRandomly()
    this.recalculateMinesAround()
  }

  public placeMinesRandomly() {
    this.grid.forEachCell(cell => {
      cell.data.mined = Math.random() < 0.15
    })
  }

  public recalculateMinesAround() {
    this.grid.forEachCell(cell => {
      const cells = this.grid.getCellsAround(cell.position)
      cell.data.minesAround = cells.reduce(
        (minesAround, cell) => minesAround + (cell.data.mined ? 1 : 0),
        0,
      )
    })
  }

  protected drawCell(p5: P5, coordinates: IPoint, cell: Cell<CellData>) {
    const cellWidth = this.config.cellConfig.width
    const cellHeight = this.config.cellConfig.height
    if (cell.data.isFlagged) {
      p5.fill(200, 0, 0).triangle(
        coordinates.x + cellWidth / 2,
        coordinates.y,
        coordinates.x,
        coordinates.y + cellHeight,
        coordinates.x + cellWidth,
        coordinates.y + cellHeight,
      )
      return
    }

    if (!cell.data.isOpen) {
      return
    }

    const mineWidth = Math.floor(cellWidth / 2)
    if (cell.data.mined) {
      p5.fill(255, 0, 0).square(
        coordinates.x + cellWidth / 2 - mineWidth / 2,
        coordinates.y + cellHeight / 2 - mineWidth / 2,
        mineWidth,
      )
    } else {
      p5.fill(0, 200, 0)
      p5.textSize(9)
      p5.textAlign(p5.CENTER)
      p5.text(
        cell.data.minesAround,
        coordinates.x + cellWidth / 2,
        coordinates.y + cellHeight / 2 + 3,
      )
    }
  }

  public draw(p5: P5) {
    if (this.isWin !== null) {
      p5.fill(0, 255, 0)
      p5.stroke(0, 255, 0)
      p5.textAlign(p5.CENTER).textSize(30)
      p5.text(
        `You ${this.isWin ? 'WON!!' : 'lost :('}`,
        this.getTotalWidth() / 2,
        this.getTotalHeight() / 2,
      )
      return
    }
    super.draw(p5)
  }

  public openCell(realCoords: IPoint) {
    const cell = this.getCellFromAbsCoord(realCoords)
    if (!cell || cell.data.isFlagged) {
      return
    }

    if (cell.data.mined) {
      this.isWin = false
      return
    }

    this.autoOpenCell(cell)

    const { openCells, mines } = this.getGridStats()
    if (openCells === mines) {
      this.isWin = true
    }
  }

  public flagCell(realCoords: IPoint) {
    const cell = this.getCellFromAbsCoord(realCoords)
    if (!cell || cell.data.isOpen) {
      return
    }

    cell.data.isFlagged = !cell.data.isFlagged
  }

  private autoOpenCell(cell: Cell<CellData>, cache: boolean[][] = null) {
    if (cache === null) {
      cache = Array.from({ length: this.grid.size.y }, () =>
        Array.from({ length: this.grid.size.x }),
      )
    }

    if (cache[cell.position.y][cell.position.x] == true || !cell) {
      return
    }
    cache[cell.position.y][cell.position.x] = true

    if (cell.data.mined === true) {
      return
    }

    cell.data.isOpen = true

    if (cell.data.minesAround > 0) {
      return
    }

    this.getCellsAround(cell).forEach(cell => this.autoOpenCell(cell, cache))
  }

  private getGridStats(): { openCells: number; mines: number } {
    let result = {
      openCells: 0,
      mines: 0,
    }

    this.grid.forEachCell(cell => {
      result.openCells += cell.data.isOpen ? 1 : 0
      result.mines += cell.data.mined ? 1 : 0
    })

    return result
  }

  public tick() {}
}
