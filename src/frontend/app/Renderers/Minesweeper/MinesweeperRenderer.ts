import { AbstractRenderer } from '../../_util/Renderer'
import { MinesweeperGameBoard } from './MinesweeperGameBoard'

export class MinesweeperRenderer extends AbstractRenderer {
  public static NAME = 'minesweeper'

  disableContextMenu = true

  public board: MinesweeperGameBoard

  public setup() {
    super.setup()
    const { p5 } = this

    this.board = new MinesweeperGameBoard()
    this.board.changeSizeToFit({
      x: p5.width,
      y: p5.height,
    })
    this.board.init()
    this.board.randomizeCells()
  }

  public draw() {
    const { p5 } = this

    p5.background(51)

    this.board.draw(p5)
  }

  public mousePressed() {
    let position = { x: this.p5.mouseX, y: this.p5.mouseY }
    if (this.p5.mouseButton === 'right') {
      this.board.flagCell(position)
    } else {
      this.board.openCell(position)
    }
  }
}
