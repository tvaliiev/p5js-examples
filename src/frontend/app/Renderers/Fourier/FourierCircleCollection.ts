import P5 from 'p5'
import { FourierCircle } from './FourierCircle'
import { Point } from '../../_util/Point'
import { range } from '../../_util/util'

export class FourierCircleCollection {
  private lastPoint: Point | null

  constructor(
    private p5: P5,
    public items: FourierCircle[] = [],
  ) {}

  public getLastPoint(): Point | null {
    return this.lastPoint
  }

  public getLastCircle(): FourierCircle | null {
    return this.items[this.items.length - 1] || null
  }

  public getFirstCircle(): FourierCircle | null {
    return this.items[0] || null
  }

  public draw(time: number): FourierCircleCollection {
    this.items.forEach(fourierCircle => {
      fourierCircle.setTime(time).draw()

      const currentPoint = fourierCircle.getNextPoint()

      this.lastPoint = this.lastPoint
        ? this.lastPoint.addPoint(currentPoint)
        : currentPoint
    })

    return this
  }

  public static create(p5: P5, count: number): FourierCircleCollection {
    const items = []

    range(count).forEach(i => items.push(new FourierCircle(p5, i)))

    return new FourierCircleCollection(p5, items)
  }
}
