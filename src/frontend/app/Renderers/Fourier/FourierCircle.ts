import P5 from 'p5'
import { Point } from '../../_util/Point'

export class FourierCircle {
  private x: number = 0
  private y: number = 0
  private readonly w: number
  private readonly appender: number
  private nextPoint: Point

  constructor(
    private readonly p5: P5,
    private readonly nth: number = 0,
    private size: number = 40,
    private time: number = 0,
  ) {
    this.appender = this.calculateAppender()
    this.w = this.calculateWidth()
    this.setTime(time)
  }

  public getCoordinates(): Point {
    return new Point(this.x, this.y)
  }

  public setTime(time: number): FourierCircle {
    this.time = time
    this.nextPoint = this.calculateNextPoint()

    return this
  }

  public draw(): FourierCircle {
    this.p5
      .stroke(255, 100)
      .noFill()
      .ellipse(this.x, this.y, this.w * 2)

    this.p5.fill(255).stroke(255)

    this.p5.ellipse(this.x, this.y, 2)

    const point = this.getNextPoint()

    this.p5.line(this.x, this.y, point.x, point.y).translate(point.x, point.y)

    return this
  }

  public calculateNextPoint(): Point {
    const x = this.w * this.p5.cos(this.appender * this.time)
    const y = this.w * this.p5.sin(this.appender * this.time)

    return new Point(x, y)
  }

  public getNextPoint(): Point {
    return this.nextPoint
  }

  private calculateWidth(): number {
    return this.size * (4 / (this.appender * this.p5.PI))
  }

  private calculateAppender(): number {
    return this.nth * 2 + 1
  }
}
