import P5 from 'p5'
import { FixedSizeArray } from '../../_util/FixedSizeArray'
import { Point } from '../../_util/Point'

export class FourierGraph {
  private points: FixedSizeArray<Point>

  constructor(
    private p5: P5,
    private start: Point,
    private readonly graphSize = 600,
  ) {
    this.points = new FixedSizeArray<Point>(graphSize)
  }

  public getStartPoint(): Point {
    return this.start
  }

  public addPoint(point: Point): FourierGraph {
    this.points.push(point)

    return this
  }

  public draw(): FourierGraph {
    this.p5.push()

    this.p5.translate(this.start.x, this.start.y).stroke(255).noFill()

    this.p5.beginShape()
    this.points.getItems().forEach((point, i) => {
      this.p5.vertex(i, point.y)
    })
    this.p5.endShape()

    this.p5.pop()

    return this
  }
}
