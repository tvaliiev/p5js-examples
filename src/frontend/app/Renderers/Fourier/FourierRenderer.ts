import { AbstractRenderer, IDimensions } from '../../_util/Renderer'
import { IRenderer } from '../../_util/Renderer'
import { Point } from '../../_util/Point'
import { FourierPlot } from './FourierPlot'

export class FourierRenderer extends AbstractRenderer implements IRenderer {
  public static NAME = 'fourier'
  private plots: { plot: FourierPlot; size: number }[] = []

  private time: number = 0
  private sizes = [
    // 1, 2, 3, 5, 8, 13,
    // 1, 2, 3,
    5, 8, 13, 21, 34, 55,
    // 13
  ]

  public setup() {
    super.setup()

    this.sizes.forEach((size, i) => {
      this.plots.push({
        plot: new FourierPlot(this.p5, new Point(150, 150 * i + 100)),
        size,
      })
    })
  }

  public draw() {
    this.p5.background(0)

    this.plots.forEach(({ plot, size }) => {
      plot.draw(size, this.time)
    })

    this.time += 0.02
  }

  public getDimensions(): IDimensions {
    return { x: 1200, y: 970 }
  }
}
