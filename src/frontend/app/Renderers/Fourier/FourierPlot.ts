import P5 from 'p5'
import { FourierCircleCollection } from './FourierCircleCollection'
import { Point } from '../../_util/Point'
import { FourierGraph } from './FourierGraph'

export class FourierPlot {
  private circles: FourierCircleCollection
  private readonly graph: FourierGraph

  constructor(
    private readonly p5: P5,
    private readonly start: Point,
  ) {
    this.graph = new FourierGraph(p5, this.start.addPoint(new Point(300, 0)))
  }

  public getGraph(): FourierGraph {
    return this.graph
  }

  public getStartPoint(): Point {
    return this.start
  }

  public draw(circles: number, time: number): void {
    this.drawCircles(circles, time)
    this.drawLineCircleToGraph()
    this.drawCircleCount()
    this.drawGraph()
  }

  private drawCircleCount() {
    const startPoint = this.getGraph().getStartPoint()

    this.p5
      .stroke(255)
      .fill(255)
      .text(this.circles.items.length, startPoint.x - 20, startPoint.y)
  }

  private drawLineCircleToGraph() {
    this.p5.push()

    const point = this.start.addPoint(this.circles.getLastPoint())

    this.p5
      .stroke(255, 50)
      .line(point.x, point.y, this.getGraph().getStartPoint().x, point.y)

    this.p5.pop()
  }

  private drawCircles(circles: number, time: number) {
    this.p5.push()
    this.p5.translate(this.start.x, this.start.y)
    this.circles = FourierCircleCollection.create(this.p5, circles).draw(time)
    this.p5.pop()
  }

  private drawGraph() {
    this.p5.push()
    this.graph.addPoint(this.circles.getLastPoint()).draw()
    this.p5.pop()
  }
}
