import { CellularAutomata } from '../../_util/CellularAutomata/CellularAutomata'
import { CellularAutomataRule } from '../../_util/CellularAutomata/CellularAutomataRule'
import { Grid } from '../../_util/Grid'
import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions } from '../../_util/Renderer'
import { range } from '../../_util/util'

export class CellularAutomataRenderer extends AbstractRenderer {
  private grid: Grid<boolean>
  private ca: CellularAutomata
  private currentRowIndex: number = 0
  private cellWH = 5

  public getDimensions(): IDimensions {
    return {
      x: 1200,
      y: 800,
    }
  }

  public setup() {
    super.setup()

    this.grid = new Grid<boolean>(this.getDimensions(), () => false)
    const number = 167
    this.resetWithNewNumber(number)

    // @ts-ignore
    this.p5.createInput(number.toString(), 'number').changed(e => {
      const num = Number(e.target.value)
      this.p5.background(51)
      this.resetWithNewNumber(num)
    })
  }

  private resetWithNewNumber(number: number) {
    this.ca = new CellularAutomata(
      new CellularAutomataRule(
        CellularAutomataRule.DefinitionFromNumber(number),
      ),
    )
    this.grid.getCellAt(Point.fromArray([this.p5.width / 2, 0]).floor()).data =
      true
    this.currentRowIndex = 0
  }

  public draw() {
    const { p5 } = this

    range(50).forEach(() => {
      if (this.currentRowIndex < p5.height - 1) {
        const currentItems = this.grid
          .getRow(this.currentRowIndex)
          .map(c => c.data)
        const futureGeneration = this.ca.getNextGeneration(currentItems)
        this.grid.getRow(this.currentRowIndex + 1).forEach(cell => {
          cell.data = futureGeneration[cell.position.x]
        })

        this.currentRowIndex++
      }
    })

    p5.loadPixels()
    this.grid.forEachCell(cell => {
      const index = cell.position.toOneDimensionIndex(p5.width) * 4
      const color = cell.data ? 51 : 255
      p5.pixels[index] = color
      p5.pixels[index + 1] = color
      p5.pixels[index + 2] = color
      p5.pixels[index + 3] = 255
    })
    p5.updatePixels()
  }
}
