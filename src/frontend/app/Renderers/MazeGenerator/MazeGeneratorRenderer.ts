import { Cell } from '../../_util/Cell'
import { Direction, DirectionOpposite } from '../../_util/Direction'
import { Grid } from '../../_util/Grid'
import { IPoint, Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions } from '../../_util/Renderer'

interface CellData {
  walls: { [key in Direction]: boolean }
  visited: boolean
}

export class MazeGeneratorRenderer extends AbstractRenderer {
  public static NAME = 'maze-generator'

  private grid: Grid<CellData>
  private current: Cell<CellData>
  private size: IPoint = {
    x: 40,
    y: 40,
  }
  private stack: Cell<CellData>[] = []

  public getDimensions(): IDimensions {
    return {
      x: 800,
      y: 800,
    }
  }

  public setup() {
    super.setup()

    this.grid = new Grid<CellData>(this.size, () => ({
      walls: {
        [Direction.UP]: true,
        [Direction.DOWN]: true,
        [Direction.LEFT]: true,
        [Direction.RIGHT]: true,
      },
      visited: false,
    }))
    this.current = this.grid.getCellAt({ x: 0, y: 0 })
  }

  public draw() {
    const { p5 } = this

    const dimensions = Point.fromObject(this.getDimensions()).dividePoint(
      this.size,
    )

    p5.background(51)

    this.current.data.visited = true
    const next = this.getRandomNotVisitedNeighbour(this.current)
    if (next) {
      next.cell.data.visited = true

      this.stack.push(this.current)

      this.current.data.walls[next.direction] = false
      next.cell.data.walls[DirectionOpposite[next.direction]] = false

      this.current = next.cell
    } else if (this.stack.length) {
      this.current = this.stack.pop()
    }

    this.grid.forEachCell(cell => {
      p5.stroke(255).noFill()
      const position = cell.position.multiplyPoint(dimensions)

      if (cell.data.walls[Direction.UP]) {
        p5.line(position.x, position.y, position.x + dimensions.x, position.y)
      }
      if (cell.data.walls[Direction.DOWN]) {
        p5.line(
          position.x,
          position.y + dimensions.y,
          position.x + dimensions.x,
          position.y + dimensions.y,
        )
      }
      if (cell.data.walls[Direction.LEFT]) {
        p5.line(position.x, position.y, position.x, position.y + dimensions.y)
      }
      if (cell.data.walls[Direction.RIGHT]) {
        p5.line(
          position.x + dimensions.x,
          position.y,
          position.x + dimensions.x,
          position.y + dimensions.y,
        )
      }

      if (cell === this.current) {
        p5.noStroke()
          .fill(0, 0, 255, 100)
          .rect(position.x, position.y, dimensions.x, dimensions.y)
      } else if (cell.data.visited) {
        p5.fill(255, 0, 255, 100)
          .noStroke()
          .rect(position.x, position.y, dimensions.x, dimensions.y)
      }
    })
  }

  private getRandomNotVisitedNeighbour(
    cell: Cell<CellData>,
  ): { cell: Cell<CellData>; direction: Direction } | undefined {
    const { p5 } = this
    const neighbours = this.grid
      .getCellsAtDirections(cell.position)
      .filter(neighbour => !neighbour.cell.data.visited)

    if (neighbours.length === 0) {
      return undefined
    }

    return neighbours[p5.floor(p5.random(0, neighbours.length))]
  }
}
