import * as tf from '@tensorflow/tfjs'
import { range } from '../../../_util/util'
import { AbstractRenderer, IDimensions } from '../../../_util/Renderer'

export class TensorFlowXORRenderer extends AbstractRenderer {
  private model: tf.Sequential
  private inputs: tf.Tensor2D
  private outputs: tf.Tensor2D
  isTraining: boolean = false

  public getDimensions(): IDimensions {
    return { x: 800, y: 800 }
  }

  public async setup() {
    super.setup()
    this.inputs = tf.tensor2d([
      [0, 0],
      [0, 1],
      [1, 0],
      [1, 1],
    ])
    this.outputs = tf.tensor2d([[0], [1], [1], [0]])

    this.model = tf.sequential({
      layers: [
        tf.layers.dense({ units: 2, activation: 'sigmoid', inputShape: [2] }),
        tf.layers.dense({ units: 1, activation: 'sigmoid' }),
      ],
    })

    this.model.compile({
      loss: tf.losses.meanSquaredError,
      optimizer: tf.train.rmsprop(0.15),
    })

    setTimeout(() => this.train(), 1)
  }

  train() {
    this.trainModel().then(loss => {
      console.log('loss = ', loss)
      if (loss > 0.002) {
        setTimeout(() => this.train(), 1)
      }
    })
  }

  trainModel() {
    return this.model
      .fit(this.inputs, this.outputs, {
        shuffle: true,
        epochs: 8,
      })
      .then(({ history }) => history.loss[history.loss.length - 1])
  }

  public draw() {
    const { p5 } = this
    p5.background(0).noStroke().textSize(9)

    const resolution = 40
    const size = this.getSize().divide(resolution, resolution)
    const inputs = []
    range(size.y).forEach(y => {
      range(size.x).forEach(x => {
        inputs.push([y / size.y, x / size.x])
      })
    })
    // @ts-ignore
    const results = this.model.predict(tf.tensor2d(inputs)).dataSync()

    range(size.y).forEach(y => {
      range(size.x).forEach(x => {
        const index = y * size.y + x
        const gray = 255 * results[index]
        p5.fill(gray)
          .rect(x * resolution, y * resolution, resolution, resolution)
          .fill(255 - gray)
          .text(
            p5.nf(results[index], 1, 3),
            x * resolution,
            y * resolution + 10,
          )
      })
    })
  }
}
