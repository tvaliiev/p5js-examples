import { AbstractRenderer } from '../../../_util/Renderer'
import * as tf from '@tensorflow/tfjs'

export class TensorflowBasicRenderer extends AbstractRenderer {
  public async setup() {
    super.setup()

    const { p5 } = this

    p5.noCanvas()

    const model = tf.sequential({
      layers: [
        tf.layers.dense({
          units: 4,
          activation: 'sigmoid',
          inputShape: [2],
        }),
        tf.layers.dense({ units: 1, activation: 'sigmoid' }),
      ],
    })

    model.compile({
      optimizer: tf.train.sgd(0.5),
      loss: tf.losses.meanSquaredError,
    })

    const inputs = tf.tensor2d([
      [0, 0],
      [0.5, 0.5],
      [1, 1],
    ])

    const outputs = tf.tensor2d([[1], [0.5], [0]])
    for (let i = 0; i < 400; i++) {
      const history = await model.fit(inputs, outputs, {
        shuffle: true,
        epochs: 10,
      })
      console.log(history.history.loss[0], i)
      if (history.history.loss[0] < 0.0001) {
        break
      }
    }

    model
      .predict(
        tf.tensor2d([
          [1, 1],
          [0.7, 0.7],
          [0.5, 0.5],
          [0.3, 0.3],
          [0.1, 0.1],
          [0, 0],
        ]),
      )
      // @ts-ignore
      .print()
  }

  public draw() {}
}
