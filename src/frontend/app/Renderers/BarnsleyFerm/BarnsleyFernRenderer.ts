import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions, IRenderer } from '../../_util/Renderer'
import { range } from '../../_util/util'

type StepPoint = {
  a: number
  b: number
  c: number
  d: number
  e: number
  f: number

  chance: number
}
type BarnsleyFernConfig = {
  startX: number
  stopX: number
  startY: number
  stopY: number
  stepPoints: StepPoint[]
}

export class BarnsleyFernRenderer
  extends AbstractRenderer
  implements IRenderer
{
  public static NAME = 'barnsley-fern'

  private config: BarnsleyFernConfig = {
    startX: -2.182,
    stopX: 2.6558,
    startY: 0,
    stopY: 9.9983,
    stepPoints: [
      { a: 0, b: 0, c: 0, d: 0.16, e: 0, f: 0, chance: 0.01 },
      { a: 0.85, b: 0.04, c: -0.04, d: 0.85, e: 0, f: 1.6, chance: 0.85 },
      { a: 0.2, b: -0.26, c: 0.23, d: 0.22, e: 0, f: 1.6, chance: 0.07 },
      { a: -0.15, b: 0.28, c: 0.26, d: 0.24, e: 0, f: 0.44, chance: 0.07 },
    ],
  }

  private point: Point = new Point(0, 0)

  public getDimensions(): IDimensions {
    return { x: 600, y: 600 }
  }

  public setup() {
    super.setup()
    this.p5
      .background(0)
      .stroke(255)
      .strokeWeight(1)
      .colorMode(this.p5.HSB, 255, 255, 255)
  }

  public draw() {
    range(200).forEach(() => {
      this.drawPoint()
      this.nextPoint()
    })
  }

  private drawPoint(): void {
    const p5 = this.p5

    p5.stroke(
      p5.map(this.point.y, this.config.startY, this.config.stopY, p5.height, 0),
      255,
      200,
    )

    let px = p5.map(
      this.point.x,
      this.config.startX,
      this.config.stopX,
      0,
      p5.width,
    )
    let py = p5.map(
      this.point.y,
      this.config.startY,
      this.config.stopY,
      p5.height,
      0,
    )

    p5.point(px, py)
  }

  private nextPoint(): void {
    const stepPoint = this.getStepPoint()

    this.point = new Point(
      stepPoint.a * this.point.x + stepPoint.b * this.point.y + stepPoint.e,
      stepPoint.c * this.point.x + stepPoint.d * this.point.y + stepPoint.f,
    )
  }

  private getStepPoint() {
    const random = this.p5.random(1)
    let chance = 0

    for (let i = 0; i < this.config.stepPoints.length; i++) {
      const stepPoint: StepPoint = this.config.stepPoints[i]
      chance += stepPoint.chance
      if (random <= chance) {
        return stepPoint
      }
    }

    throw new Error(`No step points chosen`)
  }
}
