import { AbstractRenderer, IRenderer } from '../../_util/Renderer'
import { randomBetween, range } from '../../_util/util'

export class WaterRippleRenderer extends AbstractRenderer implements IRenderer {
  public static NAME = 'water-ripple'
  private current: number[][]
  private previous: number[][]
  private damping = 0.99
  private cols = 500
  private rows = 500

  public setup() {
    const plotSize = this.getPlotSize()
    this.p5.createCanvas(plotSize.x, plotSize.y)
    this.current = range(this.rows).map(() => {
      return range(this.cols).map(() => 0)
    })
    this.previous = range(this.rows).map(() => {
      return range(this.cols).map(() => 0)
    })
    this.p5.mouseDragged = this.mouseDragged.bind(this)
  }

  private mouseDragged() {
    if (
      typeof this.previous[this.p5.mouseX] === 'undefined' ||
      typeof this.previous[this.p5.mouseX][this.p5.mouseY] === 'undefined'
    ) {
      return
    }

    this.previous[this.p5.mouseX][this.p5.mouseY] = 3000
  }

  public draw() {
    this.p5.background(0)

    this.renderWater()
    this.simulateWaterDrop()
    this.swapBuffers()
  }

  private simulateWaterDrop() {
    if (Math.random() > 0.03) {
      return
    }

    this.previous[randomBetween(0, this.cols)][randomBetween(0, this.rows)] =
      10000
  }

  private swapBuffers() {
    const temp = this.current
    this.current = this.previous
    this.previous = temp
  }

  private renderWater() {
    this.p5.loadPixels()
    for (let i = 1; i < this.cols - 1; i++) {
      for (let j = 1; j < this.rows - 1; j++) {
        this.current[i][j] =
          (this.previous[i - 1][j] +
            this.previous[i + 1][j] +
            this.previous[i][j - 1] +
            this.previous[i][j + 1]) /
            2 -
          this.current[i][j]

        this.current[i][j] *= this.damping

        let index = i + j * this.cols
        index *= 4
        // @ts-ignore
        this.p5.pixels[index] = this.current[i][j]
        this.p5.pixels[index + 1] = this.current[i][j]
        this.p5.pixels[index + 2] = this.current[i][j]
      }
    }
    this.p5.updatePixels()
  }

  public getPlotSize(): { x: number; y: number } {
    return { x: this.cols, y: this.rows }
  }
}
