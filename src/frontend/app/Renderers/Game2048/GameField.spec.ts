import { Color } from '../../_util/Color'
import { Direction } from '../../_util/Direction'
import { CustomException } from '../../_util/Exceptions/CustomException'
import { Point } from '../../_util/Point'
import { range } from '../../_util/util'
import { GameField } from './GameField'

function getNewField(initial: number | number[][] = 4): GameField {
  const field = new GameField(new Point(0, 0), {
    height: Array.isArray(initial) ? initial.length : initial,
    width: Array.isArray(initial) ? initial[0].length : initial,
    square: {
      textColor: new Color(0, 0, 0),
      background: new Color(1, 1, 1),
      spacing: 20,
      size: 30,
    },
    background: new Color(2, 2, 2),
  })

  if (initial && Array.isArray(initial)) {
    populateFields(field, initial)
  }

  return field
}

function populateFields(field: GameField, initial) {
  initial.forEach((row, y) => {
    row.forEach((_, x) => {
      field.changeGameSquareValue(new Point(x, y), initial[y][x])
    })
  })
}

describe('GameField.shift', () => {
  it.each([
    [
      Direction.UP,
      {
        initial: [
          [1, 2, 3, 4, 10],
          [1, 0, 3, 4, 5],
          [0, 2, 3, 0, 0],
          [0, 0, 3, 4, 0],
          [0, 0, 3, 4, 5],
        ],
        expected: [
          [2, 4, 12, 16, 20],
          [0, 0, 3, 0, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
        ],
      },
    ],
    [
      Direction.UP,
      {
        initial: [
          [0, 0, 0, 0, 0],
          [0, 0, 3, 4, 0],
          [1, 2, 3, 3, 0],
          [0, 2, 3, 4, 4],
          [1, 0, 3, 4, 8],
        ],
        expected: [
          [2, 4, 12, 4, 4],
          [0, 0, 0, 3, 8],
          [0, 0, 0, 8, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
        ],
      },
    ],
    [
      Direction.DOWN,
      {
        initial: [
          [1, 2, 3, 4, 10],
          [1, 0, 3, 4, 5],
          [0, 2, 3, 0, 0],
          [0, 0, 3, 4, 0],
          [0, 0, 3, 4, 5],
        ],
        expected: [
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 3, 0, 0],
          [2, 4, 12, 16, 20],
        ],
      },
    ],
    [
      Direction.DOWN,
      {
        initial: [
          [0, 0, 0, 0, 0],
          [0, 0, 3, 4, 0],
          [1, 2, 3, 3, 4],
          [0, 2, 3, 4, 8],
          [1, 0, 3, 4, 4],
        ],
        expected: [
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 4, 4],
          [0, 0, 0, 3, 8],
          [2, 4, 12, 8, 4],
        ],
      },
    ],
    [
      Direction.LEFT,
      {
        initial: [
          [1, 1, 0, 0, 0],
          [2, 0, 2, 0, 0],
          [3, 3, 3, 3, 3],
          [4, 4, 0, 4, 4],
          [5, 10, 0, 5, 5],
        ],
        expected: [
          [2, 0, 0, 0, 0],
          [4, 0, 0, 0, 0],
          [12, 3, 0, 0, 0],
          [16, 0, 0, 0, 0],
          [5, 20, 0, 0, 0],
        ],
      },
    ],
    [
      Direction.LEFT,
      {
        initial: [
          [0, 0, 1, 0, 1],
          [0, 0, 2, 2, 0],
          [0, 3, 3, 3, 3],
          [0, 4, 3, 4, 4],
          [0, 0, 4, 8, 4],
        ],
        expected: [
          [2, 0, 0, 0, 0],
          [4, 0, 0, 0, 0],
          [12, 0, 0, 0, 0],
          [4, 3, 8, 0, 0],
          [4, 8, 4, 0, 0],
        ],
      },
    ],
    [
      Direction.RIGHT,
      {
        initial: [
          [0, 0, 1, 0, 1],
          [0, 0, 2, 2, 0],
          [0, 3, 3, 3, 3],
          [0, 4, 3, 4, 4],
          [0, 0, 4, 8, 4],
        ],
        expected: [
          [0, 0, 0, 0, 2],
          [0, 0, 0, 0, 4],
          [0, 0, 0, 0, 12],
          [0, 0, 4, 3, 8],
          [0, 0, 4, 8, 4],
        ],
      },
    ],
    [
      Direction.RIGHT,
      {
        initial: [
          [0, 0, 1, 0, 1],
          [0, 0, 2, 2, 0],
          [0, 3, 3, 3, 3],
          [0, 4, 3, 4, 4],
          [0, 0, 4, 8, 4],
        ],
        expected: [
          [0, 0, 0, 0, 2],
          [0, 0, 0, 0, 4],
          [0, 0, 0, 0, 12],
          [0, 0, 4, 3, 8],
          [0, 0, 4, 8, 4],
        ],
      },
    ],
    [
      Direction.RIGHT,
      {
        initial: [
          [0, 2, 0, 1, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
        ],
        expected: [
          [0, 0, 0, 2, 1],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0],
        ],
      },
    ],
  ])(
    'Should squash properly in %s direction',
    (shift, { initial, expected }) => {
      const field = getNewField(initial)

      expect(field.getValues()).toEqual(initial)

      field.shift(shift)

      expect(field.getValues()).toEqual(expected)
    },
  )
})

describe('GameField.getValues&&changeGameSquareValue', () => {
  it('Should output correct field state in two dim. arrays', () => {
    const field = getNewField()

    field.changeGameSquareValue(new Point(0, 0), 1)
    field.changeGameSquareValue(new Point(0, 1), 2)
    field.changeGameSquareValue(new Point(0, 2), 3)
    field.changeGameSquareValue(new Point(2, 1), 2)
    field.changeGameSquareValue(new Point(2, 2), 3)
    field.changeGameSquareValue(new Point(2, 3), 4)

    expect(field.getValues()).toEqual([
      [1, 0, 0, 0],
      [2, 0, 2, 0],
      [3, 0, 3, 0],
      [0, 0, 4, 0],
    ])
  })
})

describe('GameField.randomSquare', () => {
  it.each([
    [
      {
        initial: [
          [0, 0, 0, 0],
          [1, 0, 0, 0],
          [0, 0, 0, 0],
          [0, 0, 0, 0],
        ],
        expectedPosition: [{ pos: new Point(0, 1), value: 1 }],
      },
    ],
    [
      {
        initial: [
          [0, 0, 0, 0],
          [1, 0, 0, 3],
          [0, 0, 2, 0],
          [0, 0, 0, 0],
        ],
        expectedPosition: [
          { pos: new Point(0, 1), value: 1 },
          { pos: new Point(2, 2), value: 2 },
          { pos: new Point(3, 1), value: 3 },
        ],
      },
    ],
  ])('Should return random square', ({ initial, expectedPosition }) => {
    const field = getNewField(initial)

    range(50).forEach(_ => {
      const { square, position } = field.randomSquare(s => !s.isEmpty())

      expect(square).not.toBeNull()
      expect(position).not.toBeNull()

      expect(expectedPosition).toContainEqual({
        pos: position,
        value: square.value,
      })
    })
  })

  it('Should return null when no square found', () => {
    const initial = [
      [0, 0, 0, 0],
      [1, 0, 0, 3],
      [0, 0, 2, 0],
      [0, 0, 0, 0],
    ]

    const field = getNewField(initial)

    const { square, position } = field.randomSquare(s => s.value === 5)

    expect(square).toBeNull()
    expect(position).toBeNull()
  })
})

describe('GameField.populateSquare', () => {
  it('Should detect that game is over', () => {
    const field = getNewField([
      [1, 2, 3, 4],
      [1, 1, 1, 3],
      [1, 1, 2, 1],
      [1, 1, 1, 1],
    ])

    field.populateSquare()

    expect(field.gameIsOver).toBeTruthy()
  })

  it('Should populate square', () => {
    const field = getNewField([
      [0, 0],
      [0, 0],
    ])

    field.populateSquare()
    field.populateSquare()

    expect(field.gameIsOver).toBeFalsy()

    const sum = field
      .getValues()
      .flat(1)
      .reduce((acc, el) => acc + el, 0)

    expect([2, 4, 6, 8]).toContain(sum)
  })
})

describe('GameField.initializeFirstPieces', () => {
  it('Should initialize first pieces', () => {
    const field = getNewField([
      [0, 0],
      [0, 0],
    ])

    field.initializeFirstPieces()

    expect(field.gameIsOver).toBeFalsy()

    const sum = field
      .getValues()
      .flat(1)
      .reduce((acc, el) => acc + el, 0)
    expect([2, 4, 6, 8]).toContain(sum)
  })
})

describe('GameField.forEachSquare', () => {
  it('Should break when callback returns false', () => {
    const field = getNewField([
      [0, 0],
      [0, 0],
    ])

    expect(field.forEachSquare(() => false)).toEqual(false)
  })

  it('Should rethrow exception when something goes wrong', () => {
    const field = getNewField([
      [0, 0],
      [0, 0],
    ])

    expect(() =>
      field.forEachSquare(() => {
        throw new CustomException()
      }),
    ).toThrow(CustomException)
  })
})
