import P5 from 'p5'
import { Boundary } from '../../_util/Boundary'
import { Color } from '../../_util/Color'
import { Direction, DirectionMoveOpposite } from '../../_util/Direction'
import {
  BreakException,
  ignoreBreakException,
} from '../../_util/Exceptions/BreakException'
import { IPoint, Point } from '../../_util/Point'
import { randomBetween, range } from '../../_util/util'
import { GameSquare, IGameSquareConfig } from './GameSquare'

export interface IGameFieldConfig {
  width: number
  height: number
  background: Color

  square: IGameSquareConfig
}

export class GameField {
  private squares: { [y: number]: { [x: number]: GameSquare } } = []
  public gameIsOver: boolean = false
  private score: number = 0

  constructor(
    private position: Point,
    private gameGridConfig: IGameFieldConfig,
  ) {
    this.initializeGameGrid()
  }

  public initializeFirstPieces() {
    range(this.gameGridConfig.width).forEach(() => {
      this.populateSquare()
    })
  }

  public populateSquare() {
    const { square } = this.randomSquare(square => square.isEmpty())

    if (!square) {
      this.gameOver()
      return
    }

    /* istanbul ignore next */
    square.value = Math.random() < 0.4 ? 4 : 2
  }

  private gameOver() {
    this.gameIsOver = true
  }

  /* istanbul ignore next */
  public draw(p5: P5) {
    const background = this.gameGridConfig.background

    p5.fill(background.r, background.g, background.b).stroke(
      background.r,
      background.g,
      background.b,
    )

    const squareConfig = this.gameGridConfig.square
    const width =
      this.gameGridConfig.width * squareConfig.size + squareConfig.spacing
    const height =
      this.gameGridConfig.height * squareConfig.size + squareConfig.spacing
    p5.rect(this.position.x, this.position.y, width, height, 3)

    if (this.gameIsOver) {
      p5.textSize(20)
        // @ts-ignore
        .fill(...squareConfig.textColor.toColorArgs())
        .textAlign(p5.CENTER, p5.CENTER)
        .text(
          `Game over!\nYour score: ${this.score}`,
          this.position.x + width / 2,
          this.position.y + height / 2,
        )
      return
    }

    this.score = 0
    this.forEachSquare((square, y, x) => {
      square.draw(p5)
      // square.drawDebugData(p5, new Point(x, y))

      this.score += square.value
    })
  }

  public shift(direction: Direction) {
    const configs = {
      [Direction.UP]: {
        max: this.gameGridConfig.width,
      },
      [Direction.DOWN]: {
        max: this.gameGridConfig.width,
      },
      [Direction.LEFT]: {
        max: this.gameGridConfig.height,
      },
      [Direction.RIGHT]: {
        max: this.gameGridConfig.height,
      },
    }

    const config = configs[direction]

    for (let startCoord = 0; startCoord < config.max; startCoord++) {
      this.squashColumnOrRow(direction, startCoord)
    }
  }

  public squashColumnOrRow(direction: Direction, columnOrRowId: number): void {
    const configs = {
      [Direction.UP]: {
        start: new Point(columnOrRowId, 0),
        adder: DirectionMoveOpposite[Direction.UP],
        condition: i => i.y < this.gameGridConfig.height - 1,
      },
      [Direction.DOWN]: {
        start: new Point(columnOrRowId, this.gameGridConfig.height - 1),
        adder: DirectionMoveOpposite[Direction.DOWN],
        condition: i => i.y >= 0,
      },
      [Direction.RIGHT]: {
        start: new Point(this.gameGridConfig.width - 1, columnOrRowId),
        adder: DirectionMoveOpposite[Direction.RIGHT],
        condition: i => i.x >= 0,
      },
      [Direction.LEFT]: {
        start: new Point(0, columnOrRowId),
        adder: DirectionMoveOpposite[Direction.LEFT],
        condition: i => i.x < this.gameGridConfig.width - 1,
      },
    }
    const config = configs[direction]

    for (
      let i = config.start;
      config.condition(i);
      i = i.addPoint(config.adder)
    ) {
      const currentSquare = this.squares[i.y][i.x]
      const { nextSquare, position } = this.getNextSquare(
        i.addPoint(config.adder),
        direction,
        square => square && !square.isEmpty(),
      )

      if (currentSquare.isEmpty()) {
        if (nextSquare) {
          this.swapSquares(position, i)
          i = i.addPoint(config.adder.multiply(-1, -1))
        }

        continue
      }

      if (!nextSquare) {
        return
      }

      if (nextSquare.shouldSquashInto(currentSquare)) {
        nextSquare.squashInto(currentSquare)
        i = config.start.subtractPoint(config.adder)
      }
    }
  }

  public getNextSquare(
    searchFromCoord: Point,
    direction: Direction,
    filter: (square: GameSquare) => boolean,
  ): { nextSquare: GameSquare | null; position: Point | null } {
    const configs = {
      [Direction.UP]: {
        condition: i => i.y < this.gameGridConfig.height,
        adder: DirectionMoveOpposite[Direction.UP],
      },
      [Direction.DOWN]: {
        condition: i => i.y >= 0,
        adder: DirectionMoveOpposite[Direction.DOWN],
      },
      [Direction.RIGHT]: {
        adder: DirectionMoveOpposite[Direction.RIGHT],
        condition: i => i.x >= 0,
      },
      [Direction.LEFT]: {
        adder: DirectionMoveOpposite[Direction.LEFT],
        condition: i => i.x < this.gameGridConfig.width,
      },
    }
    const config = configs[direction]

    for (
      let i = searchFromCoord.clone();
      config.condition(i);
      i = i.addPoint(config.adder)
    ) {
      const currentSquare = this.squares[i.y][i.x]

      const filterResult = filter(currentSquare)
      if (filterResult) {
        return {
          nextSquare: currentSquare,
          position: i,
        }
      }
    }

    return {
      nextSquare: null,
      position: null,
    }
  }

  public swapSquares(a: Point, b: Point) {
    const tmp = this.squares[a.y][a.x].value
    this.squares[a.y][a.x].value = this.squares[b.y][b.x].value
    this.squares[b.y][b.x].value = tmp
  }

  public randomSquare(
    filter: (square: GameSquare) => boolean,
    failAt: number = this.gameGridConfig.width *
      this.gameGridConfig.height *
      50,
  ): { square: GameSquare | null; position: Point | null } {
    while (failAt) {
      failAt--
      const randomX = randomBetween(0, this.gameGridConfig.width - 1)
      const randomY = randomBetween(0, this.gameGridConfig.height - 1)

      const square = this.squares[randomY][randomX]
      if (filter(square) === true) {
        return { square, position: new Point(randomX, randomY) }
      }
    }

    return { square: null, position: null }
  }

  public forEachSquare(
    cb: (square: GameSquare, y: number, x: number) => boolean | void,
  ): false | void {
    return ignoreBreakException(() => {
      range(this.gameGridConfig.width).forEach(y => {
        range(this.gameGridConfig.height).forEach(x => {
          if (!this.squares[y]) {
            cb(undefined, y, x)
            return
          }

          if (cb(this.squares[y][x], y, x) === false) {
            throw new BreakException(false)
          }
        })
      })
    })
  }

  public changeGameSquareValue(coord: Point, value: number) {
    this.squares[coord.y][coord.x].value = value
  }

  public initializeGameGrid() {
    this.forEachSquare((_, y, x) => {
      if (!this.squares[y]) {
        this.squares[y] = {}
      }

      this.squares[y][x] = this.generateGameSquare(new Point(x, y))
    })
  }

  private generateGameSquare(position: IPoint, value = 0) {
    const squareConfig = this.gameGridConfig.square

    return new GameSquare(
      new Boundary(
        new Point(
          this.position.x +
            position.x * squareConfig.size +
            squareConfig.spacing,
          this.position.y +
            position.y * squareConfig.size +
            squareConfig.spacing,
        ),
        new Point(
          this.position.x + (position.x + 1) * squareConfig.size,
          this.position.y + (position.y + 1) * squareConfig.size,
        ),
      ),
      squareConfig,
      value,
    )
  }

  public getValues(): number[][] {
    const result = []

    this.forEachSquare((square, y, x) => {
      if (typeof result[y] === 'undefined') {
        result[y] = []
      }
      result[y][x] = square.value
    })

    return result
  }
}
