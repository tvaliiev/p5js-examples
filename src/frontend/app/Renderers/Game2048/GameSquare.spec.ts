import { Boundary } from '../../_util/Boundary'
import { Color } from '../../_util/Color'
import { Point } from '../../_util/Point'
import { GameSquare } from './GameSquare'

function squareFactory(value = 0): GameSquare {
  return new GameSquare(
    new Boundary(new Point(0, 0), new Point(1, 1)),
    {
      textColor: new Color(0, 0, 0),
      background: new Color(1, 1, 1),
      spacing: 20,
      size: 20,
    },
    value,
  )
}

describe('GameSquare.constructor', () => {
  it('Should create square without value', () => {
    const square = new GameSquare(
      new Boundary(new Point(0, 0), new Point(1, 1)),
      {
        textColor: new Color(0, 0, 0),
        background: new Color(1, 1, 1),
        spacing: 20,
        size: 20,
      },
    )

    expect(square.value).toEqual(0)
  })
})

describe('GameSquare.squashInto', () => {
  it('Should not squash into other when values are not equal', () => {
    const square1 = squareFactory()
    const square2 = squareFactory(1)

    expect(square1.squashInto(square2)).toBeFalsy()
  })

  it('Should squash into other when values are equal', () => {
    const square1 = squareFactory(2)
    const square2 = squareFactory(2)

    expect(square1.squashInto(square2)).toBeTruthy()
    expect(square1.value).toEqual(0)
    expect(square2.value).toEqual(4)
  })
})
