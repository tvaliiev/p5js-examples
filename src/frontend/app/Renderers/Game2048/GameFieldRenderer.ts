import P5 from 'p5'
import { Point } from '../../_util/Point'
import { GameField, IGameFieldConfig, Direction } from './GameField'

export class GameFieldRenderer {
  private gameField: GameField

  constructor(
    private position: Point = new Point(0, 0),
    private gameFieldConfig: IGameFieldConfig,
  ) {
    this.gameField = new GameField(position, gameFieldConfig)
    this.gameField.initializeFirstPieces()
  }

  draw(p5: P5) {
    this.gameField.draw(p5)
  }

  shift(direction: Direction) {
    this.gameField.shift(direction)
    this.gameField.populateSquare()
  }
}
