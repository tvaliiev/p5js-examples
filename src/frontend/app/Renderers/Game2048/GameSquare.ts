import P5 from 'p5'
import { Boundary } from '../../_util/Boundary'
import { Color } from '../../_util/Color'
import { Point } from '../../_util/Point'

export interface IGameSquareConfig {
  size: number
  background: Color
  textColor: Color
  spacing: number
}

export class GameSquare {
  constructor(
    public readonly boundary: Boundary,
    private config: IGameSquareConfig,
    public value: number = 0,
  ) {}

  /* istanbul ignore next */
  public draw(p5: P5) {
    p5.fill(
      this.config.background.r,
      this.config.background.g,
      this.config.background.b,
    ).stroke(
      this.config.background.r,
      this.config.background.g,
      this.config.background.b,
    )
    this.boundary.draw(p5, 3)

    if (this.value !== 0) {
      p5.fill(
        this.config.textColor.r,
        this.config.textColor.g,
        this.config.textColor.b,
      )
        .stroke(
          this.config.textColor.r,
          this.config.textColor.g,
          this.config.textColor.b,
        )
        .textSize(17)
        .textAlign(p5.CENTER)
        .text(this.value, this.boundary.center.x, this.boundary.center.y + 5)
    }
  }

  /* istanbul ignore next */
  public drawDebugData(p5: P5, squareCoordinate: Point): void {
    this.drawSquareCoordinate(p5, squareCoordinate)
  }

  /* istanbul ignore next */
  private drawSquareCoordinate(p5: P5, squareCoordinate: Point) {
    p5.fill(255)
      .stroke(255)
      .textSize(12)
      .text(
        squareCoordinate.toShortString(),
        this.boundary.top_left.x + 18,
        this.boundary.top_left.y + 10,
      )
  }

  public squashInto(other: GameSquare): boolean {
    if (!this.shouldSquashInto(other)) {
      return false
    }

    other.value += this.value
    this.value = 0
    return true
  }

  public isEmpty(): boolean {
    return this.value === 0
  }

  public shouldSquashInto(other: GameSquare) {
    return !this.isEmpty() && this.value === other.value
  }
}
