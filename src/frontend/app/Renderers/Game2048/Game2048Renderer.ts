import { Color } from '../../_util/Color'
import { Direction } from '../../_util/Direction'
import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions } from '../../_util/Renderer'
import { IGameFieldConfig } from './GameField'
import { GameFieldRenderer } from './GameFieldRenderer'

enum GameState {
  GAME = 'game',
}

interface ICurrentGameState {
  renderer: GameFieldRenderer
}

export class Game2048Renderer extends AbstractRenderer {
  public static NAME = '2048'

  private currentStateName: GameState = GameState.GAME
  private currentState: ICurrentGameState
  private states: { [key in GameState]?: ICurrentGameState }

  public getDimensions(): IDimensions {
    return { x: 800, y: 800 }
  }

  public setup() {
    super.setup()

    const { p5 } = this

    const gameFieldConfig: IGameFieldConfig = {
      width: 4,
      height: 4,
      background: new Color(187, 173, 160),
      square: {
        size: 100,
        spacing: 20,
        background: new Color(205, 193, 180),
        textColor: new Color(119, 110, 101),
      },
    }
    this.states = {
      [GameState.GAME]: {
        renderer: new GameFieldRenderer(
          new Point(
            (p5.width -
              gameFieldConfig.width *
                (gameFieldConfig.square.size +
                  gameFieldConfig.square.spacing)) /
              2,
            (p5.height -
              gameFieldConfig.height *
                (gameFieldConfig.square.size +
                  gameFieldConfig.square.spacing)) /
              2,
          ),
          gameFieldConfig,
        ),
      },
    }

    this.changeStateTo(GameState.GAME)
  }

  private changeStateTo(state: GameState) {
    this.currentStateName = state
    this.currentState = this.states[state]
  }

  public draw() {
    this.p5.background(51)
    this.currentState.renderer.draw(this.p5)
  }

  public keyPressed() {
    if (this.currentStateName !== GameState.GAME) {
      return
    }

    const configMap = {
      75: Direction.UP,
      [this.p5.UP_ARROW]: Direction.UP,
      74: Direction.DOWN,
      [this.p5.DOWN_ARROW]: Direction.DOWN,
      72: Direction.LEFT,
      [this.p5.LEFT_ARROW]: Direction.LEFT,
      76: Direction.RIGHT,
      [this.p5.RIGHT_ARROW]: Direction.RIGHT,
    }

    Object.keys(configMap).forEach(key => {
      if (this.p5.keyCode !== parseInt(key)) {
        return
      }

      this.currentState.renderer.shift(configMap[key])
    })
  }
}
