import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions, IRenderer } from '../../_util/Renderer'
import { range } from '../../_util/util'
import P5 from 'p5'

export class ChaosGameRenderer extends AbstractRenderer implements IRenderer {
  public static NAME = 'chaos-game'

  private seedPoints: Point[] = []
  private targetSeedPoints: number
  private tempPoint: Point
  private readonly POINTS_PER_FRAME = 5000
  private points: number
  private percent: number
  private prevSeedPoint: Point
  private targetSeedPointsInput: P5.Element
  private percentInput: P5.Element

  public getDimensions(): IDimensions {
    return { x: 1400, y: 900 }
  }

  public setup() {
    super.setup()

    this.targetSeedPointsInput = this.p5.createSlider(3, 100, 5, 1)
    this.percentInput = this.p5.createSlider(0.1, 0.9, 0.5, 0.01)

    this.p5.translate(this.p5.width / 2, this.p5.height / 2)

    this.reset()
  }

  private reset() {
    const p5 = this.p5

    p5.background(0).stroke(255).fill(255)

    this.targetSeedPoints = parseFloat(
      this.targetSeedPointsInput.value().toString(),
    )
    this.percent = parseFloat(this.percentInput.value().toString())
    this.points = 0

    p5.text(
      `${this.targetSeedPoints} | ${this.percent}`,
      -p5.width / 2 + 40,
      -p5.height / 2 + 40,
    )

    const circleRadius = p5.min(p5.width, p5.height) / 2 - 10
    this.seedPoints = range(this.targetSeedPoints).map(i => {
      const angle = (i * p5.TWO_PI) / this.targetSeedPoints

      return new Point(
        circleRadius * p5.cos(angle),
        circleRadius * p5.sin(angle),
      )
    })

    p5.strokeWeight(8).noFill()

    this.seedPoints.forEach(point => p5.point(point.x, point.y))
    this.tempPoint = new Point(
      p5.random(p5.width) - p5.width,
      p5.random(p5.height) - p5.height,
    )

    p5.strokeWeight(2)
  }

  public draw(): void {
    let p5 = this.p5
    p5.translate(p5.width / 2, p5.height / 2)
    console.log(p5.frameRate(), this.points)

    const percentInputValue = parseFloat(this.percentInput.value().toString())
    const targetSeedPointInputValue = parseFloat(
      this.targetSeedPointsInput.value().toString(),
    )

    if (
      this.percent !== percentInputValue ||
      this.targetSeedPoints !== targetSeedPointInputValue
    ) {
      this.targetSeedPoints = targetSeedPointInputValue
      this.percent = percentInputValue

      this.reset()
    }

    if (this.points > 500_000) {
      return
    }

    // this.p5.background(0)

    p5.strokeWeight(1).stroke(255, 0, 255, 100)
    range(this.POINTS_PER_FRAME).forEach(_ => {
      p5.point(this.tempPoint.x, this.tempPoint.y)

      let targetSeedPoint: Point
      do {
        targetSeedPoint = p5.random(this.seedPoints)
      } while (targetSeedPoint === this.prevSeedPoint)
      this.prevSeedPoint = targetSeedPoint

      const middlePoint = new Point(
        p5.lerp(this.tempPoint.x, targetSeedPoint.x, this.percent),
        p5.lerp(this.tempPoint.y, targetSeedPoint.y, this.percent),
      )

      p5.point(middlePoint.x, middlePoint.y)
      this.tempPoint = middlePoint
    })

    this.points += this.POINTS_PER_FRAME
  }
}
