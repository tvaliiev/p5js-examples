import { AbstractRenderer, IRenderer } from '../../_util/Renderer'

export class EvolutionarySteeringRenderer
  extends AbstractRenderer
  implements IRenderer
{
  public static NAME = 'evolutionary-steering-renderer'

  public draw() {}
}
