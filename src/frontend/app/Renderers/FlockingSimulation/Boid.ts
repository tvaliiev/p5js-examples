import P5 from 'p5'
import { Point } from '../../_util/Point'
import { randomBetween } from '../../_util/util'

export class Boid {
  private maxForce = 0.2
  private maxSpeed = 5

  constructor(
    private position: Point,
    private velocity: Point = Point.random(randomBetween(2, 4)),
    private acceleration = new Point(0, 0),
  ) {}

  edges(p5: P5) {
    if (this.position.x > p5.width) {
      this.position.x = 0
    } else if (this.position.x < 0) {
      this.position.x = p5.width
    }
    if (this.position.y > p5.height) {
      this.position.y = 0
    } else if (this.position.y < 0) {
      this.position.y = p5.height
    }
  }

  align(boids) {
    let perceptionRadius = 25
    let steering = new Point(0, 0)
    let total = 0
    for (let other of boids) {
      let d = this.position.distance(other.position)

      if (other != this && d < perceptionRadius) {
        steering = steering.addPoint(other.velocity)
        total++
      }
    }
    if (total > 0) {
      steering = steering
        .divide(total, total)
        .setMagnitude(this.maxSpeed)
        .subtractPoint(this.velocity)
        .limit(this.maxForce)
    }
    return steering
  }

  separation(boids) {
    let perceptionRadius = 24
    let steering = new Point(0, 0)
    let total = 0
    for (let other of boids) {
      let d = this.position.distance(other.position)
      if (other != this && d < perceptionRadius) {
        let diff = this.position
          .subtractPoint(other.position)
          .divide(d, total * d)
        steering = steering.addPoint(diff)
        total++
      }
    }
    if (total > 0) {
      steering = steering
        .divide(total, total)
        .setMagnitude(this.maxSpeed)
        .subtractPoint(this.velocity)
        .limit(this.maxForce)
    }
    return steering
  }

  cohesion(boids) {
    let perceptionRadius = 50
    let steering = new Point(0, 0)
    let total = 0
    for (let other of boids) {
      let d = this.position.distance(other.position)
      if (other != this && d < perceptionRadius) {
        steering = steering.addPoint(other.position)
        total++
      }
    }
    if (total > 0) {
      steering = steering
        .divide(total, total)
        .subtractPoint(this.position)
        .setMagnitude(this.maxSpeed)
        .subtractPoint(this.velocity)
        .limit(this.maxForce)
    }
    return steering
  }

  flock(
    boids,
    influences: { align: number; cohesion: number; separation: number },
  ) {
    let alignment = this.align(boids).multiply(
      influences.align,
      influences.align,
    )
    let cohesion = this.cohesion(boids).multiply(
      influences.cohesion,
      influences.cohesion,
    )
    let separation = this.separation(boids).multiply(
      influences.separation,
      influences.separation,
    )

    this.acceleration = this.acceleration
      .addPoint(alignment)
      .addPoint(cohesion)
      .addPoint(separation)
  }

  update() {
    this.position = this.position.addPoint(this.velocity)
    this.velocity = this.velocity
      .addPoint(this.acceleration)
      .limit(this.maxSpeed)
    this.acceleration = new Point(0, 0)
  }

  draw(p5: P5) {
    p5.strokeWeight(8).stroke(255).point(this.position.x, this.position.y)
  }
}
