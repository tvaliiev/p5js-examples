import P5 from 'p5'
import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions } from '../../_util/Renderer'
import { range } from '../../_util/util'
import { Boid } from './Boid'

export class FlockingSimulationRenderer extends AbstractRenderer {
  public static NAME = 'flocking-simulation'

  public getDimensions(): IDimensions {
    return { x: 640, y: 360 }
  }

  private flock: Boid[] = []
  private inputs: P5.Element[]

  public setup() {
    super.setup()

    const { p5 } = this
    range(200).forEach(() => {
      this.flock.push(
        new Boid(
          new Point(p5.random(p5.width), p5.random(p5.height)),
          Point.random(p5.random(3, 5)),
        ),
      )
    })

    this.inputs = [
      p5.createSlider(0, 2, 1.5, 0.1),
      p5.createSlider(0, 2, 1, 0.1),
      p5.createSlider(0, 2, 2, 0.1),
    ]
  }

  public draw() {
    const { p5 } = this

    p5.background(51)

    const influences = {
      align: parseFloat(this.inputs[0].value().toString()),
      cohesion: parseFloat(this.inputs[1].value().toString()),
      separation: parseFloat(this.inputs[2].value().toString()),
    }

    this.flock.forEach(b => {
      b.edges(p5)
      b.flock(this.flock, influences)
      b.update()
      b.draw(p5)
    })
  }
}
