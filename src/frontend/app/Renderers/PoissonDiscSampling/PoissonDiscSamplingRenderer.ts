import { DirectionGroups } from '../../_util/Direction'
import {
  BreakException,
  ignoreBreakException,
} from '../../_util/Exceptions/BreakException'
import { Grid } from '../../_util/Grid'
import { IPoint, Point } from '../../_util/Point'
import { AbstractRenderer } from '../../_util/Renderer'
import { randomElement, randomFloat, range } from '../../_util/util'

type CellData = {
  value: Point
}

export class PoissonDiscSamplingRenderer extends AbstractRenderer {
  private activePoints: Point[] = []
  private radius = 20
  private samplesBeforeReject = 30
  private cellSize: IPoint
  private grid: Grid<CellData>

  public setup() {
    super.setup()

    const { p5 } = this

    this.cellSize = {
      x: this.radius / Math.sqrt(2),
      y: this.radius / Math.sqrt(2),
    }

    const dimensions = this.getDimensions()
    this.grid = new Grid<CellData>(
      {
        x: Math.floor(dimensions.x / this.cellSize.x),
        y: Math.floor(dimensions.y / this.cellSize.y),
      },
      () => ({
        value: undefined,
      }),
    )

    const randomPoint = new Point(p5.random(p5.width), p5.random(p5.height))
    const randomPointIndex = randomPoint.dividePoint(this.cellSize).floor()
    this.grid.getCellAt(randomPointIndex).data.value = randomPoint
    this.activePoints.push(randomPoint)
  }

  public draw() {
    const { p5 } = this

    range(3).forEach(() => this.createBunchOfNewPoints())

    p5.stroke(255).strokeWeight(4).background(0)
    this.grid.forEachCell(cell => {
      if (!cell.data.value) {
        return
      }

      p5.point(cell.data.value.x, cell.data.value.y)
    })

    p5.strokeWeight(4).stroke(255, 0, 255)
    this.activePoints.forEach(point => {
      p5.point(point.x, point.y)
    })
  }

  private createBunchOfNewPoints() {
    if (this.activePoints.length <= 0) {
      return
    }

    const { element: randomActivePoint, index: randomActivePointIndex } =
      randomElement(this.activePoints)
    let isFoundOneSample = false
    range(this.samplesBeforeReject).forEach(() => {
      const offset = Point.random(randomFloat(this.radius, this.radius * 2))

      const sample = randomActivePoint.addPoint(offset)

      const sampleCoordinates = sample.dividePoint(this.cellSize).floor()
      if (!this.grid.cellExistsAt(sampleCoordinates)) {
        return
      }
      const sampleCell = this.grid.getCellAt(sampleCoordinates)

      let isOkaySample = true

      ignoreBreakException(() => {
        this.grid
          .getCellsAtDirections(sampleCell.position, DirectionGroups.HVDC)
          .forEach(({ cell }) => {
            if (!cell.data.value) {
              return
            }

            const d = sample.distance(cell.data.value)
            if (d <= this.radius) {
              isOkaySample = false
              throw new BreakException()
            }
          })
      })

      if (isOkaySample) {
        this.activePoints.push(sample)
        sampleCell.data.value = sample
        isFoundOneSample = true
      }
    })
    if (!isFoundOneSample) {
      this.activePoints.splice(randomActivePointIndex, 1)
    }
  }
}
