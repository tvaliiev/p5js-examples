export class NGramGraph {
  private states = {}

  constructor(public readonly order = 3) {}

  addItem(gram: { str: string; nextChar: string }) {
    if (gram.str.length !== this.order) {
      throw new Error('Invalid string length while inserting to graph')
    }

    if (!this.states[gram.str]) {
      this.states[gram.str] = []
    }

    this.states[gram.str].push(gram.nextChar)
  }

  getItem(key: string): string[] {
    return this.states[key]
  }
}
