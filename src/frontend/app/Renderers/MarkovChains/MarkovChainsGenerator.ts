import { randomElement } from '../../_util/util'
import { NGramGraph } from './NGramGraph'

export class MarkovChainGenerator {
  private static *getSubstring(text: string, count: number) {
    for (let i = 0; i < text.length - count; i++) {
      yield { str: text.substring(i, i + count), nextChar: text[i + count] }
    }
  }

  static getGenerator(
    text: string,
    nGramOrder: number = 3,
  ): Generator<{ str: string; nextChar: string }> {
    return MarkovChainGenerator.getSubstring(text, nGramOrder)
  }

  static generateNextCharForText(
    text: string,
    graph: NGramGraph,
  ): string | null {
    const slice = text.slice(-graph.order)
    console.log('slice = ', slice)
    const item = graph.getItem(slice)

    if (!item) {
      return null
    }

    return randomElement(item)?.element
  }

  static generateText(
    graph: NGramGraph,
    start: string,
    targetLength: number,
  ): string {
    let result = start
    console.log('targetLength = ', targetLength)
    targetLength -= graph.order - 1
    console.log('targetLength = ', targetLength)

    for (let i = 0; i < targetLength; i++) {
      const nextChar = MarkovChainGenerator.generateNextCharForText(
        result,
        graph,
      )
      if (!nextChar) {
        break
      }
      result += nextChar
    }

    return result
  }
}
