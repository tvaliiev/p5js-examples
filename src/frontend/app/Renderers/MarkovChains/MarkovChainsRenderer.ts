import { AbstractRenderer } from '../../_util/Renderer'
import { MarkovChainGenerator } from './MarkovChainsGenerator'
import { NGramGraph } from './NGramGraph'

export class MarkovChainsRenderer extends AbstractRenderer {
  private readonly text = `The unicorn is a legendary creature that has been described since antiquity as a beast with a large, pointed, spiraling horn projecting from its forehead. The unicorn was depicted in ancient seals of the Indus Valley Civilization and was mentioned by the ancient Greeks in accounts of natural history by various writers, including Ctesias, Strabo, Pliny the Younger, and Aelian.[1] The Bible also describes an animal, the re'em, which some translations have erroneously rendered with the word unicorn.[1] In European folklore, the unicorn is often depicted as a white horse-like or goat-like animal with a long horn and cloven hooves (sometimes a goat's beard). In the Middle Ages and Renaissance, it was commonly described as an extremely wild woodland creature, a symbol of purity and grace, which could only be captured by a virgin. In the encyclopedias its horn was said to have the power to render poisoned water potable and to heal sickness. In medieval and Renaissance times, the tusk of the narwhal was sometimes sold as unicorn horn.`
  private order = 7

  public setup() {
    super.setup()
    const { p5 } = this
    this.generateText()

    p5.createSlider(1, 10, this.order, 1)
      // @ts-ignore
      .changed(e => {
        this.order = +e.target.value
        this.generateText()
      })

    p5.noLoop()
  }

  private generateText() {
    const { p5 } = this

    const generator = MarkovChainGenerator.getGenerator(this.text, this.order)
    let substr
    const graph = new NGramGraph(this.order)
    while ((substr = generator.next().value)) {
      graph.addItem(substr)
    }

    console.log(this.order)
    const text = MarkovChainGenerator.generateText(
      graph,
      this.text.substring(0, this.order),
      this.text.length,
    )

    p5.background(255)
    p5.text(`Original text: \n\n${this.text}`, 0, 0, p5.width, p5.height / 2)
    p5.text(text, 0, p5.height / 2, p5.width, p5.height)
  }

  public draw() {}
}
