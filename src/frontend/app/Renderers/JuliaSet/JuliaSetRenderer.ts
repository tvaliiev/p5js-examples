import {
  AbstractRenderer,
  IDimensions,
} from '../../_util/Renderer/AbstractRenderer'
import { IRenderer } from '../../_util/Renderer/IRenderer'
import { range } from '../../_util/util'

type Color = { r: number; g: number; b: number }

export class JuliaSetRenderer extends AbstractRenderer implements IRenderer {
  public static NAME = 'julia-set'

  private angle = 0
  private maxIterations = 200
  private colors: Color[] = []

  public getDimensions(): IDimensions {
    return { x: 640, y: 360 }
  }

  public setup() {
    super.setup()

    this.p5.pixelDensity(1).colorMode(this.p5.HSB, 1)

    range(this.maxIterations).forEach(n => {
      const hu = this.p5.sqrt(n / this.maxIterations)
      const color = this.p5.color(hu, 255, 150)
      this.colors.push({
        r: this.p5.red(color),
        g: this.p5.green(color),
        b: this.p5.blue(color),
      })
    })
  }

  public draw(): void {
    let ca = this.p5.map(this.p5.mouseX, 0, this.p5.width, -1, 1) //-0.70176;
    let cb = this.p5.map(this.p5.mouseY, 0, this.p5.height, -1, 1) //-0.3842;

    // let ca = this.p5.cos(this.angle * 3.213) //sin(angle);
    // let cb = this.p5.sin(this.angle)

    // let ca = 0
    // let cb = 0.8

    this.angle += 0.02

    this.p5.background(255)

    // Establish a range of values on the complex plane
    // A different range will allow us to "zoom" in or out on the fractal

    // It all starts with the width, try higher or lower values
    //let w = abs(sin(angle)) * 5;
    let w = 5
    let h = (w * this.p5.height) / this.p5.width

    // Start at negative half the width and height
    let xmin = -w / 2
    let ymin = -h / 2

    // Make sure we can write to the pixels[] array.
    // Only need to do this once since we don't do any other drawing.
    this.p5.loadPixels()

    // x goes from xmin to xmax
    let xmax = xmin + w
    // y goes from ymin to ymax
    let ymax = ymin + h

    // Calculate amount we increment x,y for each pixel
    let dx = (xmax - xmin) / this.p5.width
    let dy = (ymax - ymin) / this.p5.height

    // Start y
    let y = ymin
    for (let j = 0; j < this.p5.height; j++) {
      // Start x
      let x = xmin
      for (let i = 0; i < this.p5.width; i++) {
        // Now we test, as we iterate z = z^2 + cm does z tend towards infinity?
        let a = x
        let b = y
        let n = 0
        while (n < this.maxIterations) {
          let aa = a * a
          let bb = b * b
          // Infinity in our finite world is simple, let's just consider it 16
          if (aa + bb > 4.0) {
            break // Bail
          }
          let twoab = 2.0 * a * b
          a = aa - bb + ca
          b = twoab + cb
          n++
        }

        // We color each pixel based on how long it takes to get to infinity
        // If we never got there, let's pick the color black
        let pix = (i + j * this.p5.width) * 4
        if (n == this.maxIterations) {
          this.p5.pixels[pix] = 0
          this.p5.pixels[pix + 1] = 0
          this.p5.pixels[pix + 2] = 0
        } else {
          // Otherwise, use the colors that we made in setup()
          this.p5.pixels[pix] = this.colors[n].r
          this.p5.pixels[pix + 1] = this.colors[n].g
          this.p5.pixels[pix + 2] = this.colors[n].b
        }
        x += dx
      }
      y += dy
    }
    this.p5.updatePixels()
  }
}
