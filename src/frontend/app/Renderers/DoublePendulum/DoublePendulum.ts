import P5 from 'p5'
import { AbstractRenderer, IDimensions } from '../../_util/Renderer'
import { IRenderer } from '../../_util/Renderer'

type Bar = {
  radius: number
  mass: number
  angle: number
  x: number
  y: number
  velocity: number
  acceleration: number
}

export class DoublePendulum extends AbstractRenderer implements IRenderer {
  public static NAME = 'double-pendulum'

  private bars: Bar[]
  private start = 200
  private trace: P5.Graphics
  private lastEndPosition: { x: number; y: number }
  private g = 1

  public setup() {
    super.setup()

    this.bars = [
      {
        radius: 200,
        mass: 40,
        angle: this.p5.PI / 2,
        x: 0,
        y: 0,
        acceleration: 0,
        velocity: 0.02,
      },
      {
        radius: 200,
        mass: 40,
        angle: this.p5.PI / 2,
        x: 0,
        y: 0,
        acceleration: 0,
        velocity: 0.01,
      },
    ]

    this.trace = this.p5.createGraphics(this.p5.width, this.p5.height)
    this.trace.background(175).translate(this.p5.width / 2, this.start)
  }

  public draw(): void {
    this.p5.image(this.trace, 0, 0)

    this.p5
      .stroke(0)
      .strokeWeight(2)
      .translate(this.p5.width / 2, this.start)

    let num1 =
      -this.g *
      (2 * this.bars[0].mass + this.bars[1].mass) *
      this.p5.sin(this.bars[0].angle)
    let num2 =
      -this.bars[1].mass *
      this.g *
      this.p5.sin(this.bars[0].angle - 2 * this.bars[1].angle)
    let num3 =
      -2 *
      this.p5.sin(this.bars[0].angle - this.bars[1].angle) *
      this.bars[1].mass
    let num4 =
      this.bars[1].velocity * this.bars[1].velocity * this.bars[1].radius +
      this.bars[0].velocity *
        this.bars[0].velocity *
        this.bars[0].radius *
        this.p5.cos(this.bars[0].angle - this.bars[1].angle)
    let den =
      this.bars[0].radius *
      (2 * this.bars[0].mass +
        this.bars[1].mass -
        this.bars[1].mass *
          this.p5.cos(2 * this.bars[0].angle - 2 * this.bars[1].angle))
    this.bars[0].acceleration = (num1 + num2 + num3 * num4) / den

    num1 = 2 * this.p5.sin(this.bars[0].angle - this.bars[1].angle)
    num2 =
      this.bars[0].velocity *
      this.bars[0].velocity *
      this.bars[0].radius *
      (this.bars[0].mass + this.bars[1].mass)
    num3 =
      this.g *
      (this.bars[0].mass + this.bars[1].mass) *
      this.p5.cos(this.bars[0].angle)
    num4 =
      this.bars[1].velocity *
      this.bars[1].velocity *
      this.bars[1].radius *
      this.bars[1].mass *
      this.p5.cos(this.bars[0].angle - this.bars[1].angle)
    den =
      this.bars[1].radius *
      (2 * this.bars[0].mass +
        this.bars[1].mass -
        this.bars[1].mass *
          this.p5.cos(2 * this.bars[0].angle - 2 * this.bars[1].angle))
    this.bars[1].acceleration = (num1 * (num2 + num3 + num4)) / den

    this.bars.forEach((bar, i) => {
      const prevBar = this.bars[i - 1] || null

      const prevX = prevBar ? prevBar.x : 0
      const prevY = prevBar ? prevBar.y : 0

      bar.x = prevX + bar.radius * this.p5.sin(bar.angle)
      bar.y = prevY + bar.radius * this.p5.cos(bar.angle)

      bar.velocity += bar.acceleration
      bar.angle += bar.velocity

      this.p5
        .line(prevX, prevY, bar.x, bar.y)
        .fill(0)
        .circle(bar.x, bar.y, bar.mass)
    })

    const lastBar = this.bars[this.bars.length - 1]
    if (this.lastEndPosition) {
      this.trace.stroke(0)
      this.trace.line(
        this.lastEndPosition.x,
        this.lastEndPosition.y,
        lastBar.x,
        lastBar.y,
      )
    }

    this.lastEndPosition = {
      x: lastBar.x,
      y: lastBar.y,
    }
  }

  public getDimensions(): IDimensions {
    return { x: 900, y: 900 }
  }
}
