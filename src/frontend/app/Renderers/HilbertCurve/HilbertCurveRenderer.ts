import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions } from '../../_util/Renderer'

export class HilbertCurveRenderer extends AbstractRenderer {
  private order = 4
  private N: number
  private total: number
  private path: Point[]
  private lineWidth: Point
  private halfOfLineWith: Point
  private counter
  private orderSlider

  public getDimensions(): IDimensions {
    return { x: 800, y: 800 }
  }

  public setup() {
    super.setup()
    const { p5 } = this
    p5.background(0)

    this.orderSlider = p5.createSlider(1, 8, this.order, 1)
    this.orderSlider.changed(() => {
      this.order = parseInt(this.orderSlider.value())
      this.calculateConstants()

      this.recalculatePath()
    })
    this.calculateConstants()
    this.recalculatePath()
  }

  private calculateConstants() {
    this.counter = 0
    this.N = 2 ** this.order
    this.total = this.N * this.N
    this.lineWidth = Point.fromObject(this.getDimensions()).divide(
      this.N,
      this.N,
    )
    this.halfOfLineWith = this.lineWidth.divide(2, 2)
  }

  private recalculatePath() {
    this.path = []
    for (let i = 0; i < this.total; i++) {
      const point = this.hilbert(i)
        .multiplyPoint(this.lineWidth)
        .addPoint(this.halfOfLineWith)
      this.path.push(point)
    }
  }

  private hilbert(i: number): Point {
    const points = [
      Point.fromArray([0, 0]),
      Point.fromArray([0, 1]),
      Point.fromArray([1, 1]),
      Point.fromArray([1, 0]),
    ]
    let index = i & 0b11
    let v = points[index]

    for (let j = 1; j < this.order; j++) {
      i = i >>> 2
      index = i & 0b11

      const len = 2 ** j
      if (index === 0) {
        const tmp = v.x
        v.x = v.y
        v.y = tmp
      } else if (index === 1) {
        v.y += len
      } else if (index === 2) {
        v.x += len
        v.y += len
      } else if (index === 3) {
        const tmp = len - 1 - v.x
        v.x = len - 1 - v.y
        v.y = tmp
        v.x += len
      }
    }

    return v
  }

  public draw() {
    const { p5 } = this

    p5.background(0).stroke(255).strokeWeight(2).noFill()

    p5.beginShape()
    for (let i = 0; i < this.counter; i++) {
      const point = this.path[i]
      p5.vertex(point.x, point.y)
    }
    p5.endShape()
    if (this.counter < this.total) {
      this.counter += this.order
      if (this.counter > this.total) {
        this.counter = this.total
      }
    }
  }
}
