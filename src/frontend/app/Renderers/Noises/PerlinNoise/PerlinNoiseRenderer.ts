import { AbstractRenderer, IDimensions } from '../../../_util/Renderer'
import { PerlinNoiseGrid } from '../../../_util/PerlinNoiceGrid'
import { Point } from '../../../_util/Point'
import P5 from 'p5'

export class PerlinNoiseRenderer extends AbstractRenderer {
  private map: PerlinNoiseGrid

  private scaleSlider: P5.Element
  private latestScaleSliderValue: number

  private levelSlider: P5.Element
  private latestLevelSliderValue: number

  setup() {
    super.setup()

    const { p5 } = this

    this.latestScaleSliderValue = 0.01
    this.scaleSlider = p5.createSlider(
      0.0001,
      20,
      this.latestScaleSliderValue,
      0.1,
    )
    this.scaleSlider.size(500)

    this.latestLevelSliderValue = 260
    this.levelSlider = p5.createSlider(
      10,
      10_000,
      this.latestLevelSliderValue,
      10,
    )
    this.levelSlider.size(500)

    this.reset()

    p5.frameRate(5)
  }

  private reset() {
    const scaleSliderValue = parseFloat(this.scaleSlider.value().toString())
    const levelSliderValue = parseFloat(this.levelSlider.value().toString())
    this.map = new PerlinNoiseGrid(
      new Point(this.p5.width, this.p5.height),
      scaleSliderValue,
      levelSliderValue,
    )

    console.log(scaleSliderValue, levelSliderValue)
  }

  draw(): void {
    const { p5 } = this

    const sliderScaleValue = parseFloat(this.scaleSlider.value().toString())
    const sliderLevelValue = parseFloat(this.levelSlider.value().toString())
    if (
      sliderScaleValue !== this.latestScaleSliderValue ||
      sliderLevelValue !== this.latestLevelSliderValue
    ) {
      this.reset()

      this.map.forEachCell(cell => {
        p5.stroke(cell.data)
        p5.point(...cell.position.toArray())
      })
    }
  }
}
