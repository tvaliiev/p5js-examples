import {
  Direction,
  DirectionMoveOpposite,
  randomDirection,
} from '../../_util/Direction'
import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions, IRenderer } from '../../_util/Renderer'
import { randomBetween } from '../../_util/util'
import { Walker } from './Walker'

export class RandomWalker extends AbstractRenderer implements IRenderer {
  public static NAME = 'random-walker'

  private walker: Walker
  public path: Point[] = []

  public getDimensions(): IDimensions {
    return { x: 600, y: 600 }
  }

  public setup() {
    super.setup()

    const { p5 } = this

    const position = new Point(p5.width / 2, p5.height / 2)
    this.walker = new Walker(position)
    this.path.push(position)
  }

  public draw() {
    const { p5 } = this

    p5.background(51).fill(255).stroke(255).strokeWeight(3)

    this.walker.draw(p5)

    const direction: Direction = randomDirection(() => p5.floor(p5.random(4)))
    const newPosition = this.walker.position.addPoint(
      DirectionMoveOpposite[direction],
    )
    this.walker.moveTo(newPosition)

    this.path.push(newPosition)

    p5.strokeWeight(1)
    this.path.forEach(p => p5.point(p.x, p.y))
  }
}
