import P5 from 'p5'
import { Point } from '../../_util/Point'

export class Walker {
  constructor(public position: Point) {}

  public moveTo(position: Point) {
    this.position = position
  }

  public draw(p5: P5) {
    p5.point(this.position.x, this.position.y)
  }
}
