import { AbstractRenderer } from '../../_util/Renderer'

export class SupershapesRenderer extends AbstractRenderer {
  private constants = {
    n1: 1,
    n2: 1,
    n3: 1,
    m: 5,
    a: 1,
    b: 1,
  }

  private sliders = {}

  public setup() {
    super.setup()

    const adders = {
      m: 1,
    }
    const constantChangeSettings = {
      m: {
        step: 1,
      },
    }

    Object.keys(this.constants).forEach(key => {
      const changeSetting = constantChangeSettings[key] || {}
      this.sliders[key] = this.p5.createSlider(
        changeSetting.min || 0,
        changeSetting.max || 20,
        this.constants[key],
        changeSetting.step || 0.001,
      )

      this.sliders[key].changed(e => {
        this.constants[key] = e.target.value
      })
    })
  }

  public draw() {
    const { p5 } = this

    p5.translate(p5.width / 2, p5.height / 2)
      .background(51)
      .stroke(255)
      .noFill()

    p5.beginShape()
    const radius = 100

    const total = 300
    const increment = p5.TWO_PI / total
    for (let angle = 0; angle < p5.TWO_PI; angle += increment) {
      const r = this.superShape(angle) * radius

      const x = r * Math.cos(angle)
      const y = r * Math.sin(angle)

      p5.vertex(x, y)
    }
    p5.endShape(p5.CLOSE)
  }

  private superShape(angle: number): number {
    const { a, m, n1, n3, n2, b } = this.constants

    let part1 = Math.abs((1 / a) * Math.cos((angle * m) / 4))
    part1 = Math.pow(part1, n2)

    let part2 = Math.abs((1 / b) * Math.sin((angle * m) / 4))
    part2 = Math.pow(part2, n3)

    let part3 = Math.pow(part1 + part2, 1 / n1)

    if (part3 === 0) {
      return 0
    }

    return 1 / part3
  }
}
