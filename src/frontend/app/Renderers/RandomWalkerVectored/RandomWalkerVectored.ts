import {
  Direction,
  DirectionMoveOpposite,
  randomDirection,
} from '../../_util/Direction'
import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions, IRenderer } from '../../_util/Renderer'
import { randomBetween } from '../../_util/util'
import { Walker } from './Walker'

export class RandomWalkerVectored
  extends AbstractRenderer
  implements IRenderer
{
  public static NAME = 'random-walker-vectored'

  private walker: Walker
  private prevPosition: Point
  private currentPosition: Point

  public getDimensions(): IDimensions {
    return { x: 600, y: 600 }
  }

  public setup() {
    super.setup()

    const { p5 } = this

    this.currentPosition = new Point(p5.width / 2, p5.height / 2)
    this.walker = new Walker(this.currentPosition)
    this.prevPosition = this.currentPosition.clone()

    p5.background(51)
  }

  public draw() {
    const { p5 } = this

    p5.fill(255).stroke(255).strokeWeight(1)

    this.prevPosition = this.currentPosition.clone()

    const bigStepProb = p5.random(100)
    const multiplyBy = randomBetween(25, 100)
    this.currentPosition = Point.random()
      .when(bigStepProb < 2, p => p.multiply(multiplyBy, multiplyBy))
      .when(bigStepProb >= 2, p => p.setMagnitude(2))
      .addPoint(this.walker.position)

    this.walker.moveTo(this.currentPosition)
    this.walker.draw(p5)

    p5.line(
      this.currentPosition.x,
      this.currentPosition.y,
      this.prevPosition.x,
      this.prevPosition.y,
    )
  }
}
