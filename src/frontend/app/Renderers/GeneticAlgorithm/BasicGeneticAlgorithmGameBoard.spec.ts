import { Point } from '../../_util/Point'
import {
  predictableRandom,
  restoreRandom,
} from '../../_util/test/restoreRandom'
import { BasicGeneticAlgorithmGameBoard } from './BasicGeneticAlgorithmGameBoard'
import { CellOccupant } from './CellOccupant'

describe('BasicGeneticAlgorithmBoard.generateInitialBots', () => {
  restoreRandom()

  it('should generate initial bots', () => {
    predictableRandom('1')
    const board = new BasicGeneticAlgorithmGameBoard(Point.fromArray([30, 30]))
    board.init()

    // @ts-ignore
    board.generateInitialBots()
    board.bots.forEach(bot => {
      const cell = board.getCellFromGridCoord(bot.position)
      expect(cell.data.occupant).toEqual(CellOccupant.BOT)
      expect(cell.data.occupantData).toEqual(bot)
    })
    expect(board).toMatchSnapshot()
  })
})
