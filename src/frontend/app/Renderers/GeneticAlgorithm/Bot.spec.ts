import seedrandom from 'seedrandom'
import { restoreRandom } from '../../_util/test/restoreRandom'
import { range } from '../../_util/util'
import { Bot } from './Bot'

describe('Bot.GenerateRandom', () => {
  let originalRandom = Math.random
  afterEach(() => {
    Math.random = originalRandom
  })

  it('should generate random program with 56 cells 0 - 55 each cell', () => {
    seedrandom('1', { global: true })
    const bot = Bot.GenerateRandom({ x: 0, y: 0 })
    expect(bot).toMatchSnapshot()
    expect(bot.program.length).toEqual(56)

    const set = range(100)
      .map(() => Bot.GenerateRandom({ x: 0, y: 0 }).program)
      .flat()
    const highest = Math.max(...set)
    const lowest = Math.min(...set)

    expect(highest).toEqual(55)
    expect(lowest).toEqual(0)
  })
})

describe('Bot.GenerateFrom', () => {
  restoreRandom()

  it('should generate same bot when no mutation triggered', () => {
    seedrandom('1', { global: true })
    const bot = Bot.GenerateRandom({ x: 0, y: 0 })
    const copy = Bot.GenerateFrom(bot)

    expect(bot.program).toEqual(copy.program)
    expect(bot.program).not.toBe(copy.program)
    expect(bot.program === copy.program).toBe(false)
    expect(copy).toMatchSnapshot()
  })

  it('should generate same bot when mutation is triggered', () => {
    seedrandom('5', { global: true })
    const bot = Bot.GenerateRandom({ x: 0, y: 0 })
    const copy = Bot.GenerateFrom(bot)

    expect(bot.program).not.toEqual(copy.program)
    expect(copy).toMatchSnapshot()
  })
})

describe('Bot.advancePointer', () => {
  it('should advance pointer in range 1', () => {
    const bot = Bot.GenerateRandom({ x: 0, y: 0 })
    bot.pointer = 0
    bot.advancePointer(20)
    expect(bot.pointer).toEqual(20)
  })

  it('should advance pointer in range 2', () => {
    const bot = Bot.GenerateRandom({ x: 0, y: 0 })
    bot.pointer = 0
    bot.advancePointer(100)
    expect(bot.pointer).toEqual(44)
  })
})
