import { IPoint } from '../../_util/Point'
import { randomFloat, randomInt, range } from '../../_util/util'

export type Command = number
export type Program = Command[]
export const PROGRAM_SIZE = 56

export class Bot {
  private static readonly MAX_COMMAND = 56

  constructor(
    public program: Program,
    public position: IPoint = { x: -1, y: 0 },
    public resources = 99,
    public pointer = 0,
  ) {}

  public static GenerateRandom(position: IPoint): Bot {
    return new Bot(
      range(PROGRAM_SIZE).map(() => randomInt(this.MAX_COMMAND)),
      position,
    )
  }

  public static GenerateFrom(bot: Bot): Bot {
    const newBot = new Bot([...bot.program])

    if (randomFloat() < 0.2) {
      const instructionsToChange = randomInt(10)
      for (let i = 0; i < instructionsToChange; i++) {
        newBot.program[randomInt(PROGRAM_SIZE - 1)] = randomInt(
          this.MAX_COMMAND,
        )
      }
    }

    return newBot
  }

  /* istanbul ignore next */
  public getCurrentOp(): Command {
    return this.program[this.pointer]
  }

  public advancePointer(by: number) {
    this.pointer += by
    if (this.pointer >= PROGRAM_SIZE) {
      this.pointer -= PROGRAM_SIZE
    }
  }
}
