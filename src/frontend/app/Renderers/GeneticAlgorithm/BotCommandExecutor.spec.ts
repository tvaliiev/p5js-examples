import { Point } from '../../_util/Point'
import {
  predictableRandom,
  restoreRandom,
} from '../../_util/test/restoreRandom'
import { BasicGeneticAlgorithmGameBoard } from './BasicGeneticAlgorithmGameBoard'
import { Bot } from './Bot'
import { BotCommandExecutor } from './BotCommandExecutor'
import { CellOccupant } from './CellOccupant'
import { MinimalBotCountReached } from './MinimalBotCountReached'

describe('BotCommandExecutor.onBotDied', () => {
  it('should just remove bot', () => {
    const bot = Bot.GenerateRandom({ x: 0, y: 0 })

    const removeBotFn = jest.fn()
    const Board = jest.fn().mockImplementation(() => ({
      bots: { length: 7 },
      removeBot: removeBotFn,
    }))
    const board = new Board()
    const e = new BotCommandExecutor(board)

    // @ts-ignore
    e.onBotDied(bot)

    expect(removeBotFn).toHaveBeenCalledWith(bot)
    expect(removeBotFn).toHaveBeenCalledTimes(1)
  })

  it('should throw error when bots length is 10', () => {
    const bot = Bot.GenerateRandom({ x: 0, y: 0 })

    const removeBotFn = jest.fn()
    const Board = jest.fn().mockImplementation(() => ({
      bots: { length: 10 },
      removeBot: removeBotFn,
    }))
    const board = new Board()
    const e = new BotCommandExecutor(board)

    expect(() => {
      // @ts-ignore
      e.onBotDied(bot)
    }).toThrow(MinimalBotCountReached)

    expect(removeBotFn).toHaveBeenCalledWith(bot)
    expect(removeBotFn).toHaveBeenCalledTimes(1)
  })
})

describe('BotCommandExecutor.decrementBotResources', () => {
  it('should just decrement resources when they are greater then 2', () => {
    const bot = Bot.GenerateRandom(Point.empty().toObject())
    const Board = jest.fn().mockImplementation(() => ({}))
    const board = new Board()

    bot.resources = 2
    const e = new BotCommandExecutor(board)
    // @ts-ignore
    e.decrementBotResources(bot)

    expect(bot.resources).toEqual(1)
  })
  it('should decrement resources and make bot die when there is <= 0 resources', () => {
    const bot = Bot.GenerateRandom(Point.empty().toObject())
    const removeBotFn = jest.fn()
    const Board = jest.fn().mockImplementation(() => ({
      bots: { length: 50 },
      removeBot: removeBotFn,
    }))
    const board = new Board()

    bot.resources = 1
    const e = new BotCommandExecutor(board)
    // @ts-ignore
    e.decrementBotResources(bot)

    expect(bot.resources).toEqual(0)
    expect(removeBotFn).toHaveBeenCalledWith(bot)
    expect(removeBotFn).toBeCalledTimes(1)
  })
})

describe('BotCommandExecutor.executeCommandFor', () => {
  restoreRandom()

  it.each([
    [[0], Point.fromArray([3, 3]), Point.fromArray([2, 2])],
    [[1], Point.fromArray([3, 3]), Point.fromArray([3, 2])],
    [[2], Point.fromArray([3, 3]), Point.fromArray([4, 2])],
    [[3], Point.fromArray([3, 3]), Point.fromArray([2, 3])],
    [[4], Point.fromArray([3, 3]), Point.fromArray([4, 3])],
    [[5], Point.fromArray([3, 3]), Point.fromArray([2, 4])],
    [[6], Point.fromArray([3, 3]), Point.fromArray([3, 4])],
    [[7], Point.fromArray([3, 3]), Point.fromArray([4, 4])],
  ])(
    'should move with program %p from point %p to point %p when there is empty cell',
    (program, fromPoint, toPoint) => {
      predictableRandom('4')

      const board = new BasicGeneticAlgorithmGameBoard(
        Point.fromArray([30, 30]),
      )
      board.init(false)
      const e = new BotCommandExecutor(board)

      const bot = Bot.GenerateRandom(fromPoint)
      bot.pointer = 0
      bot.program = program

      e.executeCommandFor(bot)
      expect(bot.position).toEqual(toPoint.toObject())
      expect(bot.pointer).toEqual(1)
      expect(bot.resources).toEqual(98)
    },
  )

  it('should not move when bot in destination', () => {
    const board = new BasicGeneticAlgorithmGameBoard(Point.fromArray([30, 30]))
    board.init()
    const e = new BotCommandExecutor(board)

    const bot = Bot.GenerateRandom({ x: 3, y: 3 })
    const bot2 = Bot.GenerateRandom({ x: 3, y: 2 })
    bot.pointer = 0
    bot.program = [1]
    board.moveBotTo(bot, board.getCellFromGridCoord(bot.position))
    board.moveBotTo(bot2, board.getCellFromGridCoord(bot2.position))

    e.executeCommandFor(bot)
    expect(bot.position).toEqual({ x: 3, y: 3 })
    expect(bot.pointer).toEqual(5)
    expect(bot.resources).toEqual(98)
  })

  it('should not move when wall in destination', () => {
    const board = new BasicGeneticAlgorithmGameBoard(Point.fromArray([30, 30]))
    board.init()
    const e = new BotCommandExecutor(board)

    const bot = Bot.GenerateRandom({ x: 3, y: 3 })
    bot.pointer = 0
    bot.program = [1]
    board.moveBotTo(bot, board.getCellFromGridCoord(bot.position))
    board.getCellFromGridCoord({ x: 3, y: 2 }).data.occupant = CellOccupant.WALL

    e.executeCommandFor(bot)
    expect(bot.position).toEqual({ x: 3, y: 3 })
    expect(bot.pointer).toEqual(3)
    expect(bot.resources).toEqual(98)
  })

  it('should die when poison in destination', () => {
    const board = new BasicGeneticAlgorithmGameBoard(Point.fromArray([30, 30]))
    board.init()
    const e = new BotCommandExecutor(board)

    const originalCell = board.getCellFromGridCoord({ x: 3, y: 3 })
    const bot = Bot.GenerateRandom(originalCell.position.toObject())
    const bot2 = Bot.GenerateRandom({ x: 4, y: 4 })
    bot.pointer = 0
    bot.program = [1]
    board.bots = [bot, bot2]
    board.moveBotTo(bot, board.getCellFromGridCoord(bot.position))
    board.moveBotTo(bot2, board.getCellFromGridCoord(bot2.position))
    const targetCell = board.getCellFromGridCoord({ x: 3, y: 2 })
    targetCell.data.occupant = CellOccupant.POISON
    const originalPoisonCount = ++board.poisonCount

    e.executeCommandFor(bot)

    expect(board.bots).toEqual([bot2])
    expect(board.poisonCount).toEqual(originalPoisonCount - 1)
    expect(targetCell.data.occupant).toEqual(CellOccupant.EMPTY)
    expect(targetCell.data.occupantData).toBeNull()
    expect(originalCell.data.occupant).toEqual(CellOccupant.EMPTY)
    expect(originalCell.data.occupantData).toBeNull()
    expect(bot.resources).toEqual(98)
  })

  it('should eat when food in destination', () => {
    const board = new BasicGeneticAlgorithmGameBoard(Point.fromArray([30, 30]))
    board.init()
    const e = new BotCommandExecutor(board)

    const originalCell = board.getCellFromGridCoord({ x: 3, y: 3 })
    const bot = Bot.GenerateRandom(originalCell.position.toObject())
    bot.pointer = 0
    bot.program = [1]
    board.bots = [bot]
    board.moveBotTo(bot, board.getCellFromGridCoord(bot.position))
    const targetCell = board.getCellFromGridCoord({ x: 3, y: 2 })
    targetCell.data.occupant = CellOccupant.FOOD
    const originalFoodCount = ++board.foodCount

    e.executeCommandFor(bot)

    expect(board.bots).toEqual([bot])
    expect(board.foodCount).toEqual(originalFoodCount - 1)
    expect(targetCell.data.occupant).toEqual(CellOccupant.BOT)
    expect(targetCell.data.occupantData).toEqual(bot)
    expect(originalCell.data.occupant).toEqual(CellOccupant.EMPTY)
    expect(originalCell.data.occupantData).toBeNull()
    expect(bot.resources).toEqual(108)
  })

  it('should grab and eat food in destination', () => {
    const board = new BasicGeneticAlgorithmGameBoard(Point.fromArray([30, 30]))
    board.init()
    const e = new BotCommandExecutor(board)

    const originalCell = board.getCellFromGridCoord({ x: 3, y: 3 })
    const bot = Bot.GenerateRandom(originalCell.position.toObject())
    bot.pointer = 0
    bot.program = [9]
    board.bots = [bot]
    board.moveBotTo(bot, board.getCellFromGridCoord(bot.position))
    const targetCell = board.getCellFromGridCoord({ x: 3, y: 2 })
    targetCell.data.occupant = CellOccupant.FOOD
    const originalFoodCount = ++board.foodCount

    e.executeCommandFor(bot)

    expect(board.bots).toEqual([bot])
    expect(board.foodCount).toEqual(originalFoodCount - 1)
    expect(targetCell.data.occupant).toEqual(CellOccupant.EMPTY)
    expect(targetCell.data.occupantData).toBeNull()
    expect(originalCell.data.occupant).toEqual(CellOccupant.BOT)
    expect(originalCell.data.occupantData).toEqual(bot)
    expect(bot.resources).toEqual(108)
    expect(bot.position).toEqual(originalCell.position)
  })

  it('should transform poison to food when grabbing in destination', () => {
    const board = new BasicGeneticAlgorithmGameBoard(Point.fromArray([30, 30]))
    board.init()
    const e = new BotCommandExecutor(board)

    const originalCell = board.getCellFromGridCoord({ x: 3, y: 3 })
    const bot = Bot.GenerateRandom(originalCell.position.toObject())
    bot.pointer = 0
    bot.program = [9]
    board.bots = [bot]
    board.moveBotTo(bot, board.getCellFromGridCoord(bot.position))
    const targetCell = board.getCellFromGridCoord({ x: 3, y: 2 })
    targetCell.data.occupant = CellOccupant.POISON
    const originalPoisonCount = ++board.poisonCount
    const originalFoodCount = board.foodCount

    e.executeCommandFor(bot)

    expect(board.bots).toEqual([bot])
    expect(board.poisonCount).toEqual(originalPoisonCount - 1)
    expect(board.foodCount).toEqual(originalFoodCount + 1)
    expect(targetCell.data.occupant).toEqual(CellOccupant.FOOD)
    expect(targetCell.data.occupantData).toBeNull()
    expect(originalCell.data.occupant).toEqual(CellOccupant.BOT)
    expect(originalCell.data.occupantData).toEqual(bot)
    expect(bot.position).toEqual(originalCell.position)
    expect(bot.resources).toEqual(98)
  })

  it('should just peek and move pointer, without changing anything external', () => {
    const board = new BasicGeneticAlgorithmGameBoard(Point.fromArray([30, 30]))
    board.init()
    const e = new BotCommandExecutor(board)

    const originalCell = board.getCellFromGridCoord({ x: 3, y: 3 })
    const bot = Bot.GenerateRandom(originalCell.position.toObject())
    bot.pointer = 0
    bot.program = [17]
    board.bots = [bot]
    board.moveBotTo(bot, board.getCellFromGridCoord(bot.position))
    const targetCell = board.getCellFromGridCoord({ x: 3, y: 2 })
    targetCell.data.occupant = CellOccupant.POISON
    const originalPoisonCount = ++board.poisonCount
    const originalFoodCount = board.foodCount

    e.executeCommandFor(bot)

    expect(board.bots).toEqual([bot])
    expect(board.poisonCount).toEqual(originalPoisonCount)
    expect(board.foodCount).toEqual(originalFoodCount)
    expect(targetCell.data.occupant).toEqual(CellOccupant.POISON)
    expect(targetCell.data.occupantData).toBeNull()
    expect(originalCell.data.occupant).toEqual(CellOccupant.BOT)
    expect(originalCell.data.occupantData).toEqual(bot)
    expect(bot.position).toEqual(originalCell.position)
    expect(bot.resources).toEqual(98)
  })
})
