export enum CellOccupant {
  EMPTY,
  FOOD,
  WALL,
  POISON,
  BOT,
}
