import { Color } from '../../_util/Color'
import { CellConfig, GameBoardConfig } from '../../_util/GameBoard'
import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions } from '../../_util/Renderer'
import { BasicGeneticAlgorithmGameBoard } from './BasicGeneticAlgorithmGameBoard'
import { range } from '../../_util/util'

export class BasicGeneticAlgorithmRenderer extends AbstractRenderer {
  public static NAME = 'Basic Genetic Algorithm'
  private board: BasicGeneticAlgorithmGameBoard
  private speedSlider
  private renderCheckbox
  private skipToAmountInput
  private skipButton

  public getDimensions(): IDimensions {
    return { x: 1200, y: 800 }
  }

  public setup() {
    super.setup()
    const { p5 } = this
    this.speedSlider = this.p5.createSlider(0, 500, 1, 1)
    this.renderCheckbox = this.p5.createCheckbox('Render?', true)
    this.skipToAmountInput = this.p5.createInput('500', 'number')
    this.skipButton = p5.createButton('Skip')
    this.skipButton.mousePressed(() => {
      const skipTo =
        parseInt(this.skipToAmountInput.value()) + this.board.generation
      for (let i = 0; skipTo > this.board.generation; i++) {
        this.board.tick()
      }
    })
    p5.textSize(10)

    this.board = new BasicGeneticAlgorithmGameBoard(
      Point.empty(),
      new GameBoardConfig(2, new CellConfig(15, 15, new Color(0, 0, 0, 0))),
    )
    this.board.changeSizeToFit(
      Point.fromObject(this.getDimensions()).subtract(200, 0),
    )
    this.board.init(true)
    // this.p5.frameRate(1)
  }

  public draw(): void {
    const { p5 } = this
    p5.background(25)

    const speed = parseInt(this.speedSlider.value())
    range(speed).forEach(() => {
      this.board.tick()
    })
    if (this.renderCheckbox.checked()) {
      this.board.draw(p5)
    }

    const dimensions = this.getDimensions()
    const avg = Math.round(
      this.board.generationResults.getItems().reduce((acc, e) => acc + e, 0) /
        this.board.generationResults.getItems().length,
    )
    p5.textSize(15)
      .fill(100, 100, 100)
      .text(
        `Current generation:\n${
          this.board.generation
        }\nGeneration results:\n${this.board.generationResults
          .getItems()
          .join(`\n`)}\nAverage: ${avg}`,
        dimensions.x - 210,
        20,
      )
      .textSize(10)
  }
}
