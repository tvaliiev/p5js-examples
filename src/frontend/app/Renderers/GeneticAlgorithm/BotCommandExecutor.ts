import { BasicGeneticAlgorithmGameBoard } from './BasicGeneticAlgorithmGameBoard'
import { Bot } from './Bot'
import { CellOccupant } from './CellOccupant'
import { MinimalBotCountReached } from './MinimalBotCountReached'

export class BotCommandExecutor {
  constructor(private board: BasicGeneticAlgorithmGameBoard) {}

  public executeCommandFor(bot: Bot) {
    let commandsCounter = 0
    while (commandsCounter < 11) {
      let command = bot.getCurrentOp()
      if (command === undefined) {
        break
      }
      const currentCell = this.board.getCellFromGridCoord(bot.position)

      // MOVE
      if (command <= 7) {
        const cells = this.board.getCellsAround(currentCell)

        const targetCell = cells[command]
        const targetCellOccupant = targetCell.data.occupant
        if (
          targetCell.data.occupant === CellOccupant.BOT ||
          targetCell.data.occupant === CellOccupant.WALL
        ) {
          bot.advancePointer(targetCellOccupant + 1)
          break
        }

        if (targetCell.data.occupant === CellOccupant.POISON) {
          this.onBotDied(bot)
          targetCell.data.occupant = CellOccupant.EMPTY
          targetCell.data.occupantData = null
          this.board.poisonCount--
          break
        }

        if (targetCell.data.occupant === CellOccupant.FOOD) {
          bot.resources += 10
          this.board.foodCount--
          targetCell.data.occupant = CellOccupant.EMPTY
        }

        this.board.moveBotTo(bot, targetCell)

        bot.advancePointer(targetCellOccupant + 1)
        break
      }

      // GRAB or TRANSFORM_POISON
      command -= 8
      if (command <= 7) {
        const cells = this.board.getCellsAround(currentCell)
        const targetCell = cells[command]
        const targetCellOccupant = targetCell.data.occupant

        if (targetCellOccupant === CellOccupant.POISON) {
          targetCell.data.occupant = CellOccupant.FOOD
          this.board.poisonCount--
          this.board.foodCount++
        } else if (targetCellOccupant === CellOccupant.FOOD) {
          bot.resources += 10
          this.board.foodCount--
          targetCell.data.occupant = CellOccupant.EMPTY
        }

        bot.advancePointer(targetCellOccupant + 1)
        break
      }

      // PEEK
      command -= 8
      if (command <= 7) {
        const cells = this.board.getCellsAround(currentCell)
        const targetCell = cells[command]
        bot.advancePointer(targetCell.data.occupant + 1)
        commandsCounter++
        continue
      }

      // JUMP
      command -= 8
      bot.advancePointer(command)
      commandsCounter++
    }
    this.decrementBotResources(bot)
  }

  private onBotDied(bot: Bot) {
    this.board.removeBot(bot)

    if (this.board.bots.length === 10) {
      throw new MinimalBotCountReached()
    }
  }

  private decrementBotResources(bot: Bot) {
    bot.resources--
    if (bot.resources <= 0) {
      this.onBotDied(bot)
    }
  }
}
