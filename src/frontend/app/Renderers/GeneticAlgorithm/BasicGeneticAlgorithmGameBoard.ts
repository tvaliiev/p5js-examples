import P5 from 'p5'
import { Cell } from '../../_util/Cell'
import { FixedSizeArray } from '../../_util/FixedSizeArray'
import { GameBoard } from '../../_util/GameBoard'
import { IPoint, Point } from '../../_util/Point'
import { randomFloat, range } from '../../_util/util'
import { Bot } from './Bot'
import { BotCommandExecutor } from './BotCommandExecutor'
import { CellOccupant } from './CellOccupant'
import { MinimalBotCountReached } from './MinimalBotCountReached'

export interface BasicGeneticAlgCellData {
  occupant: CellOccupant
  occupantData: Bot | null
}

export class BasicGeneticAlgorithmGameBoard extends GameBoard<BasicGeneticAlgCellData> {
  private readonly MAX_BOTS = 80

  public bots: Bot[] = []
  public foodCount = 0
  public poisonCount = 0
  public generation = 1
  public generationResults: FixedSizeArray<number> = new FixedSizeArray<number>(
    15,
  )

  protected defaultCellFactory(pos: Point): BasicGeneticAlgCellData {
    return {
      occupant: CellOccupant.EMPTY,
      occupantData: null,
    }
  }

  public init(addRandomBots = false) {
    super.init()
    this.generateInitialScene()

    if (addRandomBots) {
      this.generateInitialBots()
    }
  }

  private generateInitialScene() {
    range(50).forEach(() => this.addCell(CellOccupant.FOOD))
    range(30).forEach(() => this.addCell(CellOccupant.POISON))
    this.generateWalls()
  }

  public tick() {
    try {
      this.processBots()

      this.makeFoodIfNeeded()
      this.makePoisonIfNeeded()
    } catch (e) {
      if (!(e instanceof MinimalBotCountReached)) {
        throw e
      }

      const initialLength = this.bots.length
      const currentGenerationBestResult = this.bots.reduce(
        (acc, bot) => (bot.resources > acc ? bot.resources : acc),
        0,
      )
      this.generationResults.push(currentGenerationBestResult)
      while (this.bots.length != this.MAX_BOTS) {
        for (let i = 0; i < initialLength; i++) {
          const bot = Bot.GenerateFrom(this.bots[i])
          this.bots.push(bot)
          const randomEmptyCell = this.getRandomEmptyCell()

          this.moveBotTo(bot, randomEmptyCell)
        }
      }

      this.init()
      this.generateInitialScene()
      this.generation++
    }
  }

  public moveBotTo(bot: Bot, targetCell: Cell<BasicGeneticAlgCellData>) {
    const cell = this.getCellFromGridCoord(bot.position)
    if (cell) {
      cell.data.occupant = CellOccupant.EMPTY
      cell.data.occupantData = null
    }

    targetCell.data.occupant = CellOccupant.BOT
    targetCell.data.occupantData = bot
    bot.position = targetCell.position
  }

  private processBots() {
    const executor = new BotCommandExecutor(this)
    this.bots.forEach(bot => {
      executor.executeCommandFor(bot)
    })
  }

  private makePoisonIfNeeded() {
    while (this.poisonCount <= 70) {
      this.addCell(CellOccupant.POISON)
      this.poisonCount++
    }
  }

  private makeFoodIfNeeded() {
    while (this.foodCount <= 300) {
      this.addCell(CellOccupant.FOOD)
      this.foodCount++
    }
  }

  private addCell(what: CellOccupant) {
    this.getRandomEmptyCell().data.occupant = what
  }

  public getRandomEmptyCell(): Cell<BasicGeneticAlgCellData> {
    return this.grid.randomCell(
      cell => cell.data.occupant === CellOccupant.EMPTY,
      10,
    )
  }

  private generateInitialBots() {
    range(this.MAX_BOTS).forEach(() => {
      const randomCell = this.getRandomEmptyCell()

      const bot = Bot.GenerateRandom(randomCell.position)

      randomCell.data.occupant = CellOccupant.BOT
      randomCell.data.occupantData = bot

      this.bots.push(bot)
    })
  }

  /* istanbul ignore next */
  protected drawCell(
    p5: P5,
    coordinates: IPoint,
    cell: Cell<BasicGeneticAlgCellData>,
  ) {
    const { width, height } = this.config.cellConfig
    if (cell.data.occupant === CellOccupant.EMPTY) {
      p5.fill(44, 44, 44).square(coordinates.x, coordinates.y, width)
      return
    }

    if (cell.data.occupant === CellOccupant.FOOD) {
      p5.fill(0, 200, 0).square(coordinates.x, coordinates.y, width, height)
      return
    }

    if (cell.data.occupant === CellOccupant.BOT) {
      p5.fill(0, 0, 200).square(coordinates.x, coordinates.y, width)
      if (cell.data.occupantData) {
        p5.fill(255, 255, 255).text(
          cell.data.occupantData.resources,
          coordinates.x + 1,
          coordinates.y + 11,
        )
      }
      return
    }

    if (cell.data.occupant === CellOccupant.POISON) {
      p5.fill(200, 0, 0).square(coordinates.x, coordinates.y, width, height)
      return
    }

    if (cell.data.occupant === CellOccupant.WALL) {
      p5.fill(100, 100, 100).square(coordinates.x, coordinates.y, width)
      return
    }
  }

  private generateWalls() {
    const dimensions = this.grid.size
    range(dimensions.x).forEach(x => {
      this.grid.getCellAt({ x, y: 0 }).data.occupant = CellOccupant.WALL
      this.grid.getCellAt({ x, y: dimensions.y - 1 }).data.occupant =
        CellOccupant.WALL
    })

    range(dimensions.y).forEach(y => {
      this.grid.getCellAt({ y, x: 0 }).data.occupant = CellOccupant.WALL
      this.grid.getCellAt({ y, x: dimensions.x - 1 }).data.occupant =
        CellOccupant.WALL
    })

    const leftWallPositionX = Math.round(dimensions.x / 3)
    range(Math.round(dimensions.y / 2)).forEach(y => {
      this.grid.getCellAt({ y, x: leftWallPositionX }).data.occupant =
        CellOccupant.WALL
    })

    const rightWallPositionX = dimensions.x - Math.round(dimensions.x / 3)
    range(Math.round(dimensions.y / 2), dimensions.y - 1, -1).forEach(y => {
      this.grid.getCellAt({ y, x: rightWallPositionX }).data.occupant =
        CellOccupant.WALL
    })
  }

  /* istanbul ignore next */
  public draw(p5: P5) {
    super.draw(p5)
  }

  public removeBot(bot: Bot) {
    const targetIndex = this.bots.findIndex(b => b === bot)
    this.bots.splice(targetIndex, 1)
    const botCell = this.grid.getCellAt(bot.position)
    botCell.data = { occupant: CellOccupant.EMPTY, occupantData: null }
  }
}
