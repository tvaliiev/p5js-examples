import P5 from 'p5'
import { Point } from '../../_util/Point'

export class Leaf {
  constructor(public position: Point) {}

  draw(p5: P5) {
    p5.circle(this.position.x, this.position.y, 3)
  }
}
