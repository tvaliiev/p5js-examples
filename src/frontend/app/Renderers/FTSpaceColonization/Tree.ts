import P5 from 'p5'
import {
  BreakException,
  ignoreBreakException,
} from '../../_util/Exceptions/BreakException'
import { Point } from '../../_util/Point'
import { range } from '../../_util/util'
import { Branch } from './Branch'
import { Leaf } from './Leaf'

export class Tree {
  leaves: Leaf[] = []
  branches: Branch[] = []

  constructor(
    private p5: P5,
    leavesCount = 100,
    private growSettings = { maxDistance: 30, minDistance: 10 },
  ) {
    const { width, height } = p5
    range(leavesCount).forEach(i => {
      this.leaves.push(
        new Leaf(new Point(p5.random(width), p5.random(height - 100))),
      )
    })

    let current = new Branch(
      new Point(p5.width / 2, p5.height),
      new Point(0, -1),
    )
    this.branches.push(current)

    let found = false

    while (!found) {
      const leafWithDistance = this.leaves.find(leaf => {
        const d = leaf.position.distance(current.position)
        return d < this.growSettings.maxDistance
      })

      found = !!leafWithDistance

      if (!found) {
        current = current.next()
        this.branches.push(current)
      }
    }
  }

  draw() {
    this.leaves.forEach(leaf => {
      leaf.draw(this.p5)
    })

    this.branches.forEach(branch => {
      branch.draw(this.p5)
    })

    this.grow()
  }

  grow() {
    const leavesToDelete = []
    this.leaves.forEach(leaf => {
      let closest = null
      ignoreBreakException(() => {
        closest = this.branches.reduce(
          (closest, curr) => {
            const distanceToCurrent = leaf.position.distance(curr.position)
            if (distanceToCurrent < this.growSettings.minDistance) {
              leavesToDelete.push(leaf)
              throw new BreakException(leaf)
            }

            if (distanceToCurrent > this.growSettings.maxDistance) {
              return closest
            }

            if (
              closest.branch === null ||
              distanceToCurrent < closest.distance
            ) {
              return {
                branch: curr,
                distance: distanceToCurrent,
              }
            }

            return closest
          },
          { branch: null, distance: Infinity },
        )
      })

      const closestBranch: Branch | null = closest ? closest.branch : null
      if (closestBranch === null) {
        return
      }

      const newDirection = leaf.position
        .subtractPoint(closestBranch.position)
        .normalize()
      closestBranch.direction = closestBranch.direction.addPoint(newDirection)
      closestBranch.found++
    })

    this.leaves = this.leaves.filter(leaf => {
      return !leavesToDelete.find(e => e === leaf)
    })

    this.branches.forEach(branch => {
      if (branch.found === 0) {
        branch.reset()
        return
      }

      branch.direction = branch.direction.divide(branch.found, branch.found)
      this.branches.push(branch.next())

      branch.reset()
    })
  }
}
