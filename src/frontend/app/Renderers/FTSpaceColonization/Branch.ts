import P5 from 'p5'
import { Point } from '../../_util/Point'

export class Branch {
  public found = 0
  private length = 5
  private readonly originalDirection: Point

  constructor(
    public position: Point,
    public direction: Point,
    public parent: Branch | null = null,
  ) {
    this.originalDirection = direction
  }

  reset() {
    this.found = 0
    this.direction = this.originalDirection
  }

  next(): Branch {
    return new Branch(
      this.position.addPoint(this.direction.multiply(this.length, this.length)),
      this.direction,
      this,
    )
  }

  draw(p5: P5) {
    if (this.parent === null) {
      return
    }

    p5.stroke(255)
    p5.line(
      this.position.x,
      this.position.y,
      this.parent.position.x,
      this.parent.position.y,
    )
  }
}
