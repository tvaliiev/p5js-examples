import { AbstractRenderer, IDimensions } from '../../_util/Renderer'
import { Tree } from './Tree'

export class FTSpaceColonizationRenderer extends AbstractRenderer {
  private tree: Tree

  private minDistInput
  private maxDistInput

  public getDimensions(): IDimensions {
    return { x: 800, y: 800 }
  }

  public setup() {
    super.setup()

    const { p5 } = this

    this.minDistInput = p5.createSlider(5, 100, 10, 0.1)
    this.maxDistInput = p5.createSlider(5, 200, 30, 0.1)

    this.createTree()

    this.minDistInput.changed(() => this.createTree())
    this.maxDistInput.changed(() => this.createTree())
  }

  private createTree() {
    this.tree = new Tree(this.p5, 1000, this.getGrowSettings())
  }

  private getGrowSettings() {
    return {
      minDistance: this.minDistInput.value(),
      maxDistance: this.maxDistInput.value(),
    }
  }

  public draw() {
    const { p5 } = this

    p5.background(51)

    this.tree.draw()
  }
}
