import { Cell } from '../../_util/Cell'
import { IPoint, Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions } from '../../_util/Renderer'
import {
  CellConfig,
  GameOfLifeBoard,
  GameOfLifeBoardConfig,
} from './GameOfLifeBoard'

export class GameOfLifeRenderer extends AbstractRenderer {
  public static NAME = 'game-of-life'
  private board: GameOfLifeBoard
  private pauseBtn
  private onPause = true

  private clearBtn

  private saveBtn
  private saveSlot: GameOfLifeBoard

  private restoreBtn

  private lastChangedCell

  public getDimensions(): IDimensions {
    return { x: 1200, y: 900 }
  }

  public setup() {
    super.setup()

    const { p5 } = this
    this.clearBtn = p5.createButton('Clear')
    this.clearBtn.position(90, 30)
    this.clearBtn.mousePressed(() => {
      this.board.clear()
      if (!this.onPause) {
        this.changePauseState()
      }
    })

    this.pauseBtn = p5.createButton('Start')
    this.pauseBtn.position(30, 30)
    this.pauseBtn.mousePressed(this.changePauseState.bind(this))

    this.saveBtn = p5.createButton('Save')
    this.saveBtn.position(150, 30)
    this.saveBtn.mousePressed(() => {
      this.saveSlot = this.board.clone()
    })

    this.restoreBtn = p5.createButton('Restore')
    this.restoreBtn.position(210, 30)
    this.restoreBtn.mousePressed(() => {
      if (!this.saveSlot) {
        return
      }

      this.board = this.saveSlot.clone()
    })

    this.board = new GameOfLifeBoard(
      new Point(
        Math.floor((p5.width - 75 * 2) / 12),
        Math.floor((p5.height - 75 * 2) / 12),
      ),
      new GameOfLifeBoardConfig(2, new CellConfig(10, 10)),
    )
    this.board.init()

    // this.board.randomizeCells()
  }

  public changePauseState() {
    if (this.onPause) {
      this.pauseBtn.html('Pause')
    } else {
      this.pauseBtn.html('Start')
    }

    this.onPause = !this.onPause
  }

  public mouseClicked() {
    this.swapCellIfNotChangedRecently()
  }

  private getCellAt(position: IPoint): Cell<any> {
    let { x, y } = position
    x -= 75
    y -= 75

    if (
      x <= 0 ||
      y <= 0 ||
      x >= this.board.getTotalWidth() ||
      y >= this.board.getTotalHeight()
    ) {
      return
    }

    return this.board.getCellFromAbsCoord({ x, y })
  }

  private swapCellData(cell: Cell<any>): Cell<any> {
    cell.data.alive = !cell.data.alive

    this.lastChangedCell = cell

    return cell
  }

  public mouseDragged() {
    this.swapCellIfNotChangedRecently()
  }

  private swapCellIfNotChangedRecently() {
    const { p5 } = this
    const cell = this.getCellAt({
      x: p5.mouseX,
      y: p5.mouseY,
    })

    if (!cell || this.lastChangedCell === cell) {
      return
    }

    this.swapCellData(cell)
  }

  public draw() {
    const { p5 } = this
    p5.background(51).translate(75, 75)

    this.board.draw(p5)

    if (!this.onPause) {
      this.board.tick()
    }
  }
}
