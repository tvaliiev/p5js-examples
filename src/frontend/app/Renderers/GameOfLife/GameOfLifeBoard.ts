import P5 from 'p5'
import { Cell } from '../../_util/Cell'
import { GameBoard } from '../../_util/GameBoard'
import { Grid } from '../../_util/Grid'
import { IPoint, Point } from '../../_util/Point'
import { Color } from '../../_util/Color'

type CellData = {
  alive: boolean
}

export class CellConfig {
  constructor(
    public width: number = 40,
    public height: number = 40,
    public background: Color = new Color(44, 44, 44),
  ) {}
}

export class GameOfLifeBoardConfig {
  constructor(
    public spacing: number = 20,
    public cellConfig: CellConfig = new CellConfig(),
    public background: Color = new Color(22, 22, 22),
  ) {}
}

export class GameOfLifeBoard extends GameBoard<CellData> {
  constructor(
    public size: IPoint,
    protected config: GameOfLifeBoardConfig = new GameOfLifeBoardConfig(),
  ) {
    super(size)
  }

  protected defaultCellFactory(pos: Point): CellData {
    return { alive: false }
  }

  protected drawCell(p5: P5, coordinates: IPoint, cell: Cell<CellData>) {
    if (!cell.data.alive) {
      return
    }

    const { width, height } = this.config.cellConfig

    p5.fill(255).stroke(100).rect(coordinates.x, coordinates.y, width, height)
  }

  public tick() {
    const newGrid = this.getNewGrid()

    this.grid.forEachCell(cell => {
      const newGridCell = newGrid.getCellAt(cell.position)
      const cells = this.grid.getCellsAround(cell.position)

      const { alive } = cells.reduce(
        (acc, cell) => {
          if (cell.data.alive) {
            acc.alive++
          } else {
            acc.dead++
          }

          return acc
        },
        { alive: 0, dead: 0 },
      )

      if (!cell.data.alive && alive === 3) {
        newGridCell.data.alive = true
      } else if (cell.data.alive && (alive < 2 || alive > 3)) {
        newGridCell.data.alive = false
      } else {
        newGridCell.data.alive = cell.data.alive
      }
    })

    this.grid = newGrid
  }

  public clone(): GameOfLifeBoard {
    const clone = new GameOfLifeBoard(this.size, this.config)

    clone.grid = this.grid.clone()

    return clone
  }
}
