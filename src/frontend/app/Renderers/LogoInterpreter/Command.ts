import P5 from 'p5'
import { Turtle } from './Turtle'

export abstract class Command {
  public static NAME
  public readonly NEED_ARG: boolean = true

  protected arg: string

  public setArg(arg: string) {
    this.arg = arg
  }

  public needArgument(): boolean {
    return this.NEED_ARG
  }

  public abstract execute(p5: P5, turtle: Turtle): void
}
