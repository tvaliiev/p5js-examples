import P5 from 'p5'
import { AbstractRenderer } from '../../_util/Renderer'
import { LocalStorage } from '../../_util/LocalStorage'
import { Parser } from './Parser'
import { Turtle } from './Turtle'

export class LogoInterpreterRenderer extends AbstractRenderer {
  public static NAME = 'logo-turtle-interpreter'

  private textElement: P5.Element
  private storage: LocalStorage = new LocalStorage(LogoInterpreterRenderer.NAME)

  public setup() {
    super.setup()

    const { p5 } = this
    p5.angleMode(p5.DEGREES)
    const initialCode =
      this.storage.getItem('code') ||
      'repeat 3 [ repeat 3 [ fd 60 rt 120 ] lt 120 pu fd 60 pd ]'
    this.textElement = p5.createElement('textarea', initialCode)
    this.textElement.attribute('cols', '80')
    this.textElement.attribute('rows', '5')
    // @ts-ignore
    this.textElement.input(() => this.interpretCode())

    this.interpretCode()
  }

  private interpretCode() {
    const p5 = this.p5
    p5.background(255)
    p5.push()
    try {
      const turtle = new Turtle({ x: p5.width / 2, y: p5.width / 2 }, 0, p5)
      const code = this.getCode()
      new Parser(code).parseAndExecute(this.p5, turtle)
    } catch (e) {
      console.error(e)
    }
    p5.pop()
  }

  private getCode() {
    const value = this.textElement.value() as string
    this.storage.setItem('code', value)
    return value
  }

  public draw() {
    const { p5 } = this
  }
}
