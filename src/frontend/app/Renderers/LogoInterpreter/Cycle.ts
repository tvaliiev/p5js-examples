import P5 from 'p5'
import { range } from '../../_util/util'
import { Command } from './Command'
import { Turtle } from './Turtle'

export class Cycle {
  public static NAME = 'repeat'
  public static OPEN = '['
  public static CLOSE = ']'
  constructor(
    public times: number,
    public commands: (Command | Cycle)[],
  ) {}

  execute(p5: P5, turtle: Turtle) {
    range(this.times).forEach(() => {
      this.commands.forEach(command => {
        command.execute(p5, turtle)
      })
    })
  }
}
