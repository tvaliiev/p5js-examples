import P5 from 'p5'
import { IPoint } from '../../_util/Point'

export class Turtle {
  handlers: { [key: string]: Function }
  public isDrawing: boolean = true

  constructor(
    private position: IPoint,
    private angle: number,
    private p5: P5,
  ) {
    p5.translate(position.x, position.y)
    p5.rotate(angle)
  }

  private getHandlers(): { [key: string]: (arg) => void } {
    return {
      fd: arg => {},
      bk: arg => {},
      rt: arg => {},
      lt: arg => {},
      cs: () => {
        this.p5.background(51)
      },
      pu: () => {
        this.isDrawing = false
      },
      pd: () => {
        this.isDrawing = true
      },
    }
  }
}
