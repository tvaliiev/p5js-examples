import { Command } from './Command'
import { BackCommand } from './Commands/BackCommand'
import { ForwardCommand } from './Commands/ForwardCommand'
import { LeftCommand } from './Commands/LeftCommand'
import { PenDownCommand } from './Commands/PenDownCommand'
import { PenUpCommand } from './Commands/PenUpCommand'
import { RightCommand } from './Commands/RightCommand'

export class CommandFactory {
  private static commands = {
    [ForwardCommand.NAME]: ForwardCommand,
    [RightCommand.NAME]: RightCommand,
    [LeftCommand.NAME]: LeftCommand,
    [BackCommand.NAME]: BackCommand,
    [PenUpCommand.NAME]: PenUpCommand,
    [PenDownCommand.NAME]: PenDownCommand,
  }

  public static hasCommand(tag: string): boolean {
    return this.commands.hasOwnProperty(tag)
  }

  public static create(tag: string): Command {
    if (!CommandFactory.hasCommand(tag)) {
      return null
    }

    let command = this.commands[tag]
    console.log(command)
    return new command()
  }
}
