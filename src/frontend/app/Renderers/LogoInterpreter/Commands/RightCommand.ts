import P5 from 'p5'
import { Command } from '../Command'
import { Turtle } from '../Turtle'

export class RightCommand extends Command {
  public static NAME = 'rt'

  public execute(p5: P5, turtle: Turtle) {
    p5.rotate(parseInt(this.arg))
  }
}
