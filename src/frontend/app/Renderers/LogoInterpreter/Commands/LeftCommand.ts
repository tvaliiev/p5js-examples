import P5 from 'p5'
import { Command } from '../Command'
import { Turtle } from '../Turtle'

export class LeftCommand extends Command {
  public static NAME = 'lt'

  public execute(p5: P5, turtle: Turtle) {
    p5.rotate(-this.arg)
  }
}
