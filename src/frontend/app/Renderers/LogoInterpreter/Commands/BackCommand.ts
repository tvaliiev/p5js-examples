import P5 from 'p5'
import { Command } from '../Command'
import { Turtle } from '../Turtle'

export class BackCommand extends Command {
  public static NAME = 'bk'

  public execute(p5: P5, turtle: Turtle) {
    let arg = -parseInt(this.arg)
    if (turtle.isDrawing) {
      p5.line(0, 0, arg, 0)
    }

    p5.translate(arg, 0)
  }
}
