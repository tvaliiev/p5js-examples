import P5 from 'p5'
import { Command } from '../Command'
import { Turtle } from '../Turtle'

export class PenUpCommand extends Command {
  public static NAME = 'pu'

  public readonly NEED_ARG = false

  public execute(p5: P5, turtle: Turtle) {
    turtle.isDrawing = false
  }
}
