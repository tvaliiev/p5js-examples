import P5 from 'p5'
import { Command } from '../Command'
import { Turtle } from '../Turtle'

export class PenDownCommand extends Command {
  public static NAME = 'pd'

  public readonly NEED_ARG = false

  public execute(p5: P5, turtle: Turtle) {
    turtle.isDrawing = true
  }
}
