import P5 from 'p5'
import { Command } from './Command'
import { CommandFactory } from './CommandFactory'
import { Cycle } from './Cycle'
import { Turtle } from './Turtle'

export class Ast {
  constructor(public nodes: (Cycle | Command)[] = []) {}
}

class Executor {
  constructor(private ast: Ast) {}

  execute(p5: P5, turtle: Turtle) {
    for (const node of this.ast.nodes) {
      node.execute(p5, turtle)
    }
  }
}

export class Parser {
  private index = 0

  constructor(private text: string) {}

  parse(): Ast {
    const ast = new Ast()

    this.skipSpaces()

    while (this.index < this.text.length) {
      this.skipSpaces()
      const commandTag = this.getNextWord()
      if (commandTag === Cycle.NAME) {
        ast.nodes.push(this.createCycle())
      } else {
        const parsedCommand = CommandFactory.create(commandTag)
        if (!parsedCommand) {
          throw new Error('Cannot create these command: ' + commandTag)
        }

        if (parsedCommand.needArgument()) {
          parsedCommand.setArg(this.getNextWord())
        }

        ast.nodes.push(parsedCommand)
      }
    }

    return ast
  }

  parseAndExecute(p5: P5, turtle: Turtle) {
    const ast = this.parse()
    new Executor(ast).execute(p5, turtle)
  }

  private skipSpaces() {
    let char = this.getCurrentChar()
    while (char === ' ') {
      this.index++
      char = this.getCurrentChar()
    }
  }

  private createCycle(): Cycle {
    const times = parseInt(this.getNextWord())

    if (this.getNextWord() !== Cycle.OPEN) {
      throw new Error(
        'Syntax exception: cycle did not start properly at ' + this.index,
      )
    }

    let cycleParenthesisStack = []
    let cycleSubstr = ''
    let char
    while ((char = this.getCurrentChar())) {
      this.index++

      if (char === Cycle.CLOSE) {
        if (cycleParenthesisStack.length === 0) {
          break
        }

        const last = cycleParenthesisStack.pop()
        if (last === Cycle.CLOSE) {
          throw new Error(
            'Syntax exception: parenthesis did not match at ' + this.index,
          )
        }
      } else if (char === Cycle.OPEN) {
        cycleParenthesisStack.push(char)
      }

      cycleSubstr += char
    }

    this.skipSpaces()

    return new Cycle(times, new Parser(cycleSubstr).parse().nodes)
  }

  private getNextWord(): string {
    let token = ''
    let char = this.getCurrentChar()
    while (char && char !== ' ') {
      token += char
      this.index++
      char = this.getCurrentChar()
    }

    this.skipSpaces()

    return token
  }

  private getCurrentChar() {
    return this.text.charAt(this.index)
  }
}
