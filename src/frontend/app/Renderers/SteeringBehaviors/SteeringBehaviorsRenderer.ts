import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions, IRenderer } from '../../_util/Renderer'
// @ts-ignore
import fontUrl from 'data-url:../../../fonts/avenir-next-lt-pro-demi.otf'
import { Vehicle } from '../../_util/Vehicle'

export class SteeringBehaviorsRenderer
  extends AbstractRenderer
  implements IRenderer
{
  public static NAME = 'steering-behavior'

  private textConfig = {
    text: 'train',
    x: 100,
    y: 250,
    fontSize: 200,
  }

  private font
  private vehicles: Vehicle[] = []
  private input

  public getDimensions(): IDimensions {
    return { x: 1200, y: 400 }
  }

  public preload() {
    this.font = this.p5.loadFont(fontUrl)
  }

  public setup() {
    super.setup()

    const { p5 } = this

    p5.background(51)

    p5.textFont(this.font)
    this.input = p5.createInput(this.textConfig.text)
    this.input.position(0, 20)

    this.initialize()
  }

  private initialize() {
    const { p5 } = this
    const textPoints = this.font.textToPoints(
      this.textConfig.text,
      this.textConfig.x,
      this.textConfig.y,
      this.textConfig.fontSize,
    )
    this.vehicles = []
    textPoints.forEach(({ x: pointX, y: pointY }) => {
      const point = new Point(pointX, pointY)
      this.vehicles.push(
        new Vehicle(
          point,
          new Point(p5.random(p5.width), p5.random(p5.height)),
          5,
          2,
          new Point(p5.random(-1, 1), p5.random(-1, 1)),
        ),
      )
    })
  }

  public draw() {
    const { p5 } = this
    const mousePosition = new Point(p5.mouseX, p5.mouseY)

    if (this.textConfig.text !== this.input.value()) {
      this.textConfig.text = this.input.value()
      this.initialize()
    }

    p5.background(51).stroke(0, 255, 0).strokeWeight(4)

    this.vehicles.forEach(vehicle => {
      vehicle.behaviors(mousePosition)
      vehicle.update()
      vehicle.drawAsPoint(p5)
    })
  }
}
