import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions, IRenderer } from '../../_util/Renderer'
import { randomBetween } from '../../_util/util'

export class RecursionRenderer extends AbstractRenderer implements IRenderer {
  public static NAME = 'recursion'

  public getDimensions(): IDimensions {
    return {
      x: 600,
      y: 600,
    }
  }

  public draw() {
    const { p5 } = this

    p5.background(0).stroke(255).noFill().noLoop()

    this.drawCircle(new Point(300, 200), 300)
  }

  public drawCircle(coord: Point, d: number): void {
    this.p5.circle(coord.x, coord.y, d)

    if (d <= 2) {
      return
    }

    const newDiameter = d * randomBetween(0.2, 0.85)

    this.drawCircle(coord.add(newDiameter, 0), newDiameter)

    this.drawCircle(coord.add(-newDiameter, 0), newDiameter)

    this.drawCircle(coord.add(0, newDiameter), newDiameter)
  }
}
