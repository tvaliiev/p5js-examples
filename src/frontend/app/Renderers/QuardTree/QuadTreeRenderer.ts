import P5 from 'p5'
import { Boundary } from '../../_util/Boundary'
import { Color } from '../../_util/Color'
import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions, IRenderer } from '../../_util/Renderer'
import { randomFloat, range } from '../../_util/util'
import { QuadTree } from '../../_util/QuadTree'
import { FixedSizeArray } from '../../_util/FixedSizeArray'
import { PerlinNoiseGrid } from '../../_util/PerlinNoiceGrid'

export class QuadTreeRenderer extends AbstractRenderer implements IRenderer {
  public static NAME = 'quard-tree'

  private points: Point[] = []
  private tree: QuadTree
  private treeThresholdSlider: P5.Element
  private pointsAmountSlider: P5.Element
  private lastTreeThresholdSliderValue: number =
    QuadTree.DEFAULT_DIVIDE_THRESHOLD
  private lastPointsAmountSliderValue: number = 20_000
  private frameRates: FixedSizeArray<number> = new FixedSizeArray<number>(20)

  public getDimensions(): IDimensions {
    return { x: 1400, y: 800 }
  }

  public setup() {
    super.setup()

    const { p5 } = this
    this.reset()

    this.treeThresholdSlider = p5.createSlider(
      10,
      10_000,
      this.tree.getThreshold(),
    )
    this.treeThresholdSlider.size(500)

    this.pointsAmountSlider = p5.createSlider(20_000, 600_000)
    this.pointsAmountSlider.size(500)

    // this.dividerTest(this.tree, 3)
    // this.divideDebug([3, 3, 3], this.tree)
    // this.dividerTest(this.tree, 2)
  }

  private reset() {
    this.initPoints()
    this.generateQuadTree()
  }

  private initPoints() {
    const { p5 } = this

    const map = new PerlinNoiseGrid(new Point(p5.width, p5.height), 1)
    this.points = []
    range(this.lastPointsAmountSliderValue).forEach(() => {
      const point = new Point(
        p5.floor(p5.random(p5.width)),
        p5.floor(p5.random(p5.height)),
      )

      const cell = map.getCellAt(point)
      if (cell.data > 0.7) {
        return
      }
      console.log(cell.data)

      this.points.push(point)
    })

    console.log(`Amount of points = ${this.points.length}`)
  }

  private divideDebug(path: number[], tree: QuadTree) {
    const item = path.shift()

    tree.divide()

    if (!item) {
      return
    }

    return this.divideDebug(path, tree.getSubTrees()[item])
  }

  private dividerTest(node, depth = 5) {
    if (depth === 0) {
      return
    }

    if (!node.isDivided()) {
      node.divide()
    }

    Object.values(node.getSubTrees()).forEach(_ =>
      this.dividerTest(_, depth - 1),
    )
  }

  public draw() {
    const { p5 } = this

    if (p5.mouseIsPressed) {
      const point = new Point(p5.mouseX, p5.mouseY)
      this.points.push(point)
      this.tree.addPoint(point)
    }

    p5.background(0)

    p5.loadPixels()
    const pointColor = new Color(255, 255, 255)
    this.points.forEach(point => {
      point.drawAsPixel(p5, pointColor)
    })
    p5.updatePixels()

    p5.noFill()
    p5.stroke(255)
    p5.strokeWeight(1)

    const sliderValue = parseInt(this.treeThresholdSlider.value().toString())
    if (this.lastTreeThresholdSliderValue !== sliderValue) {
      this.lastTreeThresholdSliderValue = sliderValue

      this.reset()
    }

    const pointSliderValue = parseInt(
      this.pointsAmountSlider.value().toString(),
    )
    if (this.lastPointsAmountSliderValue !== pointSliderValue) {
      this.lastPointsAmountSliderValue = pointSliderValue

      this.reset()
    }

    this.tree.draw(p5)

    if (
      p5.mouseX > 0 &&
      p5.mouseX < p5.width &&
      p5.mouseY > 0 &&
      p5.mouseY < p5.height
    ) {
      const boundary = new Boundary(
        new Point(p5.mouseX - 100, p5.mouseY - 100),
        new Point(p5.mouseX + 100, p5.mouseY + 100),
      )

      p5.stroke(0, 255, 0)
      boundary.draw(p5)
      p5.fill(0, 255, 0)
      this.tree
        .itemsInBoundary(boundary)
        .forEach(item => item.point.drawAsCircle(p5, 3))
    }

    this.frameRates.push(p5.frameRate())
    const items = this.frameRates.getItems()
    let sum = items.reduce((acc, n) => acc + n, 0)
    p5.stroke(255, 0, 0)
    p5.fill(255, 0, 0)
    p5.text(p5.floor(sum / items.length), 0, 0, 50, 20)
  }

  private generateQuadTree() {
    const offset = 0
    this.tree = new QuadTree(
      new Boundary(
        new Point(offset, offset),
        new Point(this.p5.width - offset, this.p5.height - offset),
      ),
      this.lastTreeThresholdSliderValue,
    )
    console.log()

    this.points.forEach(point => this.tree.addPoint(point))
  }
}
