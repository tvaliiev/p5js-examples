import { Direction, DirectionMove } from '../../_util/Direction'
import { Point } from '../../_util/Point'
import { AbstractRenderer, IDimensions } from '../../_util/Renderer'
import { isPrime } from '../../_util/util'

export class PrimeSpiralRenderer extends AbstractRenderer {
  private step = 1
  private numberOfStepsBeforeTurn = 1
  private turnCounter = 0
  private maxSteps: number

  private currentPosition: Point
  private currentDirection: Direction = Direction.RIGHT
  private spacing: Point = new Point(30, 30)
  private nextDirection: { [key in Direction]?: Direction } = {
    [Direction.RIGHT]: Direction.UP,
    [Direction.UP]: Direction.LEFT,
    [Direction.LEFT]: Direction.DOWN,
    [Direction.DOWN]: Direction.RIGHT,
  }

  public getDimensions(): IDimensions {
    return { x: 800, y: 800 }
  }

  public setup() {
    super.setup()
    const { p5 } = this
    p5.background(0)

    p5.textAlign(p5.CENTER, p5.CENTER).textSize(10).fill(255).stroke(255)
    this.maxSteps = Point.fromObject(this.getDimensions())
      .dividePoint(this.spacing)
      .reduce((x, y) => Math.ceil(x) * Math.ceil(y))
    this.currentPosition = Point.fromObject(this.getDimensions()).divide(2, 2)
  }

  private drawCurrentNumber() {
    if (isPrime(this.step)) {
      this.p5.circle(
        this.currentPosition.x,
        this.currentPosition.y,
        this.spacing.x / 3,
      )
    }
  }

  public draw(): void {
    this.drawPointAndAdvance()
    if (this.step % this.numberOfStepsBeforeTurn === 0) {
      this.switchToNextDirection()
      this.turnCounter++
      if (this.turnCounter % 2 === 0) {
        this.numberOfStepsBeforeTurn++
      }
    }
    this.step++
    if (this.step > this.maxSteps) {
      this.p5.noLoop()
      console.log('test')
    }
  }

  private drawPointAndAdvance() {
    this.drawCurrentNumber()
    const nextPosition = DirectionMove[this.currentDirection]
      .multiplyPoint(this.spacing)
      .addPoint(this.currentPosition)
    this.p5.line(
      this.currentPosition.x,
      this.currentPosition.y,
      nextPosition.x,
      nextPosition.y,
    )

    this.currentPosition = nextPosition
  }

  private switchToNextDirection() {
    this.currentDirection = this.nextDirection[this.currentDirection]
  }
}
