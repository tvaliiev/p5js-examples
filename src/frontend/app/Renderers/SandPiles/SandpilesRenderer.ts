import { IColor } from '../../_util/Color'
import { AbstractRenderer, IDimensions, IRenderer } from '../../_util/Renderer'
import { range } from '../../_util/util'

type SandpileItem = number
type SandpileGrid = { [key: number]: { [key: number]: SandpileItem } }

export class SandpilesRenderer extends AbstractRenderer implements IRenderer {
  public static NAME = 'sandpiles'
  private static MAX_PIECES = 3
  private sandpiles: SandpileGrid = {}
  private nextSandpiles: SandpileGrid = {}
  private readonly colors: { [key: number]: IColor } = {
    [-1]: { r: 255, g: 0, b: 0 },
    0: { r: 255, g: 255, b: 0 },
    1: { r: 0, g: 185, b: 63 },
    2: { r: 0, g: 104, b: 255 },
    3: { r: 122, g: 0, b: 229 },
  }

  public getDimensions(): IDimensions {
    return { x: 600, y: 600 }
  }

  public setup() {
    super.setup()

    const p5 = this.p5

    this.forEachPixel((x, y) => {
      if (!this.sandpiles[y]) {
        this.sandpiles[y] = []
      }

      this.sandpiles[y][x] = 0
    })

    this.sandpiles[p5.floor(p5.height / 2)][p5.floor(p5.width / 2)] = 99999999

    const defaultColor = this.colors[-1]
    p5.background(defaultColor.r, defaultColor.g, defaultColor.b)

    // p5.noLoop()
  }

  public draw() {
    this.render()

    range(100, 0, 0).forEach(() => this.topple())
  }

  private topple() {
    const p5 = this.p5

    this.nextSandpiles = Object.assign({}, this.sandpiles)
    // this.forEachPixel((x, y) => this.nextSandpiles[y][x] = this.sandpiles[y][x])

    this.forEachPixel((x, y) => {
      let num = this.sandpiles[y][x]
      if (num <= 3) {
        return
      }
      this.nextSandpiles[y][x] -= 4

      if (x + 1 < p5.width) this.nextSandpiles[y][x + 1]++

      if (x - 1 >= 0) this.nextSandpiles[y][x - 1]++

      if (y + 1 < p5.height) this.nextSandpiles[y + 1][x]++

      if (y - 1 >= 0) this.nextSandpiles[y - 1][x]++
    })

    const tmp = this.sandpiles
    this.sandpiles = this.nextSandpiles
    this.nextSandpiles = tmp
  }

  private render() {
    const p5 = this.p5
    p5.loadPixels()

    this.forEachPixel((x, y) => {
      const index = (x + y * p5.width) * 4
      const color = this.colors[this.sandpiles[y][x]] || this.colors[-1]

      p5.pixels[index] = color.r
      p5.pixels[index + 1] = color.g
      p5.pixels[index + 2] = color.b
    })

    p5.updatePixels()
  }
}
